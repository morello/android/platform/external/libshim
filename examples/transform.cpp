/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

// Example which includes all existing transformations.
//
// Such a project should add libshim as a static library dependency.

#include <check.h>
#include <stl/panic.h>
#include <transform/base.h>
#include <transform/elf_auxv_t.h>
#include <transform/epoll.h>
#include <transform/ifconf.h>
#include <transform/iovec.h>
#include <transform/mmsghdr.h>
#include <transform/msghdr.h>
#include <transform/pointer.h>
#include <transform/pointer_array.h>
#include <transform/prctl_mm_map.h>
#include <transform/pselect.h>
#include <transform/sigaction.h>
#include <transform/sigevent_t.h>
#include <transform/siginfo_t.h>
#include <transform/sock_fprog.h>
#include <transform/stack_t.h>

#include <iostream>

// Example to implement a custom transformation re-using implementation in libshim.

using namespace morello::shim::transform;
using morello::shim::check::Check;
using morello::shim::stl::Arg;

struct foo {
  int bar;
};

// The local side 'struct foo' layout translating from.
using local_foo_t = foo;

// The target side 'struct foo' layout translating into.
struct target_foo_t final {
  target_foo_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_foo_t& local, bool) noexcept {
  }

  // Transforms from local into target memory layout.
  target_foo_t& operator<<(const local_foo_t& local) noexcept {
    bar = local.bar;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_foo_t& operator>>(local_foo_t& local) noexcept {
    local.bar = bar * 2;
    return local;
  }

  int bar;
};

using FooTransformation = InputTransformation<local_foo_t, target_foo_t>;

int main() {
  local_foo_t local_foo{ .bar = 42 };

  {
    Check check{ "my check" };
    FooTransformation foo_tr{ check, Arg{ &local_foo } };
    std::cout << "Target foo.bar: " << foo_tr.data()->bar << "\n";
    // Write-back occurs here.
  }

  std::cout << "Local foo.bar after write-back: " << local_foo.bar << "\n";

  return 0;
}
