#!/usr/bin/env python3
#
# Copyright (c) 2020-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# How this file works
#
# Fixups is a simple dictionary where the root key is the name of the system call.
# The next level is the index of "what to be fixed".
#
# For example, the generator knows arg1 has to be checked but it doesn't know how.
# In this case it can be fixed as { 'foo': 1: { 'check': '...' } } where '1'
# means the index of the check.
#
# Determining the index of a fixup is simple: just look at the generated code and count.
#
# Possible fixes:
#   'temp_var': adds a temporary variable to be passed to checks
#   'check': fixes a system call argument check.
#   'transform': fixes a transformation.
#   'param': changes a parameter name in the svc() call.
#   'return_transform': changes how the return value is returned from the system call.
#
# It is also possible to replace the whole svc call with 'svc': { 'foo': 'svc': '...' }

FIXUPS = {
    'epoll_ctl': {
        1: {
            'transform': 'transform::EpollEvents epoll_events_4{ check, Arg{ arg4, 4 }, Arg { (arg4 == nullptr) ? 0 : 1, 4 } };',
        },
        4: {
            'param': 'epoll_events_4',
        },
    },
    'execve': {
        1: {
            'transform': 'transform::ConstPointerArray ptr_array_2{ check, Arg{ arg2, 2}, Arg{ transform::nullterminated_array_length(arg2), 2 } };',
        },
        2: {
            'check': '.const_string_array(Arg{ arg2, 2 })',
            'transform': 'transform::ConstPointerArray ptr_array_3{ check, Arg{ arg3, 3 }, Arg{ transform::nullterminated_array_length(arg3), 3} };',
            'param': 'ptr_array_2',
        },
        3: {
            'check': '.const_string_array(Arg{ arg3, 3 })',
            'param': 'ptr_array_3',
        },
    },
    'getcpu': {
        3: {
            'check': '// NOTE: Android does not support the third argument.\n',
        },
    },
    'init_module': {
        1: {
            'check': '.buffer(Arg{ arg1, 1 }, Arg{ arg2, 2 })',
        },
    },
    'mount': {
        4: {
            'check': '.const_string(Arg{ arg5, 5 })',
        },
    },
    'ppoll': {
        1: {
            'check': '.pointer_to_array<pollfd>(Arg{ arg1, 1 }, Arg{ arg2, 2 })',
        },
    },
    'pselect6': {
        5: {
            'check': '// The 6th argument is rather interesting.\n',
        },
        1: {
            'transform': 'transform::PSelect6SigSet sigset_6{ check, Arg{ arg6, 6 } };',
        },
        'svc': 'return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, sigset_6, nr);',
    },
    'syslog': {
        1: {
            'check': '.buffer(Arg{ arg2, 2 }, Arg{ arg3, 3 })',
        },
    },
    'utimensat': {
        2: {
            'check': '.pointer_to_const_array<const struct timespec>(Arg{ arg3, 3 }, 2)',
        },
    },
    'waitid': {
        1: {
            'check': '// Android does not use arg5.\n.pointer<struct rusage>(Arg{ arg5, 5 })',
        },
    },
    'rt_sigprocmask': {
        1: {
            'check': '.const_buffer(Arg{ arg2, 2 }, Arg{ arg4, 4 })',
        },
        2: {
            'check': '.buffer(Arg{ arg3, 3 }, Arg{ arg4, 4 })',
        },
    },
    'rt_sigtimedwait': {
        1: {
            'check': '.const_buffer(Arg{ arg1, 1 }, Arg{ arg4, 4 })',
        }
    },
    'clone': {
        1: {
            'check': '.buffer(Arg{ arg2, 2 }, Arg{ 1 }) // child_stack',
        },
        3: {
            'check': '.const_buffer(Arg{ arg4, 4 }, Arg{ 1 }) \
// TLS, don\'t think it is a good idea to test W',
        }
    },
    'utimensat': {
        2: {
            'check': '.pointer_to_const_array<const struct timespec>(Arg{ arg3, 3 }, 2)',
        }
    },
    'ppoll': {
        1: {
            'check': '.pointer_to_array<pollfd>(Arg{ arg1, 1 }, Arg{ arg2, 2 })',
        },
        3: {
            'check': '.const_buffer(Arg{ arg4, 4 }, Arg{ arg5, 5 })',
        }
    },
    'epoll_pwait': {
        1: {
            'check': '.const_buffer(Arg{ arg5, 5 }, Arg{ arg6, 6 })',
        }
    },
    'process_madvise': {
        1: {
            'transform': 'transform::ConstIoVecs iovecs_2{ check, Arg{ arg2, 2 }, Arg{ arg3, 3 }, transform::iovec_data{ static_cast<PidFd>(arg1) } };',
        }
    },
    'process_vm_readv': {
        2: {
            'transform': 'transform::ConstIoVecs iovecs_4{ check, Arg{ arg4, 4 }, Arg { arg5, 5 }, transform::iovec_data{ static_cast<Pid>(arg1) } };',
        }
    },
    'process_vm_writev': {
        2: {
            'transform': 'transform::ConstIoVecs iovecs_4{ check, Arg{ arg4, 4 }, Arg { arg5, 5 }, transform::iovec_data{ static_cast<Pid>(arg1) } };',
        }
    },
    'rt_sigqueueinfo': {
        1: {
            'transform': 'transform::SigInfo siginfo_3{ check, Arg{ arg3, 3 }, transform::siginfo_t_data{ static_cast<Pid>(arg1) } };',
        }
    },
    'rt_tgsigqueueinfo': {
        1: {
            'transform': 'transform::SigInfo siginfo_4{ check, Arg{ arg4, 4 }, transform::siginfo_t_data{ static_cast<Pid>(arg1) } };',
        }
    },
    'pidfd_send_signal': {
        1: {
            'transform': 'transform::SigInfo siginfo_3{ check, Arg{ arg3, 3 }, transform::siginfo_t_data{ static_cast<PidFd>(arg1) } };',
        }
    },
    'sigaltstack': {
        1: {
            'check': '.require_executive()',
        },
    },
    'clone3': {
        'svc': 'return LIBSHIM_SVC(cl_args_1, sizeof(transform::target_clone_args_t), arg3, arg4, arg5, arg6, nr);',
    },
    'io_getevents': {
        1: {
            'check': '.pointer_to_array<struct io_event>(Arg{ arg4, 4 }, Arg{ arg3, 3 })',
        },
    },
    'io_submit': {
        1: {
            'transform': 'transform::IoCBPP iocbpp_3{check, Arg{ arg3, 3 }, Arg{ arg2, 2 }};',
        },
    },
}

def add_fixup(name, arg, fixup_type, fixup):
    if name not in FIXUPS:
        FIXUPS[name] = {}
    if arg not in FIXUPS[name]:
        FIXUPS[name][arg] = {}
    if fixup_type not in FIXUPS[name][arg]:
        FIXUPS[name][arg][fixup_type] = fixup
