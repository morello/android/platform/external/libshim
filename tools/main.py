#!/usr/bin/env python3
#
# Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# Main entry point of the shim layer generator.

import sys

# Version check: require python 3.6 or newer.
if sys.version_info.major < 3 or (
    sys.version_info.major == 3 and sys.version_info.minor < 6
):
    sys.stderr.write(
        '\nError: genshim requires Python 3.6 or newer. Current version:\n'
    )
    sys.stderr.write('Python {}\n\n'.format(sys.version))
    sys.exit(1)

if __name__ == '__main__':
    import genshim

    genshim.entry()
