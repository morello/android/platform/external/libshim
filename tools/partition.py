#!/usr/bin/env python3
#
# Copyright (c) 2020-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# This class is used to partition system calls.
#
# Partitioning is based on the parameter lists.
#
# Example:
#   void f(int, char*);
#   int g(long, char*);
#   Are in the same partition.

from operator import attrgetter

import config
import fixups
import transforms


class Partition:
    '''Representation of a system call partition.'''

    INT_TY = 'INT'
    SIZE_T_TY = 'SIZE_T'
    STR_TY = 'STR'
    CONST_STR_TY = 'CONST_STR'
    PTR_TY = 'PTR'
    CONST_PTR_TY = 'CONST_PTR'

    def __init__(self, syscall):
        '''Creates a new partition instance.'''
        syscall.set_partition(self)
        self.syscalls = [syscall]
        self.hash = []
        self.unique = False
        self.temp_vars = None
        self.checks = None
        self.transforms = None
        self.return_transform = None
        self.params = None
        self.cap_own_check = syscall.cap_own_check
        self.svc_override = None
        self.has_fixups = fixups.FIXUPS.get(syscall.name, None) is not None

        if syscall.custom:
            self.hash = [syscall.name]
            self.unique = True
        else:
            for param in syscall.params:
                if 'size_t' in param:
                    self.hash.append(self.SIZE_T_TY)
                    continue
                elif param in ['const struct timespec times[2]']:
                    # Notes: utimensat() and futimens() have this weird prototype.
                    self.hash.append('const struct timespec*')
                    continue
                elif '[' in param and ']' in param:
                    # int[N] and similar
                    self.hash.append(param)
                    continue
                elif '*' not in param:
                    self.hash.append(self.INT_TY)
                    continue
                elif param in ['char*', 'unsigned char*']:
                    self.hash.append(self.STR_TY)
                    continue
                elif param in ['const char*']:
                    self.hash.append(self.CONST_STR_TY)
                    continue
                elif param in ['void*']:
                    self.hash.append(self.PTR_TY)
                    continue
                elif param in ['const void*']:
                    self.hash.append(self.CONST_PTR_TY)
                    continue
                else:
                    self.hash.append(param)

        while len(self.hash) < config.SHIM_PARAMETERS:
            self.hash.append(self.INT_TY)

    @property
    def name(self) -> str:
        '''Returns the name of this partition.
        This name can be used as function names.
        '''
        if len(self.syscalls) > 1:
            return f'{sorted(self.syscalls, key=attrgetter("name"))[0].name}_like'
        else:
            return self.syscalls[0].name

    @property
    def returns_pointer(self) -> bool:
        '''Returns true if the system calls in this partition return a pointer.'''
        return self.syscalls[0].returns_pointer

    def try_merge(self, other) -> bool:
        '''Merges a partition into another one if the are equivalent.
        Returns true if the two partititions got merged.
        '''
        if self == other:
            for syscall in other.syscalls:
                syscall.set_partition(self)
            self.syscalls.extend(other.syscalls)
            return True
        else:
            return False

    def len(self):
        '''Returns the number of system calls in this partition.'''
        return len(self.syscalls)

    def __eq__(self, other):
        '''Checks if two partitions are equal.'''
        return (
            (self.hash == other.hash)
            and (self.returns_pointer == other.returns_pointer)
            and (self.cap_own_check == other.cap_own_check)
            and (not self.has_fixups)
            and (not other.has_fixups)
        )

    def __str__(self):
        '''Returns the string representation of this partition.'''
        return f'{self.hash}:\n    {[sc.name for sc in self.syscalls]}'

    def finalize(self) -> [str]:
        '''Calculates checks, transformations and svc() call arguments.'''
        self.temp_vars = []
        self.checks = []
        self.transforms = []
        self.params = []

        for (idx, ty) in enumerate(self.hash):
            temp_var = None
            check = None
            transform = None
            param = f'arg{idx + 1}'

            arg1 = f'Arg{{ arg{idx + 1}, {idx + 1} }}'
            arg2 = f'Arg{{ arg{idx + 2}, {idx + 2} }}'

            if ty in [self.CONST_STR_TY]:
                check = f'.const_string({arg1})'
            elif ty in [self.STR_TY, self.PTR_TY]:
                if (len(self.hash) > (idx + 1)) and (
                    self.hash[idx + 1] == self.SIZE_T_TY
                ):
                    if not self.cap_own_check:
                        check = f'.buffer({arg1}, {arg2})'
                    else:
                        check = f'.is_owning_capability({arg1}, {arg2})'
            elif ty in [self.CONST_PTR_TY]:
                if (len(self.hash) > (idx + 1)) and (
                    self.hash[idx + 1] == self.SIZE_T_TY
                ):
                    if not self.cap_own_check:
                        check = f'.const_buffer({arg1}, {arg2})'
                    else:
                        check = f'.is_owning_capability({arg1}, {arg2})'
            # BUFFER_LIKE_CHECKS
            elif ty in config.BUFFER_LIKE_CHECKS.keys():
                if (
                    len(self.hash) > (idx + 1)
                    and self.hash[idx + 1] in config.BUFFER_LIKE_CHECKS[ty]
                ):
                    if '*' in self.hash[idx + 1]:
                        check = f'.buffer<{self.hash[idx + 1][:-1]}>({arg1}, {arg2})'
                    else:
                        check = f'.buffer({arg1}, {arg2})'
            # CONST_BUFFER_LIKE_CHECKS
            elif ty in config.CONST_BUFFER_LIKE_CHECKS.keys():
                if (
                    len(self.hash) > (idx + 1)
                    and self.hash[idx + 1] in config.CONST_BUFFER_LIKE_CHECKS[ty]
                ):
                    if '*' in self.hash[idx + 1]:
                        check = (
                            f'.const_buffer<{self.hash[idx + 1][:-1]}>({arg1}, {arg2})'
                        )
                    else:
                        check = f'.const_buffer({arg1}, {arg2})'
            elif '[' in ty and ']' in ty:
                check = f'.buffer({arg1}, Arg{{ sizeof({ty}) }})'
            # Reserved struct names
            elif self.syscalls[0].custom is False and isinstance(ty, str):
                for key in transforms.TRANSFORMS:
                    if ty == key:
                        tr = transforms.TRANSFORMS[key]
                        param = f'{tr["name"]}_{idx + 1}'
                        if tr['wl']:
                            transform = f'{tr["fn"]} {param}{{check, {arg1}, {arg2}}};'
                        else:
                            transform = f'{tr["fn"]} {param}{{check, {arg1}}};'
                        break

                if transform is None and '*' in ty:
                    data_ty = ty.replace('*', '')
                    if 'const' in ty:
                        check = f'.pointer_to_const<{data_ty}>({arg1})'
                    else:
                        check = f'.pointer<{data_ty}>({arg1})'

            if not check and not transform and ty not in [self.INT_TY, self.SIZE_T_TY]:
                check = f'#error FIXME: check arg{idx + 1}'
            else:
                pass

            if temp_var:
                self.temp_vars.append(temp_var)
            if check:
                self.checks.append(check)
            if transform:
                self.transforms.append(transform)
            self.params.append(param)

        if self.returns_pointer:
            self.return_transform = '#error ret.to_valid_data_pointer(arg2); // Please override explicitly in fixups.py.)'

        # Apply manual fixups
        for name in fixups.FIXUPS:
            if name == self.syscalls[0].name:
                for arg, fixup in fixups.FIXUPS[name].items():
                    if isinstance(fixup, dict):
                        if fixup.get('temp_var'):
                            self.replace_or_insert(
                                self.temp_vars, arg, fixup['temp_var']
                            )
                        if fixup.get('check'):
                            self.replace_or_insert(self.checks, arg, fixup['check'])
                        if fixup.get('transform'):
                            self.replace_or_insert(
                                self.transforms, arg, fixup['transform']
                            )
                        if fixup.get('param'):
                            self.replace_or_insert(self.params, arg, fixup['param'])
                    else:
                        if arg == 'return_transform':
                            self.return_transform = fixup
                        if arg == 'svc':
                            self.svc_override = fixup

    @staticmethod
    def replace_or_insert(array, index_1, value):
        '''Replaces a value in a list or appends if the index is out-of-range.'''
        index = index_1 - 1
        if len(array) > index:
            array[index] = value
        else:
            array.insert(index, value)

    @property
    def declarations(self) -> [str]:
        '''Returns a more readable list of system call declarations.'''
        max_retval_size = 0
        max_name_size = 0
        for syscall in self.syscalls:
            max_retval_size = max(len(syscall.retval), max_retval_size)
            max_name_size = max(len(syscall.name), max_name_size)

        decls = []
        for syscall in self.syscalls:
            decls.append(
                ' '.join(
                    [
                        '//',
                        syscall.retval.ljust(max_retval_size),
                        syscall.name.ljust(max_name_size),
                        syscall.param_list,
                    ]
                )
            )

        return decls


def partition_system_calls(system_calls) -> [Partition]:
    '''Partitions system calls.'''
    partitions = []
    for system_call in system_calls:
        equivalent = False
        partition = Partition(system_call)

        for syscall_partition in partitions:
            if syscall_partition.try_merge(partition):
                equivalent = True
                break

        if not equivalent:
            partitions.append(partition)

    [partition.finalize() for partition in partitions]
    partitions = sorted(partitions, key=attrgetter('name'))

    return partitions
