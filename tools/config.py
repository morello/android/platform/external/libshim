#!/usr/bin/env python3
#
# Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#

from os import path

'''The root path for libshim.'''
LIBSHIM_ROOT = path.abspath(path.join(path.dirname(path.abspath(__file__)), '../'))
LIBSHIM_INCLUDE = path.join(LIBSHIM_ROOT, 'include/')

'''Clang format to use'''
CLANG_FORMAT = 'prebuilts/clang/host/linux-x86/clang-stable/bin/clang-format'

if not path.exists(CLANG_FORMAT):
    CLANG_FORMAT = 'clang-format'

'''Name of the type to use as shim arguments.'''
SHIM_TYPE = 'uintptr_shim_t'

'''Number of parameters the shim should accept, excluding NR.'''
SHIM_PARAMETERS = 6

'''Structure names which have to be shimmed.'''
SHIMMED_STRUCTURES = [
    'iovec',
    'msghdr',
    'stack_t',
    'sigevent_t',
    'siginfo_t',
    'sigaction',
    'epoll_event',
]

'''Extend buffer-like checks: <key>: [<size types>].'''
BUFFER_LIKE_CHECKS = {
    'struct sockaddr*': ['INT', 'socklen_t', 'socklen_t*'],
    'sigset64_t*': ['SIZE_T'],
}

'''Extend const buffer-like checks: <key>: [<size types>].'''
CONST_BUFFER_LIKE_CHECKS = {
    'const struct sockaddr*': ['INT', 'socklen_t', 'socklen_t*'],
    'const sigset64_t*': ['SIZE_T'],
}
