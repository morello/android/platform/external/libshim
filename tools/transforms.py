#!/usr/bin/env python3
#
# Copyright (c) 2020 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# How this file works
#
# This is a simple dictionary where the root key is the full type of the system
# call parameter to be transformed.
#
#  {
#    '<system call parameter type>': {
#      'fn': '<type of the transformation to use>',
#      'name': '<prefix name of the generated system call argument>',
#      'wl': '<means "with length": true if the argument is followed by it size.>',
#    }
#  }

TRANSFORMS = {
    'struct epoll_event*': {
        'fn': 'transform::EpollEvents',
        'name': 'epoll_events',
        'wl': True,
    },
    'const struct iovec*': {
        'fn': 'transform::ConstIoVecs',
        'name': 'iovecs',
        'wl': True,
    },
    'const struct msghdr*': {
        'fn': 'transform::ConstMsgHdr',
        'name': 'msghdr',
        'wl': False,
    },
    'struct msghdr*': {
        'fn': 'transform::MsgHdr',
        'name': 'msghdr',
        'wl': False,
    },
    'struct mmsghdr*': {
        'fn': 'transform::MMsgHdrs',
        'name': 'mmsghdrs',
        'wl': True,
    },
    'siginfo_t*': {
        'fn': 'transform::SigInfo',
        'name': 'siginfo',
        'wl': False,
    },
    'struct sigevent*': {
        'fn': 'transform::SigEvent',
        'name': 'sigevent',
        'wl': False,
    },
    'const stack_t*': {
        'fn': 'transform::ConstStack',
        'name': 'stack',
        'wl': False,
    },
    'stack_t*': {
        'fn': 'transform::Stack',
        'name': 'stack',
        'wl': False,
    },
    'const struct sigaction*': {
        'fn': 'transform::ConstSigAction',
        'name': 'sigaction',
        'wl': False,
    },
    'struct sigaction*': {
        'fn': 'transform::SigAction',
        'name': 'sigaction',
        'wl': False,
    },
    'struct clone_args*': {
        'fn': 'transform::CloneArgs',
        'name': 'cl_args',
        'wl': True,
    },
    'struct iocb*': {
        'fn': 'transform::IoCB',
        'name': 'iocb',
        'wl': False,
    },
    'struct iocb**': {
        'fn': 'transform::IoCBPP',
        'name': 'iocbpp',
        'wl': False,
    },
}
