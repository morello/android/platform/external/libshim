#!/usr/bin/env python3
#
# Copyright (c) 2020-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#
# This tool is used to generate most parts of the system call shim layer.

import json
import os
import re
import subprocess
import sys
from operator import attrgetter
from os import path
from string import Template

import config
import partition
from syscall import Syscall


def load_arch_syscalls(libc, arch, optional_path_to_json) -> [Syscall]:
    '''Loads all requested system calls.'''
    if optional_path_to_json:
        if path.exists(optional_path_to_json) and path.isfile(optional_path_to_json):
            syscalls_json_path = optional_path_to_json
        else:
            raise ValueError(
                f'Path "{optional_path_to_json}" does not point to a valid file'
            )
    else:
        syscalls_json_path = path.join(config.LIBSHIM_ROOT, f'syscalls.json')
    with open(syscalls_json_path, 'r') as r_file:
        syscalls = json.loads(r_file.read())

    system_calls = []
    for syscall in syscalls['syscalls']:
        syscall = Syscall.from_dict(syscall)
        #  Filter based on libc
        if syscall.libc and libc not in syscall.libc:
            continue
        #  Filter based on arch
        if syscall.arch and arch not in syscall.arch:
            continue
        system_calls.append(syscall)

    return sorted(system_calls, key=attrgetter('name'))


def expand_syscall_method_declarations(partitions) -> dict:
    '''Expands method declarations for the shim.'''
    method_declarations = []
    for partition in partitions:
        shim_method = f'{partition.name}'
        method_declarations.append(f'LIBSHIM_FN({shim_method});')

    return {'shim_method_declarations': ''.join(method_declarations)}


def expand_syscall_method(system_calls) -> dict:
    '''Expands the huge syscall() method for the shim.'''
    arg_list = ''.join([f'arg{p + 1}, ' for p in range(config.SHIM_PARAMETERS)])

    method = [
        '\n'.join(
            [
                'switch (nr) {',
                '',
            ]
        )
    ]

    for syscall in system_calls:
        if syscall.forward:
            shim_method = 'svc'
        else:
            shim_method = f'{syscall.partition.name}'

        syscall_case = [
            f'#ifdef {syscall.nr}',
            f'case {syscall.nr}:',
            f'// {syscall.signature}',
            f'// {syscall.man_page}',
            f'return LIBSHIM_FN_CALL({shim_method}, {arg_list}nr);',
            f'#endif // {syscall.nr}',
            '',
        ]

        method.append('\n'.join(syscall_case))

    method.append(
        '\n'.join(
            [
                'default:',
                'stl::error_message("Unknown system call number: %d.\\n", nr);',
                '#if LIBSHIM_SYSCALL_STRICT',
                'stl::panic("Terminating.\\n", nr);',
                '#else',
                f'return LIBSHIM_FN_CALL(svc, {arg_list}nr);',
                '#endif',
                '}',
                '',
                '__builtin_trap();',
            ]
        )
    )

    return {'content': ''.join(method)}


def scoped_clang_format_off(items, max_length=79) -> [str]:
    '''Turns clang-format off for a region if needed.'''
    for item in items:
        if len(item) > max_length:
            return ['// clang-format off', *items, '// clang-format on']

    return items


def expand_system_call_names(partition) -> dict:
    '''Creates some logic to decode the name of the system call being called.'''
    if partition.len() == 1:
        return [
            f'static constexpr char syscall_name[] = "{partition.name}";',
            f'static constexpr unsigned int num_args = {len(partition.syscalls[0].params)};',
        ]

    syscall_names = [
        'const char* syscall_name;',
        'unsigned int num_args;',
        '',
        'switch(static_cast<long>(nr)) {',
    ]

    for syscall in partition.syscalls:
        syscall_names += [
            f'#ifdef {syscall.nr}',
            f'case {syscall.nr}:',
            f'syscall_name = "{syscall.name}";',
            f'num_args = {len(syscall.params)};',
            'break;',
            f'#endif // {syscall.nr}',
            '',
        ]

    syscall_names += [
        f'default:',
        f'syscall_name = "<unknown from {partition.name}>";',
        f'num_args = {config.SHIM_PARAMETERS};',
        'break;',
        '}',
        '',
    ]
    return syscall_names


def expand_system_call_method(partition) -> dict:
    '''Expands the shim of a system call.'''
    arg_list = ', '.join(partition.params)
    if partition.svc_override:
        svc_call = partition.svc_override
    elif partition.returns_pointer:
        svc_call = '\n'.join(
            [
                f'auto ret = LIBSHIM_SVC({arg_list}, nr);',
                f'return {partition.return_transform};',
            ]
        )
    else:
        svc_call = f'return LIBSHIM_SVC({arg_list}, nr);'

    if partition.checks or partition.transforms:
        method = [
            f'LIBSHIM_FN({partition.name}) {{',
            *expand_system_call_names(partition),
            '',
        ]

        if partition.temp_vars:
            method += [''.join(map('    {0};\n'.format, partition.temp_vars))]
            method += ['']

        method += [
            '// clang-format off',
            f'  auto check = Check(syscall_name)',
            '    ' + '\n    '.join(partition.checks) + ';',
            '// clang-format on',
            '',
        ]

        if partition.transforms:
            method += partition.transforms
            method += ['']

        method += [
            'if (check.passed()) {',
            svc_call,
            '} else {',
            f'PRINT_CHECKED_CALL_FAILED(check, num_args);',
            'return stl::SCReturnError(check.code());',
            '}',
            '}',
            '',
        ]
    else:
        method = [f'LIBSHIM_FN({partition.name}) {{', svc_call, '}', '']

    arg_list = ''.join([f'arg{p + 1}, ' for p in range(config.SHIM_PARAMETERS)])

    info = scoped_clang_format_off(
        [
            *partition.declarations,
            *[f'// {syscall.man_page}' for syscall in partition.syscalls],
        ]
    )

    return {
        'info': '\n'.join(info),
        'undef': f'#undef {partition.name}',
        'method': '\n'.join(method),
    }


def list_transform_includes(dir) -> str:
    '''Lists all transformations for inclusion.'''
    return '\n'.join(
        [
            f'#include "{path.relpath(path.join(dir, f), start=config.LIBSHIM_INCLUDE)}"'
            for f in os.listdir(dir)
            if path.isfile(path.join(dir, f))
        ]
    )


def generate_cpp_file(template, filepath, values, format=True) -> None:
    '''Generates a CPP file and calls clang-format.'''
    filepath_dir = path.dirname(filepath)
    if not path.exists(filepath_dir):
        os.makedirs(filepath_dir)

    with open(filepath, 'w') as w_file:
        w_file.write(template.substitute(values))

    if format:
        try:
            subprocess.run([config.CLANG_FORMAT, '-i', filepath], check=True)
        except FileNotFoundError:
            # It is not an error if user has no 'clang-format' installed.
            pass


def get_template(template_name) -> Template:
    '''Returns a template from a file.'''
    template_path = path.join(config.LIBSHIM_ROOT, 'tools/templates/', template_name)
    with open(template_path, 'r') as r_file:
        template = r_file.read()

    return Template(template)


def generate_headers(partitions, output_dir):
    '''Generates header files only.'''
    header_template = get_template('shim_header')
    header_path = path.join(output_dir, 'shim_gen.h')
    header_method_declarations = expand_syscall_method_declarations(partitions)
    generate_cpp_file(header_template, header_path, header_method_declarations)


def generate_shims_sources(system_calls, partitions, output_dir):
    '''Generates shims source files.'''
    syscall_method_template = get_template('syscall_method')
    syscall_method_path = path.join(output_dir, 'syscall.cpp')
    syscall_method = expand_syscall_method(system_calls)
    generate_cpp_file(syscall_method_template, syscall_method_path, syscall_method)

    shim_method_template = get_template('shim_method')
    shim_methods_template = get_template('shim_methods')
    transform_includes = list_transform_includes(
        path.join(config.LIBSHIM_ROOT, 'include/transform/')
    )
    system_call_methods = []
    for partition in partitions:
        method = shim_method_template.substitute(expand_system_call_method(partition))
        if not partition.unique:
            system_call_methods.append(method)
        elif not path.exists(
            path.join(config.LIBSHIM_ROOT, f'src/{partition.name}.cpp')
        ):
            shim_method_path = path.join(
                config.LIBSHIM_ROOT, f'src/todo/{partition.name}.cpp'
            )
            generate_cpp_file(
                shim_methods_template,
                shim_method_path,
                {
                    'methods_shimmed_impl': method,
                    'transform_includes': transform_includes,
                },
            )

    shim_methods_path = path.join(output_dir, 'shims.cpp')
    system_call_methods = {
        'methods_shimmed_impl': ''.join(system_call_methods),
        'transform_includes': transform_includes,
    }
    generate_cpp_file(shim_methods_template, shim_methods_path, system_call_methods)


def generate_wrappers_sources(system_calls, partitions, output_dir):
    '''Generates wrappers source files.'''
    wrappers_method_path = path.join(output_dir, f'wrappers.cpp')
    wrapper_alias_template = get_template('wrapper_alias_decl')
    alias_decl_template = get_template('alias_decl')
    wrapper_methods_template = get_template('wrapper_methods')

    shimmed_c_definitions = []
    shimmed = []
    forwarded = []

    for partition in partitions:
        shimmed_c_definitions.append(f'LIBSHIM_FN_C({partition.name});')

    for system_call in system_calls:
        if system_call.forward:
            shim_function_name = '__shim_svc'
            collection = forwarded
        else:
            if system_call.partition.name == system_call.function:
                # Wrapper name is same as the shim function name,
                # so no wrapper alias is needed.
                continue

            shim_function_name = f'__shim_{system_call.partition.name}'
            collection = shimmed

        collection.append(
            wrapper_alias_template.substitute(
                {
                    'man_page': system_call.man_page,
                    'wrapper_alias_name': system_call.function,
                    'shim_function_name': shim_function_name,
                }
            )
        )

    content = {
        'methods_c_definitions_shimmed': '\n'.join(shimmed_c_definitions),
        'methods_wrapper_aliases_shimmed': '\n'.join(shimmed),
        'methods_wrapper_aliases_forwarded': '\n'.join(forwarded),
    }
    generate_cpp_file(
        wrapper_methods_template, wrappers_method_path, content, format=False
    )


def generate_stub(system_calls, output_dir):
    '''Generates stub cpp file.'''
    src_template = get_template('shim_stubs')
    src_path = path.join(output_dir, 'shim_stubs.cpp')

    aliases = ['WEAK(__shim_syscall)']
    for system_call in system_calls:
        aliases.append(f'WEAK(__shim_{system_call.function})')

    content = {'content': '\n'.join(aliases)}
    generate_cpp_file(src_template, src_path, content)


def generate(libc, arch, mode, output_dir, optional_path_to_json):
    '''Generates parts of the shim layer.'''
    system_calls = load_arch_syscalls(libc, arch, optional_path_to_json)
    shimmed_system_calls = [sc for sc in system_calls if not sc.forward]
    # System call partitioning
    partitions = partition.partition_system_calls(shimmed_system_calls)
    if mode == 'inc':
        generate_headers(partitions, output_dir)
    elif mode == 'shims':
        generate_shims_sources(system_calls, partitions, output_dir)
    elif mode == 'wrappers':
        generate_wrappers_sources(system_calls, partitions, output_dir)
    elif mode == 'stub':
        generate_stub(system_calls, output_dir)
    else:
        raise ValueError(f'Invalid mode requested: "{mode}"')


def entry():
    '''Entry point of the shim generator.'''
    if len(sys.argv) not in {5, 6} or sys.argv[3] not in [
        'inc',
        'shims',
        'wrappers',
        'stub',
    ]:
        sys.stderr.write(
            f'Usage: {sys.argv[0]} <libc name> <architecture name> <inc|shims|wrappers|stub> <output directory> [path to json]\n'
        )
        sys.exit(1)
    generate(
        sys.argv[1],
        sys.argv[2],
        sys.argv[3],
        path.abspath(sys.argv[4]),
        path.abspath(sys.argv[5]) if len(sys.argv) == 6 else None,
    )
