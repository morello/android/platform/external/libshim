#!/usr/bin/env python3
#
# Copyright (c) 2020-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#

import re
from enum import Enum

import config
import partition


class SyscallKind(Enum):
    '''The system call is safe to be called directly'''

    FORWARD = 1
    '''The system call returns a pointer which has to be handled with care.'''
    RETURNS_POINTER = 2
    '''The system call has at least one pointer in its parameter list.'''
    POINTER_IN_PARAMS = 3
    '''The system call needs to be shimmed manually.'''
    NEEDS_SHIM = 4


class Syscall:
    '''Class used to represent an system call.'''

    re_decl = re.compile(
        r'^(?P<retval>[\w*]+?)\s+(?P<name>\w+?)\s*\(\s*(?P<params>.+?)\s*\)\s*;$'
    )
    re_array = re.compile(r'\[[0-9]+\]')

    @classmethod
    def from_dict(cls, env):
        '''Creates a new instance from a dictionary.'''
        decl_str = env['decl'].strip()
        decl = re.sub(r'\s\s+', ' ', decl_str)
        decl_match = re.fullmatch(cls.re_decl, decl)
        if decl_match is None:
            raise ValueError(f'Invalid system call declaration: "{decl_str}"')

        retval = decl_match.group('retval')
        name = decl_match.group('name')
        params = [p.strip() for p in decl_match.group('params').split(',')]

        if len(params) == 1 and params[0] == 'void':
            params.pop()

        param_list = "".join(['(', ', '.join(params), ')'])

        if len(params) != 0:
            is_variadic = params[-1] == '...'
            if '...' in params[:-1]:
                raise ValueError(
                    f'Invalid parameters list - unexpected "...": "{decl_str}"'
                )
        else:
            is_variadic = False

        function = env.get('symbol', name)
        aliases = env['aliases'] if 'aliases' in env else []
        aliases = [
            alias if '(' in alias else f'{retval} {alias}{param_list}'
            for alias in aliases
        ]
        custom = env.get('custom', False)
        kind = cls.get_syscall_kind(name, retval, decl, param_list, custom)

        return Syscall(
            {
                'name': name,
                'function': function,
                'aliases': aliases,
                'params': params,
                'param_list': param_list,
                'decl': decl,
                'nr': f'__NR_{name}',
                'retval': retval,
                'returns_pointer': '*' in retval,
                'kind': kind,
                'forward': kind == SyscallKind.FORWARD,
                'hidden': function.startswith('__'),
                'noreturn': env.get('noreturn', False),
                'custom': custom,
                'variadic': is_variadic,
                'partition': None,
                'libc': env.get('libc', None),
                'arch': env.get('arch', None),
                'cap_own_check': env.get('cap_own_check', False),
            }
        )

    def __init__(self, props):
        '''Initializes a new instance.'''
        self.__props = props

    def __getattr__(self, attr):
        '''Mimic attribute-like access: syscall.decl, etc.
        This is the feature we would need from @dataclass which is only
        available from python 3.7.
        '''
        return self.__props[attr]

    def set_partition(self, partition):
        '''Sets the partition to which this system call belongs to.'''
        self.partition = partition

    @property
    def signature(self) -> str:
        '''Returns the signature (declaration) of this system call.'''
        return self.decl

    @property
    def man_page(self) -> str:
        '''Returns a link to the man page of this system call.'''
        return f'http://man7.org/linux/man-pages/man2/{self.name}.2.html'

    @classmethod
    def get_syscall_kind(cls, name, retval, decl, param_list, custom) -> SyscallKind:
        '''Decides if a system call can be directly forwarded or not.'''
        for struct in config.SHIMMED_STRUCTURES:
            if struct in decl:
                return SyscallKind.NEEDS_SHIM

        if custom:
            return SyscallKind.NEEDS_SHIM

        # TODO: There is some concern this is not correct here.
        # System calls which return pointers should be handled separate anyway.
        # Therefore we shouldn't even get here and should bail out at line 94.
        if '*' in decl or re.search(cls.re_array, param_list):
            return SyscallKind.POINTER_IN_PARAMS
        elif '*' in retval:
            return SyscallKind.RETURNS_POINTER

        return SyscallKind.FORWARD
