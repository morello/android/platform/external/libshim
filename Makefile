# Copyright (c) 2021 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

# Do not override this in $(CONFIG)
LIBSHIM_DIR_ = $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

LIBSHIM_BUILD_DIR = $(LIBSHIM_DIR_)/build
LIBSHIM_GEN_DIR = $(LIBSHIM_BUILD_DIR)/gen
LIBSHIM_GEN_DIR_SRC = $(LIBSHIM_GEN_DIR)/src
LIBSHIM_GEN_DIR_INC = $(LIBSHIM_GEN_DIR)/inc

CC = clang
CXX = clang++
AR = llvm-ar
RANLIB = llvm-ranlib
ARCH = arm64
LIBC_PATH = $(LIBSHIM_DIR_)/../../musl
LIBC = $(notdir $(LIBC_PATH))
CFLAGS =
CXXFLAGS =
FLAGS_arm64 =
FLAGS_x86_64 =
FLAGS_morello = -march=morello+c64 -mabi=purecap

LIBSHIM_JSON_PATH ?= $(LIBSHIM_DIR_)/syscalls.json

COMMON_FLAGS = \
    -O2 \
    -fPIC \
    -ffunction-sections \
    -fdata-sections \
    -fno-stack-protector \
    -ffreestanding \
    -Werror \
    -Wall \
    -Wextra \
    -Wunused \
    -Wno-deprecated-declarations \
    -Wimplicit-fallthrough \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Werror=pointer-to-int-cast \
    -Werror=int-to-pointer-cast \
    -Werror=type-limits \
    -Wcheri-pedantic \

DEFINES = \
    -DLIBSHIM_LIBRARY_BUILD=1 \
    -DLIBSHIM_ZERO_DDC=0 \

ifeq ($(KERNEL_HEADER_INCLUDES),)
KERNEL_HEADER_INCLUDES := $(shell ARCH=$(ARCH) $(LIBSHIM_DIR_)/kernel_includes.sh)
ifneq (,$(findstring ERROR,$(KERNEL_HEADER_INCLUDES)))
    $(error Failed to detect kernel header paths)
endif
endif

ifeq ($(LIBC),musl)
INC_DIRS_LIBC_HEADERS ?= -isystem $(LIBC_PATH)/include $(KERNEL_HEADER_INCLUDES)
else
INC_DIRS_LIBC_HEADERS ?= -isystem $(LIBC_PATH)/include
endif

INC_DIRS_ = \
    -I$(LIBSHIM_DIR_)/include \
    -I$(LIBSHIM_GEN_DIR_INC) \
    $(INC_DIRS_LIBC_HEADERS) \
    $(INC_DIRS) \

CFLAGS_ = \
    $(COMMON_FLAGS) \
    $(FLAGS_$(ARCH)) \
    $(DEFINES) \
    $(INC_DIRS_LIBC_HEADERS) \
    $(INC_DIRS_) \
    -std=gnu99 \
    $(CFLAGS) \

ifeq ($(LIBC),musl)
CFLAGS_ += -nostdlibinc
endif

CXXFLAGS_ = \
    $(COMMON_FLAGS) \
    $(FLAGS_$(ARCH)) \
    $(DEFINES) \
    $(INC_DIRS_) \
    -fno-exceptions \
    -std=c++11 \
    -nostdlibinc \
    $(CXXFLAGS) \

# Libshim's inline assembly snippets are safe with CHERIseed Sanitizer
ifneq (,$(findstring -fsanitize=cheriseed,$(CFLAGS_)))
CFLAGS_ += -mllvm -cheriseed-no-inline-asm
endif
ifneq (,$(findstring -fsanitize=cheriseed,$(CXXFLAGS_)))
CXXFLAGS_ += -mllvm -cheriseed-no-inline-asm
endif

SRCS = $(wildcard $(LIBSHIM_DIR_)/src/*.cpp $(LIBSHIM_DIR_)/src/*/*.cpp)
GENERATOR_SRCS = \
    $(wildcard $(LIBSHIM_DIR_)/tools/* $(LIBSHIM_DIR_)/tools/templates/*)
OBJS = $(SRCS:$(LIBSHIM_DIR_)/%.cpp=$(LIBSHIM_BUILD_DIR)/%.o)

GEN_SRCS_ = \
        shims.cpp \
        syscall.cpp \

GEN_INCS_ = shim_gen.h

GEN_SRCS = $(GEN_SRCS_:%=$(LIBSHIM_GEN_DIR_SRC)/%)
GEN_INCS = $(GEN_INCS_:%=$(LIBSHIM_GEN_DIR_INC)/%)
GEN_OBJS = $(patsubst %.cpp,%.o,$(patsubst %.S,%.o,$(GEN_SRCS)))

OBJ_DIRS = $(sort $(patsubst %/,%,$(dir $(OBJS) $(GEN_OBJS))))

all: $(LIBSHIM_BUILD_DIR)/libshim.a

clean:
	rm -rf $(LIBSHIM_BUILD_DIR)

$(OBJ_DIRS):
	[ -d $@ ] || mkdir -p $@

$(GEN_SRCS)&: $(LIBSHIM_JSON_PATH) $(GENERATOR_SRCS) $(LIBSHIM_DIR_)/tools/main.py
	$(LIBSHIM_DIR_)/tools/main.py $(LIBC) $(ARCH) shims $(LIBSHIM_GEN_DIR_SRC) $<

$(GEN_INCS): $(LIBSHIM_JSON_PATH) $(GENERATOR_SRCS) $(LIBSHIM_DIR_)/tools/main.py
	$(LIBSHIM_DIR_)/tools/main.py $(LIBC) $(ARCH) inc $(LIBSHIM_GEN_DIR_INC) $<

$(GEN_SRCS) $(GEN_INCS): | $(OBJ_DIRS)
$(OBJS) $(GEN_OBJS): $(GEN_SRCS) $(GEN_INCS)

$(LIBSHIM_BUILD_DIR)/libshim.a: $(OBJS) $(GEN_OBJS)
	rm -f $@
	$(AR) rc $@ $^
	$(RANLIB) $@

$(LIBSHIM_BUILD_DIR)/%.o: $(LIBSHIM_DIR_)/%.cpp
	$(CXX) $(CXXFLAGS_) -c -o $@ $<
$(LIBSHIM_BUILD_DIR)/%.o: $(LIBSHIM_DIR_)/%.c
	$(CC) $(CFLAGS_) -c -o $@ $<
$(LIBSHIM_BUILD_DIR)/%.o: $(LIBSHIM_DIR_)/%.S
	$(CC) $(CFLAGS_) -c -o $@ $<

%.o: %.cpp
	$(CXX) $(CXXFLAGS_) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS_) -c -o $@ $<
%.o: %.S
	$(CC) $(CFLAGS_) -c -o $@ $<
