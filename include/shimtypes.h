/*
 * Copyright (c) 2020-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cheriintrin.h>
#include <inttypes.h>
#include <sys/mman.h>
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
#include <sys/cheri.h>
#endif  // LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED

#include "shimconfig.h"
#include "stl/std.h"

namespace morello {
namespace shim {

// Type definitions for arguments passed to a shim function.
// Depending on configuration this might be different.
//  * uintptr_shim_t: the local pointer width the shim is translating from.
//  * uintptr_target_t: the target pointer width the shim is translating to.
#if defined(__aarch64__) || defined(__x86_64__)
// Shim translates into 64 bits from either 64 or 128 + 1 (morello) bits.
using uintptr_target_t = uint64_t;
#else
#error "Unrecognized architecture"
#endif

#ifdef __CHERI_PURE_CAPABILITY__
// DDC is saved, and then maybe zeroed, during libc init. This capability
// is used to derive capabilities in libshim instead.
extern "C" uintcap_t __shim_data_cap;
#endif

// This class is used to derive valid capabilities.
struct capability {
  // Derives a capability without executable permissions.
  template <typename T>
  static T build_no_exec(uintptr_t value) noexcept {
    return build<T>(value, CHERI_PERM_EXECUTE);
  }

  // Derives a capability without any write permissions.
  template <typename T>
  static T build_no_write(uintptr_t value) noexcept {
    return build<T>(value, CHERI_PERM_STORE | CHERI_PERM_STORE_CAP | CHERI_PERM_STORE_LOCAL_CAP);
  }

  // Derives a capability and optionally clears some of its permissions.
  template <typename T>
  static T build(uintptr_t value, unsigned int perms_to_clear) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
    uintcap_t cap = __shim_data_cap;
    cap = cheri_address_set(cap, value);
    cap = cheri_perms_clear(cap, perms_to_clear);
    return reinterpret_cast<T>(cap);
#else
    (void)perms_to_clear;
    return reinterpret_cast<T>(value);
#endif
  }
};

// This is the input type which is passed as arguments to the shim layer.
//
// The behavior of uintptr_shim_t is twofold: argument N of a shimmed system
// call can either be treated as a pointer or an integer-like value.
// The storage for such is uintptr_t which is defined to be large enough to hold
// a pointer value. In case of the supported architectures (see above) uintptr_t
// is also sufficiently large enough to hold an unsigned long value which is
// the de facto type passed to a system call in general.
class uintptr_shim_t {
 public:
  uintptr_shim_t() : ptr_{ 0 } {
  }

  // Constructible from pointer types.
  //
  // Note: tag bit is inherited in pure capability ABI.
  template <typename F, typename stl::enable_if<stl::is_pointer<F>::value, int>::type = 0>
  uintptr_shim_t(F from) noexcept : ptr_{ reinterpret_cast<uintptr_t>(from) } {
  }

  // Constructible from integer types.
  //
  // Note: produces an invalid capability in pure capability ABI.
  template <typename F, typename stl::enable_if<stl::is_integral<F>::value, int>::type = 0>
  uintptr_shim_t(F from) noexcept : ptr_{ static_cast<uintptr_t>(from) } {
  }

  // Constructible from nullptr_t.
  uintptr_shim_t(stl::nullptr_t) noexcept : ptr_{ 0 } {
  }

#ifdef __CHERI_PURE_CAPABILITY__
  // Constructible from uintcap_t.
  //
  // Note: tag bit is inherited in pure capability ABI.
  uintptr_shim_t(uintcap_t from) noexcept : ptr_{ from } {
  }
#elif defined(__CHERI__)
  // Constructible from uintcap_t.
  uintptr_shim_t(uintcap_t from) noexcept : ptr_{ cheri_address_get(from) } {
  }
#endif

  // Some system calls return pointers, e.g. mmap().
  // The return value is a uintptr_shim_t but it is far from a valid capability
  // in pure capability ABI because kernel is A64.
  // This member function tries to construct an unbounded capability.
  uintptr_shim_t to_valid_data_pointer_unbounded() noexcept {
    // System calls which return pointers indicate failure with MAP_FAILED.
    if (*this != MAP_FAILED) {
      ptr_ = capability::build<uintptr_t>(ptr_, 0);
    }

    return *this;
  }

  // Some system calls return pointers, e.g. mmap().
  // The return value is a uintptr_shim_t but it is far from a valid capability
  // in pure capability ABI because kernel is A64.
  // This member function tries to construct a valid and bounded capability.
  // VMEM permission is not cleared here.
  uintptr_shim_t to_root_pointer(size_t size) noexcept {
    if (*this != MAP_FAILED) {
      to_valid_data_pointer_unbounded();
#ifdef __CHERI_PURE_CAPABILITY__
      ptr_ = cheri_bounds_set(ptr_, size);
#endif /* __CHERI_PURE_CAPABILITY__ */
    }

    return *this;
  }

  // Some system calls return pointers, e.g. mmap().
  // The return value is a uintptr_shim_t but it is far from a valid capability
  // in pure capability ABI because kernel is A64.
  // This member function tries to construct a valid and bounded capability,
  // and clears the VMEM permission.
  uintptr_shim_t to_valid_data_pointer(size_t size) noexcept {
    if (*this != MAP_FAILED) {
      to_root_pointer(size);
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED && defined(__CHERI_PURE_CAPABILITY__)
      ptr_ = cheri_perms_clear(ptr_, CHERI_PERM_SW_VMEM);
#endif /* LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED && __CHERI_PURE_CAPABILITY__ */
    }

    return *this;
  }

  // Implicit conversion operator into pointer types.
  //
  // Note: tag bit is inherited in pure capability ABI.
  template <typename T, typename stl::enable_if<stl::is_pointer<T>::value, int>::type = 0>
  operator T() const noexcept {
    return reinterpret_cast<T>(ptr_);
  }

  // Implicit conversion operator into integer types through size_t.
  operator size_t() const noexcept {
    return static_cast<size_t>(ptr_);
  }

  // Compares two uintptr_shim_t objects and returns true if they are the same, false otherwise.
  //
  // Note: in morello only the integer part (lower 64 bits) is considered.
  bool operator==(const uintptr_shim_t& other) const noexcept {
    return ptr_ == other.ptr_;
  }

  // Returns true if this is considered to be a valid pointer, otherwise false.
  //
  // A pointer is not valid in two cases:
  //   - if it is in the range [0, kMinPageSize) or
  //   - if its 55th bit is set.
  //
  // In addition, morello requires the tag bit to be set.
  bool is_valid_pointer() const noexcept {
    static const size_t kMinPageSize = 4096;

#ifdef __CHERI_PURE_CAPABILITY__
    const bool is_tag_set = cheri_tag_get(ptr_);
#else
    const bool is_tag_set = true;
#endif
    const ptraddr_t address = static_cast<ptraddr_t>(ptr_);
    return (is_tag_set && (address >= kMinPageSize) &&
            ((address & (static_cast<ptraddr_t>(1) << 55)) == 0));
  }

  // Rounds a value up to the next kMinPageSize (4096 bytes).
  uintptr_shim_t round_to_page_size() const noexcept {
    // TODO: use AT_PAGESZ
    static const size_t kMinPageSize = 4096;
    const size_t value = ptr_;
    return uintptr_shim_t{ (value + (kMinPageSize - 1)) & ~(kMinPageSize - 1) };
  }

 protected:
  // Shim translates from either 64 or 128 + 1 (morello) bits because of the
  // conditional compilation around line 21.
  uintptr_t ptr_;
};

// Some static asserts just to make sure the code for uintptr_shim_t is correct.
// Should any of this change in the future the implementation above should be
// revised.
static_assert(sizeof(void*) <= sizeof(uintptr_shim_t),
              "Bad storage type: uintptr_shim_t should be able to store a pointer value.");
static_assert(sizeof(unsigned long) <= sizeof(uintptr_shim_t),
              "Bad storage type: uintptr_shim_t should be able to store an unsigned long value.");

static_assert(stl::is_pointer<uintptr_target_t>::value == false,
              "Got unexpected pointer type: uintptr_target_t");
static_assert(stl::is_integral<uintptr_target_t>::value == true,
              "Got unexpected integral type: uintptr_target_t");

#ifdef __CHERI__
static_assert(stl::is_pointer<uintcap_t>::value == false, "Got unexpected pointer type: uintcap_t");
static_assert(stl::is_integral<uintcap_t>::value == true,
              "Got unexpected integral type: uintcap_t");
#endif

// Representation of a process identifier.
struct Pid {
  Pid(uintptr_shim_t pid) noexcept : value{ static_cast<decltype(Pid::value)>(pid) } {
  }

  static Pid Invalid() {
    static constexpr int kInvaliPid = -1;
    return Pid{ kInvaliPid };
  }

  const int value;
};

// Representation of a process file descriptor.
struct PidFd {
  PidFd(uintptr_shim_t fd) noexcept : value{ static_cast<decltype(PidFd::value)>(fd) } {
  }

  const int value;
};

}  // namespace shim
}  // namespace morello
