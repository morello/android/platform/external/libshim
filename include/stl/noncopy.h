/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace morello {
namespace shim {
namespace stl {

// Parent class which prevents copy.
class NonCopy {
 public:
  NonCopy() = default;
  NonCopy(const NonCopy&) = delete;
  NonCopy& operator=(const NonCopy&) = delete;
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
