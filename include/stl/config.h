/*
 * Copyright (c) 2020-2021 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "shimconfig.h"

namespace morello {
namespace shim {
namespace stl {

// Exit code upon panic.
static constexpr int kPanicExitCode = 1;

// Global common default threshold for heap allocations.
static constexpr size_t kDefaultHeapThreshold = LIBSHIM_DEFAULT_HEAP_THRESHOLD;

// Global common default registry size.
static constexpr size_t kDefaultRegistrySize = LIBSHIM_DEFAULT_REGISTRY_SIZE;

}  // namespace stl
}  // namespace shim
}  // namespace morello
