/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <stdlib.h>

#include "array.h"
#include "config.h"
#include "lock.h"
#include "noncopy.h"
#include "panic.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace stl {

// Template class which encapsulates a variable-sized array on the heap.
//
// Accessing an element in this registry or re-allocation of the internal
// buffer is done in a context guarded with a mutex-like locking mechanism.
template <typename T, const size_t SIZE = kDefaultRegistrySize>
class Registry : NonCopy {
 public:
  // Constructor to create a new instance.
  explicit Registry() noexcept : array_{ 0 } {
    // Don't lock here: it is undefined behavior to race for
    // a being-constructed instance.
    access_element(default_size() - 1);
  }

  // Array subscript operator.
  T operator[](size_t index) noexcept {
    return get_element(index);
  }

  // Updates the value at a given index and returns the old value.
  T update(size_t index, T value) noexcept {
    return set_element(index, value);
  }

  // Returns the number of elements this registry stores.
  //
  // Note: the size is variable and should not be trusted in a multithreaded
  // environment.
  size_t size() const noexcept {
    return array_.size();
  }

  // Prints the contents of the registry.
  void display() noexcept {
    // Make this operation atomic.
    stl::Lock lock{ guard_ };

    stl::error_message("Registry contents [address = %p]:\n", this);

    size_t index = 0;
    for (auto& item : array_.iter()) {
      if (stl::is_class<T>::value) {
        stl::error_message("  [% 3d] ", index);
        item.display(STDERR_FILENO);
      } else {
        stl::error_message("  [% 3d] %p\n", index, item);
      }
      ++index;
    }
  }

  // Returns the default size of a new instance.
  static constexpr size_t default_size() noexcept {
    return kDefaultRegistrySize;
  }

 protected:
  // Returns an element in this registry.
  T get_element(size_t index) noexcept {
    // Make this operation atomic.
    stl::Lock lock{ guard_ };
    return access_element(index);
  }

  // Sets the element at index to the given value.
  T set_element(size_t index, T value) noexcept {
    // Make this operation atomic.
    stl::Lock lock{ guard_ };
    T& element = access_element(index);
    const T old = element;
    element = value;
    return old;
  }

  // Returns a reference to the element at the given index.
  //
  // Checks if the registry needs resize and if so, it does the resize.
  // Note: this method must be called from a locked context.
  // The only exceptions are the constructors and *_unsafe members.
  T& access_element(size_t index) noexcept {
    if (index >= array_.size()) {
      // Need to re-allocate array.
      const size_t new_size = (index + default_size()) & ~(default_size() - 1);
      stl::Array<T, SIZE> new_array{ new_size };
      // Copy the first part of the registry into the new array.
      memcpy(new_array.data(), array_.data(), sizeof(T) * array_.size());
      // Initialize the rest of the new items with the T's default constructor.
      for (size_t index = array_.size(); index < new_size; ++index) {
        new_array.data()[index] = T{};
      }
      // Finally, move the temporary array.
      array_ = stl::move(new_array);
    }

    return array_.data()[index];
  }

  LockGuard guard_;
  Array<T, SIZE> array_;
};

// Template class which encapsulates a variable-sized array on the heap.
//
// Elements are always allocated with mmap/munmap. Use this class if there is a
// need for a data-race-free static global variable.
//
// Note: 'enable_if' below is not SFINAE, it will actually cause error when
// T is not 'is_zero_init'.
template <typename T, typename enable_if<is_zero_init<T>::value, int>::type = 0>
class StaticRegistry : NonCopy {
 public:
  using RegistryTy = stl::Registry<T, 0>;

  // Returns a reference to the registry.
  RegistryTy& operator()() noexcept {
    return reinterpret_cast<RegistryTy&>(memory_);
  }

 protected:
  // The reserved memory for the registry.
  char memory_[sizeof(RegistryTy)] __attribute__((aligned(alignof(RegistryTy))));
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
