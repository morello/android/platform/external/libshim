/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <signal.h>
#include <sys/syscall.h>

#include "noncopy.h"
#include "panic.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace stl {

// According to man page [1], 'sigsetsize' argument should have a fixed size
// and be equal to sizeof(kernel_sigset_t). To be precise, kernel expects that
// sigsetsize is '8'.
//
// This works well with bionic, where sizeof(sigset_t) is '8'.
// However, this expectation breaks with both musl and glibc which use
// 128-byte sigset_t for some reason.
//
// kSigSetSize is meant to solve this via calculating the size based on _NSIG.
// _NSIG is defined as 'the highest signal value + 1', therefore truncating /8
// is the de facto way to calculate 'sigsetsize' according to different libc
// implementations.
//
// [1] https://man7.org/linux/man-pages/man2/rt_sigprocmask.2.html
static constexpr size_t kSigSetSize = _NSIG / 8;
// Define an appropriate type for sigset_t to be used elsewhere.
using shim_sigset_t = unsigned long;

// Some defensive checks.
static_assert(kSigSetSize == 8, "Unexpected 'sigsetsize'");
static_assert(sizeof(shim_sigset_t) >= kSigSetSize, "sizeof(shim_sigset_t) < 8");
static_assert(sizeof(sigset_t) >= sizeof(shim_sigset_t),
              "sizeof(sigset_t) < sizeof(shim_sigset_t)");

// RAII class to block all signals on the current thread temporarily.
class ScopedSignalBlock final : NonCopy {
 public:
  // Constructor which blocks all signals.
  ScopedSignalBlock() noexcept {
    sigset_t mask;
    sigfillset(&mask);
    // Do a direct system call.
    // This call can't fail.
    long ret =
        LIBSHIM_SVC_NC(SIG_SETMASK, &mask, &original_mask_, kSigSetSize, 0, 0, __NR_rt_sigprocmask);
    if (ret != 0) {
      stl::panic("rt_sigprocmask returned %d.", ret);
    }
  }

  // Destructor which restores the original signal mask.
  ~ScopedSignalBlock() noexcept {
    // Do a direct system call.
    // This call can't fail.
    long ret =
        LIBSHIM_SVC_NC(SIG_SETMASK, &original_mask_, 0, kSigSetSize, 0, 0, __NR_rt_sigprocmask);
    if (ret != 0) {
      stl::panic("rt_sigprocmask returned %d.", ret);
    }
  }

 private:
  // Use original sigset_t because it is not a problem that it might be bigger
  // than kSigSetSize. Besides, there is no need to re-implement sigfillset()
  // above.
  sigset_t original_mask_;
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
