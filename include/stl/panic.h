/*
 * Copyright (c) 2020-2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <stdio.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "shim.h"
#include "shimconfig.h"
#include "stl/config.h"

namespace morello {
namespace shim {
namespace stl {

[[noreturn]] static inline void panic(const char* message) noexcept;

static constexpr char kPrefix[] = "[LIBSHIM] ";
static constexpr size_t kPrefixLen = sizeof(kPrefix) - 1;  // Excludes terminating '\0'
static constexpr char kTruncated[] = "<truncated>\n";
static constexpr size_t kTruncatedLen = sizeof(kTruncated) - 1;  // Excludes terminating '\0'

// An arbitrary system call argument.
struct Arg final {
  // Arguments are identified with 1-based indexing.
  static constexpr size_t kInvalidIdx = -1;

  explicit Arg(uintptr_shim_t value) noexcept : Arg(value, kInvalidIdx, kInvalidIdx) {
  }

  explicit Arg(uintptr_shim_t value, size_t idx) noexcept : Arg(value, idx, kInvalidIdx) {
  }

  explicit Arg(uintptr_shim_t value, size_t idx, size_t sub_idx) noexcept
      : value{ value }, idx{ idx }, sub_idx{ sub_idx } {
  }

  explicit Arg(Arg& arg, uintptr_shim_t value) noexcept : Arg(value, arg.idx, arg.sub_idx) {
  }

  // The value of an argument.
  uintptr_shim_t value;
  // The identifier of the argument. For example, '6' means the 6th argument.
  size_t idx;
  // The sub identifier of the argument. For example, '4' means '(the 6th argument)[4]).
  size_t sub_idx;
};

// Helper to write a formatted message into a user-supplied buffer.
// The buffer can be written to a user-specified file descriptor.
struct FormattedString {
  explicit FormattedString(char* buffer, size_t buffer_size) noexcept
      : buffer_(buffer), capacity_(buffer_size) {
    if (capacity_ < min_required_capacity()) {
      panic("FormattedString: capacity is too small\n");
    }

    // Add prefix.
    memcpy(buffer_, kPrefix, kPrefixLen);
    reset();
  }

  // Works like snprintf(), except this one prints into the buffer.
  template <typename... Args>
  int write_buf(const char* format, Args... args) noexcept {
    const size_t max_len = capacity_ - used_bytes_;

    int length = 0;
    if (max_len > 0) {
#if LIBSHIM_PRETTY_PRINT
      length = snprintf(&buffer_[used_bytes_], max_len, format, args...);
#else
      // Don't count terminating '\0', just like snprintf().
      length = static_cast<int>(strlen(format));
      memcpy(&buffer_[used_bytes_], format, length <= max_len ? length : max_len);
#endif
    }

    if (length < 0) {
      has_error_ = true;
    } else if ((max_len <= length) && !truncated_) {
      used_bytes_ = capacity_;
      memcpy(&buffer_[capacity_ - kTruncatedLen], kTruncated, kTruncatedLen);
      truncated_ = true;
    } else {
      used_bytes_ += length;
    }

    return length;
  }

  // Write the formatted data using the specified file descriptor.
  void write_fd(int fd) noexcept {
    if (!has_error_) {
      LIBSHIM_SVC_NC(fd, buffer_, used_bytes_, 0, 0, 0, __NR_write);
    } else {
      static constexpr char kFailedMessage[] = "[LIBSHIM] Failed to print message\n";
      static constexpr size_t kFailedMessageLen = sizeof(kFailedMessage) - 1;  // Minus '\0'
      LIBSHIM_SVC_NC(fd, &kFailedMessage[0], kFailedMessageLen, 0, 0, 0, __NR_write);
    }

    reset();
  }

  // Returns the minimum capacity a supplied buffer must be able to hold.
  static constexpr size_t min_required_capacity() noexcept {
    return kPrefixLen + kTruncatedLen;
  }

 private:
  // Resets the instance.
  void reset() noexcept {
    used_bytes_ = kPrefixLen;
    truncated_ = false;
    has_error_ = false;
  }

  // The user-supplied buffer.
  char* const buffer_;
  // Capacity of the user-supplied buffer.
  const size_t capacity_;
  // Number of used bytes of the user-supplied buffer.
  size_t used_bytes_;
  // Marks that the formatted data was truncated or not.
  bool truncated_;
  // Records that an error happened.
  bool has_error_;
};

// Helper to create a formatted message.
//
// This is similar to FormattedString, however, an instance of this has a
// reasonably sized fixed-length buffer.
template <const size_t SIZE = 512>
struct BufferedFormattedString final : protected FormattedString {
  BufferedFormattedString() : FormattedString(&buffer_[0], SIZE) {
  }

  BufferedFormattedString& operator<<(const char* str) noexcept {
    write_buf("%s", str);
    return *this;
  }

  BufferedFormattedString& operator<<(uintptr_shim_t arg) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
    write_buf("%#p", arg);
#else
    write_buf("%p", arg);
#endif
    return *this;
  }

  BufferedFormattedString& operator<<(Arg arg) noexcept {
    if (Arg::kInvalidIdx == arg.sub_idx) {
      write_buf("argument %d", arg.idx);
    } else {
      write_buf("argument %d[%d]", arg.idx, arg.sub_idx);
    }
    return *this;
  }

  // Expose inherited parent functionality.
  using FormattedString::write_fd;

 protected:
  // Internal buffer.
  char buffer_[SIZE];
};

// Writes a formatted string to fd with an upper bound on the length.
template <typename... Args>
static inline void dnprintf(int fd, size_t size, const char* format, Args... args) noexcept {
  const size_t buffer_size = FormattedString::min_required_capacity() + size;
  char buffer[buffer_size];
  FormattedString formatted_string{ &buffer[0], buffer_size };
  formatted_string.write_buf(format, args...);
  formatted_string.write_fd(fd);
}

// Prints a message to stderr.
template <typename... Args>
static inline void error_message(const char* format, Args... args) noexcept {
  dnprintf(STDERR_FILENO, 256, format, args...);
}

// Prints a message to stderr and exits with kPanicExitCode.
template <typename... Args>
[[noreturn]] void panic(const char* format, Args... args) noexcept {
  error_message(format, args...);
  for (;;) {
    LIBSHIM_SVC_NC(kPanicExitCode, 0, 0, 0, 0, 0, __NR_exit);
  }
}

// Prints a message to stderr and exits with kPanicExitCode.
[[noreturn]] static inline void panic(const char* message) noexcept {
  panic("%s", message);
}

// Prints a message to stderr.
//
// If LIBSHIM_ERROR_PANICS is set also exits with kPanicExitCode.
template <typename... Args>
void error(const char* format, Args... args) noexcept {
#if LIBSHIM_ERROR_PANICS
  panic(format, args...);
#else
  error_message(format, args...);
#endif
}

}  // namespace stl
}  // namespace shim
}  // namespace morello
