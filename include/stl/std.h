/*
 * Copyright (c) 2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

// Do not format this file at all.
// clang-format off

/*
 * This file implements a few helpers from
 *    - cstddef:     std::nullptr_t
 *    - type_traits: std::integral_constant
 *                   std::true_type
 *                   std::false_type
 *                   std::remove_cv
 *                   std::enable_if
 *                   std::is_pointer
 *                   std::is_integral
 *                   std::is_class
 *                   std::remove_reference
 *    - utility:     std::move
 *
 * In addition, there are some traits defined for libshim only:
 *    - is_zero_init
 *
 * It's sole purpose is to make libshim independent of C++ headers.
 *
 * This implementation uses '__is_class()' which should be available in
 * recent clang and gcc.
 *
 * Note that libshim's unit tests still depend on C++ runtime.
 */

namespace morello {
namespace shim {
namespace stl {

// std::nullptr_t
typedef decltype(nullptr) nullptr_t;

// std::integral_constant
template <class T, T v>
struct integral_constant {
  static constexpr T value = v;
  using value_type = T;
  using type = integral_constant;
  constexpr operator value_type() const noexcept {
    return value;
  }
};

// std::true_type
typedef integral_constant<bool, true> true_type;

// std::false_type
typedef integral_constant<bool, false> false_type;

// std::remove_cv
template <class T> struct remove_cv                   { typedef T type; };
template <class T> struct remove_cv<const T>          { typedef T type; };
template <class T> struct remove_cv<volatile T>       { typedef T type; };
template <class T> struct remove_cv<const volatile T> { typedef T type; };

// std::enable_if
template <bool, class T = void> struct enable_if {};
template <class T>              struct enable_if<true, T> { typedef T type; };

// std::is_pointer
template <class T> struct __libshim_is_pointer     : public false_type {};
template <class T> struct __libshim_is_pointer<T*> : public true_type {};

template <class T>
struct is_pointer : public __libshim_is_pointer<typename remove_cv<T>::type> {};

// std::is_integral
template <class T> struct __libshim_is_integral                     : public false_type {};
template <>        struct __libshim_is_integral<bool>               : public true_type {};
template <>        struct __libshim_is_integral<char>               : public true_type {};
template <>        struct __libshim_is_integral<unsigned char>      : public true_type {};
template <>        struct __libshim_is_integral<signed char>        : public true_type {};
template <>        struct __libshim_is_integral<short>              : public true_type {};
template <>        struct __libshim_is_integral<unsigned short>     : public true_type {};
template <>        struct __libshim_is_integral<int>                : public true_type {};
template <>        struct __libshim_is_integral<unsigned int>       : public true_type {};
template <>        struct __libshim_is_integral<long>               : public true_type {};
template <>        struct __libshim_is_integral<unsigned long>      : public true_type {};
template <>        struct __libshim_is_integral<long long>          : public true_type {};
template <>        struct __libshim_is_integral<unsigned long long> : public true_type {};
#ifdef __CHERI__
template <>        struct __libshim_is_integral<intcap_t>           : public true_type {};
template <>        struct __libshim_is_integral<uintcap_t>          : public true_type {};
#endif

template <typename T>
struct is_integral : public __libshim_is_integral<typename remove_cv<T>::type> {};

// std::is_class
template <class T> struct is_class : public integral_constant<bool, __is_class(T)> {};

// std::remove_reference
template <class T> struct remove_reference      { typedef T type; };
template <class T> struct remove_reference<T&>  { typedef T type; };
template <class T> struct remove_reference<T&&> { typedef T type; };

// std::move
template <class T>
typename remove_reference<T>::type&& move(T&& t) noexcept {
  return static_cast<typename remove_reference<T>::type&&>(t);
}

/// std::is_const
template<class T> struct is_const          : public false_type {};
template<class T> struct is_const<const T> : public true_type {};

// All the following types and templates are defined for libshim.

// Specialize __libshim_is_zero_init for a class which can be constructed
// safely when it is simply initialized with all zeros.
template <class T> struct __libshim_is_zero_init : public false_type {};

template <typename T>
struct is_zero_init : public __libshim_is_zero_init<typename remove_cv<T>::type> {};

// Use this macro to express if T is zero initialisable.
#define ZERO_CONSTRUCTIBLE(__T) \
  template <> struct morello::shim::stl::__libshim_is_zero_init<__T> : public true_type {};

}  // namespace stl
}  // namespace shim
}  // namespace morello
