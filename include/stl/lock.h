/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <linux/futex.h>
#include <sys/syscall.h>

#include "noncopy.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace stl {

class Lock;

// Wrapper around atomic_int.
class LockGuard final : NonCopy {
 public:
  // Constructor to create a new instance.
  LockGuard() noexcept {
    __atomic_store_n(&flag_, 0, __ATOMIC_RELAXED);
  }

  // Tries to acquire the lock by changing the value from 0 to 1.
  // Returns true if the lock was successfully acquired.
  // Implementation should have semantics equal to
  //    std::atomic_exchange_explicit(&flag_, 1, std::memory_order_acquire)
  bool try_lock() noexcept {
    return (__atomic_exchange_n(&flag_, 1, __ATOMIC_ACQUIRE) == 0);
  }

  // Releases the lock by changing the value from 1 to 0.
  // Implementation should have semantics equal to
  //    std::atomic_exchange_explicit(&flag_, 0, std::memory_order_release);
  //    or
  //    std::atomic_uint32_t.store(0, std::memory_order_release);
  void release() noexcept {
    __atomic_store_n(&flag_, 0, __ATOMIC_RELEASE);
  }

 private:
  typedef uint32_t flag_t;

  // 0: the lock is free.
  // 1: the lock is taken.
  flag_t flag_;

  friend class Lock;
};

// Simple mutex implementation.
class Lock final : NonCopy {
 public:
  // Constructor to create a new instance.
  //
  // Note that the constructor also locks the guard.
  explicit Lock(LockGuard& guard) noexcept : guard_{ guard } {
    while (!guard_.try_lock()) {
      LIBSHIM_FN_CALL_NC(morello::shim::futex, &guard_.flag_, FUTEX_WAIT_PRIVATE, 0, 0, 0, 0,
                         __NR_futex);
    }
  }

  // Destructor to release the guard.
  ~Lock() noexcept {
    guard_.release();
    LIBSHIM_FN_CALL_NC(morello::shim::futex, &guard_.flag_, FUTEX_WAKE_PRIVATE, 1, 0, 0, 0,
                       __NR_futex);
  }

 private:
  LockGuard& guard_;
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
