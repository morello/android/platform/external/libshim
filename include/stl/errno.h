/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <errno.h>

#include "noncopy.h"

namespace morello {
namespace shim {
namespace stl {

// The maximum errno value.
static constexpr int kMaxErrno = 4095;

// Returns true if a value is a valid system call error result, otherwise false.
static inline bool IsSCErrorValue(unsigned long value) {
  return __builtin_expect(
      static_cast<unsigned long>(value) >= static_cast<unsigned long>(-kMaxErrno), 0);
}

// Returns an error value for a system call error.
static inline unsigned long SCReturnError(unsigned long error) {
  return -error;
}

}  // namespace stl
}  // namespace shim
}  // namespace morello
