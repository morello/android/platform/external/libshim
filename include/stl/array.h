/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <assert.h>
#include <stdlib.h>
#include <sys/mman.h>

#include "config.h"
#include "iterator.h"
#include "noncopy.h"

namespace morello {
namespace shim {
namespace stl {

// Template class which encapsulates a fix-sized array which can be sized during instantiation.
//
// This class is useful so that there is only a single type where heap allocation
// might happen in the shim layer.
//
// Below a threshold there is no heap allocation.
// This is to support possible system calls before the global allocator gets initialized.
// However, there are no guarantees that the allocation can not fail.
//
// The threshold is configurable with the 'SIZE' template argument which defaults to
// the global constant kDefaultHeapThreshold.
template <typename T, const size_t SIZE = kDefaultHeapThreshold>
class Array : NonCopy {
 public:
  // Constructor to create a new instance.
  explicit Array(size_t size) noexcept : size_{ size } {
    if (size_ > 0) {
      if (is_on_heap()) {
        items_ = reinterpret_cast<T*>(::mmap64(0, size_ * sizeof(T), PROT_READ | PROT_WRITE,
                                               MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
        if (items_ == MAP_FAILED) {
          panic("Bad alloc\n");
        }
      } else {
        items_ = &array_[0];
      }
    } else {
      items_ = nullptr;
    }
  }

  // Move assignment operator.
  Array& operator=(Array&& other) noexcept {
    if (this != &other) {
      // Release data if it is on heap.
      if ((items_ != nullptr) && is_on_heap()) {
        ::munmap(items_, size_ * sizeof(T));
      }

      // Take the data of other.
      if (other.is_on_heap()) {
        items_ = other.items_;
      } else {
        memcpy(array_, other.array_, sizeof(T) * other.size_);
        items_ = &array_[0];
      }
      size_ = other.size_;

      // Other has no ownership left here.
      other.size_ = 0;
      other.items_ = nullptr;
    }

    return *this;
  }

  // Destructor which can free allocated memory, if necessary.
  ~Array() noexcept {
    if (items_ != nullptr) {
      if (is_on_heap()) {
        ::munmap(items_, size_ * sizeof(T));
      } else {
        assert(items_ == &array_[0]);
      }

      items_ = nullptr;
    }
  }

  // Returns an iterator over the stored items.
  Iterator<T> iter() noexcept {
    return Iterator<T>(data(), size_);
  }

  // Returns a pointer to the contained items.
  T* data() noexcept {
    return items_;
  }

  // Returns the number of stored objects.
  size_t size() const noexcept {
    return size_;
  }

  // Returns the default heap allocation threshold.
  static constexpr size_t default_threshold() {
    return kDefaultHeapThreshold;
  }

  // Returns true if this instance uses heap allocation.
  bool is_on_heap() const noexcept {
    return size_ > SIZE;
  }

 protected:
  size_t size_;
  T* items_;
  T array_[SIZE];
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
