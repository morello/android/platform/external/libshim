/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <stddef.h>

#include "panic.h"

namespace morello {
namespace shim {
namespace stl {

// An iterator template to iterate over an array-like object.
template <typename T>
class Iterator final {
 public:
  // Constructor to create a new instance.
  explicit Iterator(T* items, size_t length) noexcept
      : items_{ items }, current_{ 0 }, length_{ length } {
    if ((items == nullptr) && (length != 0)) {
      panic("Argument 'items' is nullptr but length is not zero.\n");
    }
  }

  // Returns a begin iterator.
  Iterator begin() const noexcept {
    return Iterator<T>(items_, length_);
  }

  // Returns an end iterator.
  const Iterator end() const noexcept {
    auto iterator = Iterator<T>{ nullptr, 0 };
    iterator.current_ = length_;
    return iterator;
  }

  // Advances the iterator by one item using the prefix increment operator.
  Iterator& operator++() noexcept {
    ++current_;
    ++items_;
    return *this;
  }

  // Advances the iterator with an amount using the compound assignment operator.
  Iterator& operator+=(size_t count) noexcept {
    items_ += count;
    current_ += count;
    return *this;
  }

  // Checks if two iterator instances are not equal.
  //
  // Warning: Do not use this operator directly.
  // This is only meant to be used with range-based for loops only.
  bool operator!=(const Iterator<T>& other) const noexcept {
    return current_ != other.current_;
  }

  // Returns the state of the iterator at a given iteration.
  T& operator*() const noexcept {
    if (current_ >= length_) {
      panic("Tried to iterate over the range.\n");
    }
    return *items_;
  }

 private:
  T* items_;
  size_t current_;
  const size_t length_;
};

// An iterator template to iterate over two array-like objects at the same time
// always yielding the logically matching pairs.
//
// This is a shorthand for the following pattern:
//   for (int idx = 0; idx < len; ++idx) {
//     auto l = left[idx];
//     auto r = right[idx];
//     ...
// }
//
// In some languages this is called 'zip()'.
//
// It is the user's responsibility that both 'inputs' and 'outputs' point to
// arrays with at least 'length' size.
template <typename I, typename O>
class ZipIterator final {
 public:
  // Dereferenced state of an iterator instance.
  struct ZipIteration {
    I& input;
    O& output;
  };

  // Constructor to create a new instance.
  explicit ZipIterator(I* inputs, O* outputs, size_t length) noexcept
      : inputs_{ inputs }, outputs_{ outputs }, current_{ 0 }, length_{ length } {
    if (length != 0) {
      if (inputs == nullptr) {
        panic("Argument 'inputs' is nullptr but length is not zero.\n");
      } else if (outputs == nullptr) {
        panic("Argument 'outputs' is nullptr but length is not zero.\n");
      }
    }
  }

  // Returns a begin iterator which can be used to iterate over both 'inputs' and 'outputs' at the
  // same time.
  ZipIterator begin() const noexcept {
    return ZipIterator<I, O>{ inputs_, outputs_, length_ };
  }

  // Returns an end iterator.
  const ZipIterator end() const noexcept {
    auto iterator = ZipIterator<I, O>{ nullptr, nullptr, 0 };
    iterator.current_ = length_;
    return iterator;
  }

  // Advances the iterator by one item using the prefix increment operator.
  ZipIterator& operator++() noexcept {
    ++current_;
    ++inputs_;
    ++outputs_;
    return *this;
  }

  // Checks if two iterator instances are not equal.
  //
  // Warning: Do not use this operator directly.
  // This is only meant to be used with range-based for loops only.
  bool operator!=(const ZipIterator<I, O>& other) const noexcept {
    return current_ != other.current_;
  }

  // Returns the state of the iterator at a given iteration.
  ZipIteration operator*() const noexcept {
    if (current_ >= length_) {
      panic("Tried to iterate over the range.\n");
    }
    return ZipIteration{ .input = *inputs_, .output = *outputs_ };
  }

 private:
  I* inputs_;
  O* outputs_;
  size_t current_;
  const size_t length_;
};

}  // namespace stl
}  // namespace shim
}  // namespace morello
