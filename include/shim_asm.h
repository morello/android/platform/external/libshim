/*
 * Copyright (c) 2020, 2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

// Macros to help creating a function written in assembly.

#define FUNCTION_START(__function) \
  .text;                           \
  .balign 4;                       \
  .globl __function;               \
  .type __function, % function;    \
  __function:                      \
  .cfi_startproc

#define FUNCTION_END(__function) \
  .cfi_endproc;                  \
  .size __function, .- __function;

#define FUNCTION_ALIAS(__function, __alias) \
  .globl __alias;                           \
  .equ __alias, __function

#define GLOBAL_CAP(__variable) \
  .data;                       \
  .balign 16;                  \
  .globl __variable;           \
  .protected __variable;       \
  .type __variable, % object;  \
  __variable:                  \
  .octa 0;                     \
  .size __variable, .- __variable;
