/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

// Needed for __BIONIC__
#include <features.h>

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace handler {

// This namespace contains types and implementations for a more scalable
// mechanism called "handler". Put simple, handlers are extractions of
// self-contained implementations which cope with specific transformations.
//
// For example, consider the case where all ioctl() commands were handled in a
// long switch-case: neither scalable nor readable. See src/ioctl/ for details.
// In contrast, handlers extract parts of a long implementation into more
// readable and maintainable code snippets.
//
// Another advantage of handlers is that if something is not handled
// sufficiently in the shim, the user can provide a custom handler or
// explicitly state which handler to use for a given fd, etc.

// Possible outcomes of a handler call.
enum class HandlerResult {
  // The checks passed but the system call has not been invoked.
  kChecked,
  // The checks passed and the system call has been invoked.
  kHandled,
  // The handler doesn't know what to do and so nothing has been done.
  kNext,
};

// An ioctl() handler function.
using IoctlHandlerFn = HandlerResult (*)(uintptr_shim_t, uintptr_shim_t, uintptr_shim_t,
                                         uintptr_shim_t&, check::Check&);

// Macros improving code readability.
#define LIBSHIM_HANDLER_NAME(__kind, __name) __kind##_##__name##_handler

#define LIBSHIM_IOCTL_HANDLER(__name)                                                             \
  HandlerResult LIBSHIM_HANDLER_NAME(ioctl, __name)(uintptr_shim_t arg1, uintptr_shim_t arg2,     \
                                                    uintptr_shim_t arg3, uintptr_shim_t & retval, \
                                                    check::Check & check)

// Handler declarations
#if defined(__BIONIC__)
LIBSHIM_IOCTL_HANDLER(binder);
#endif
LIBSHIM_IOCTL_HANDLER(netdevice);
LIBSHIM_IOCTL_HANDLER(tty);
LIBSHIM_IOCTL_HANDLER(block);

}  // namespace handler
}  // namespace shim
}  // namespace morello
