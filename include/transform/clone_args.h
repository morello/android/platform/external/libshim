/*
 * Copyright (c) 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct clone_args' layout the shim is translating from.
struct local_clone_args_t {
  uint64_t flags;
  uintptr_t pidfd;
  uintptr_t child_tid;
  uintptr_t parent_tid;
  uint64_t exit_signal;
  uintptr_t stack;
  uint64_t stack_size;
  uintptr_t tls;
  uintptr_t set_tid;
  uint64_t set_tid_size;
  uint64_t cgroup;
};

// The target side 'struct clone_args' layout the shim is translating into.
struct target_clone_args_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_clone_args_t& local,
                              bool) noexcept {
    check.pointer<int>(Arg{ input, local.pidfd });
    check.pointer<pid_t>(Arg{ input, local.child_tid });
    check.pointer<pid_t>(Arg{ input, local.parent_tid });
    check.set_default_code(EINVAL)
        .buffer(Arg{ input, local.stack }, Arg{ input, local.stack_size })
        .reset_default_code();
    check.pointer_to_array<pid_t>(Arg{ input, local.set_tid }, Arg{ input, local.set_tid_size });
  }

  // Transforms from local into target memory layout.
  target_clone_args_t& operator<<(const local_clone_args_t& local) noexcept {
    flags_ = local.flags;
    pidfd_ = local.pidfd;
    child_tid_ = local.child_tid;
    parent_tid_ = local.parent_tid;
    exit_signal_ = local.exit_signal;
    stack_ = local.stack;
    stack_size_ = local.stack_size;
    tls_ = local.tls;
    set_tid_ = local.set_tid;
    set_tid_size_ = local.set_tid_size;
    cgroup_ = local.cgroup;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_clone_args_t& operator>>(local_clone_args_t& local) noexcept {
    // It looks like clone_args is *not* updated by kernel in any way.
    return local;
  }

 private:
  uint64_t flags_;
  target_pointer_t pidfd_;
  target_pointer_t child_tid_;
  target_pointer_t parent_tid_;
  uint64_t exit_signal_;
  target_pointer_t stack_;
  uint64_t stack_size_;
  target_pointer_t tls_;
  target_pointer_t set_tid_;
  uint64_t set_tid_size_;
  uint64_t cgroup_;
};

// Transformation of a single 'struct clone_args*' system call argument.
struct CloneArgs : public InputTransformation<local_clone_args_t, target_clone_args_t> {
  // Constructor to create a new instance.
  explicit CloneArgs(Check& check, Arg cl_args, Arg size) noexcept
      : InputTransformation(check, cl_args) {
    // 'size' must hold the size of the structure.
    check.expect(static_cast<size_t>(size.value) == sizeof(local_clone_args_t),
                 "size argument is invalid", EINVAL);
  }
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
