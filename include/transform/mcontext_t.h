/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/ucontext.h>

#include "base.h"
#include "shim.h"
#include "transform/pointer.h"
#include "transform/stack_t.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'mcontext_' layout the shim is translating from.
using local_mcontext_t_t = mcontext_t;

#if defined(__BIONIC__)
#define SHIM_MCONTEXT_RES1 reserved1
#else
#define SHIM_MCONTEXT_RES1 __reserved1
#endif

// The target side 'ucontext_t' layout the shim is translating into.
struct target_mcontext_t_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_mcontext_t_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_mcontext_t_t& operator<<(const local_mcontext_t_t& local) noexcept {
#if defined(__aarch64__)
    context_ = local;
#elif defined(__x86_64__)
    const auto local_ = reinterpret_cast<const struct sigcontext*>(&local);
    memcpy(reinterpret_cast<uint8_t*>(&gregs_), reinterpret_cast<const uint8_t*>(&local_->r8),
           sizeof(gregs_));
    fpstate_ = local_->fpstate;
    memcpy(reinterpret_cast<uint8_t*>(&reserved1_),
           reinterpret_cast<const uint8_t*>(&local_->SHIM_MCONTEXT_RES1), sizeof(reserved1_));
#endif
    return *this;
  }

  // Transforms from target into local memory layout.
  local_mcontext_t_t& operator>>(local_mcontext_t_t& local) noexcept {
#if defined(__aarch64__)
    local = context_;
#elif defined(__x86_64__)
    auto local_ = reinterpret_cast<struct sigcontext*>(&local);
    memcpy(reinterpret_cast<uint8_t*>(&local_->r8), reinterpret_cast<uint8_t*>(&gregs_),
           sizeof(gregs_));
    local_->fpstate = fpstate_.to_data_pointer<struct _fpstate*>(sizeof(struct _fpstate));
    memcpy(reinterpret_cast<uint8_t*>(&local_->SHIM_MCONTEXT_RES1),
           reinterpret_cast<uint8_t*>(&reserved1_), sizeof(reserved1_));
#endif
    return local;
  }

 private:
#if defined(__aarch64__)
  // No pointer there (ignoring extra_context, if present, which currently
  // stores the address of the extra space as __u64).
  struct sigcontext context_;
#elif defined(__x86_64__)
  // In Bionic, this structure is 'struct sigcontext', but in musl-libc it is
  // 'mcontext_t', which is technically 'struct sigcontext'. However, there is
  // a pointer in that structure: fpstate.
  // r8-r15 (8), rdi-rip (9), eflags (1), cs-ss (1), err, trapno, oldmask, cr2
  uint64_t gregs_[23];
  target_pointer_t fpstate_;
  uint64_t reserved1_[8];
#else
#error "Unrecognized architecture"
#endif
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
