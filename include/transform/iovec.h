/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/uio.h>
#include <syscall.h>

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct iovec' layout the shim is translating from.
using local_iovec_t = struct iovec;

// Data associated with any 'local_iovec_t' check.
struct iovec_data final {
  iovec_data() = default;

  explicit iovec_data(Pid pid) noexcept : info_{ pid } {
  }

  explicit iovec_data(PidFd pidfd) noexcept : info_{ pidfd } {
  }

  // Returns true if '.iov.base' is expected to be null-derived.
  bool require_null_derived() const noexcept {
    return !info_.is_same_process();
  }

 private:
  TargetedProcessInfo info_;
};

// The target side 'struct iovec' layout the shim is translating into.
struct target_iovec_t final {
  target_iovec_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, local_iovec_t& local,
                              const iovec_data data) noexcept {
    if (data.require_null_derived()) {
      check.null_derived(Arg{ input, local.iov_base });
    } else {
      check.buffer(Arg{ input, local.iov_base }, Arg{ input, local.iov_len });
    }
  }

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_iovec_t& local,
                              const iovec_data data) noexcept {
    if (data.require_null_derived()) {
      check.null_derived(Arg{ input, local.iov_base });
    } else {
      check.const_buffer(Arg{ input, local.iov_base }, Arg{ input, local.iov_len });
    }
  }

  // Transforms from local into target memory layout.
  target_iovec_t& operator<<(const local_iovec_t& local) noexcept {
    iov_base_ = local.iov_base;
    iov_len_ = local.iov_len;
    return *this;
  }

  // Transforms from target into local memory layout.
  //
  // The syscall ptrace() has 'struct iovec*' in one of its command, PTRACE_GETREGSET.
  // The call actually modifies iov->len so write it back.
  // Otherwise kernel is expected not to change the value.
  local_iovec_t& operator>>(local_iovec_t& local) noexcept {
    local.iov_len = iov_len_;
    return local;
  }

 private:
  target_pointer_t iov_base_;
  size_t iov_len_;
};

// Transformation of 'const struct iovec[]' system call arguments.
using ConstIoVecs =
    MaybeOnHeapOutputTransformation<const local_iovec_t, target_iovec_t, const iovec_data>;

// Transformation of a single 'const struct iovec*' system call argument.
using ConstIoVec = OutputTransformation<const local_iovec_t, target_iovec_t, const iovec_data>;

// Transformation of a single 'struct iovec*' system call argument.
using IoVec = InputTransformation<local_iovec_t, target_iovec_t, const iovec_data>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
