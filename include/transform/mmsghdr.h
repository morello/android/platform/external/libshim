/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/socket.h>

#include "base.h"
#include "msghdr.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct mmsghdr' layout the shim is translating from.
using local_mmsghdr_t = struct mmsghdr;

// The target side 'struct mmsghdr' layout the shim is translating into.
struct target_mmsghdr_t final {
  target_mmsghdr_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, local_mmsghdr_t& local, bool data) noexcept {
    target_msghdr_t::validate(check, input, local.msg_hdr, data);
  }

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_mmsghdr_t& local,
                              bool data) noexcept {
    target_msghdr_t::validate(check, input, local.msg_hdr, data);
  }

  // Transforms from local into target memory layout.
  //
  // This method is required by MaybeOnHeapOutputTransformation but it is not used.
  // Panics if invoked.
  target_mmsghdr_t& operator<<(const local_mmsghdr_t& local) noexcept {
    msg_hdr_ << local.msg_hdr;
    msg_len_ = local.msg_len;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_mmsghdr_t& operator>>(local_mmsghdr_t& local) noexcept {
    msg_hdr_ >> local.msg_hdr;
    local.msg_len = msg_len_;
    return local;
  }

 private:
  target_msghdr_t msg_hdr_;
  unsigned int msg_len_;

  // These will need access to private members.
  friend struct MMsgHdrs;
};

// Transformation of 'struct mmsghdr[]' system call arguments.
struct MMsgHdrs : public MaybeOnHeapInputTransformation<local_mmsghdr_t, target_mmsghdr_t> {
  // Constructor to create a new instance.
  explicit MMsgHdrs(Check& check, Arg mmsghdrs, Arg length) noexcept
      : MaybeOnHeapInputTransformation<local_mmsghdr_t, target_mmsghdr_t>(check, mmsghdrs, length,
                                                                          /* data */ false),
        iovecs_{ length.value } {
    if (!input_) {
      return;
    }

    // Input to output transformation is done here.
    auto iovecs = iovecs_.iter();
    size_t sub_idx = 0;
    for (auto iter : zip()) {
      // Transform { .msg_iov, .msg_iovlen } and check that it was successful.
      *iovecs = ConstIoVecs{ check, Arg{ iter.input.msg_hdr.msg_iov, mmsghdrs.idx, sub_idx },
                             Arg{ iter.input.msg_hdr.msg_iovlen, mmsghdrs.idx, sub_idx } };
      if (!check.passed()) {
        input_ = nullptr;
        return;
      }

      iter.output.msg_hdr_.msg_iov_ = (*iovecs).data();
      ++iovecs;
      ++sub_idx;
    }
  }

 private:
  stl::Array<ConstIoVecs> iovecs_;
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
