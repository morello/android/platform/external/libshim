/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/select.h>

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'pselect6_sigset_t' layout the shim is translating from.
struct local_pselect6_sigset_t {
  void* ss_addr;
  size_t ss_len;
};

// The target side 'pselect6_sigset_t' layout the shim is translating into.
struct target_pselect6_sigset_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_pselect6_sigset_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_pselect6_sigset_t& operator<<(const local_pselect6_sigset_t& local) noexcept {
    ss_addr_ = local.ss_addr;
    ss_len_ = local.ss_len;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_pselect6_sigset_t& operator>>(local_pselect6_sigset_t& local) noexcept {
    local.ss_addr = ss_addr_.to_data_pointer<void*>(ss_len_);
    local.ss_len = ss_len_;
    return local;
  }

 private:
  target_pointer_t ss_addr_;
  size_t ss_len_;
};

// Transformation of the outlier 6th pselect6 system call arguments.
using PSelect6SigSet = InputTransformation<local_pselect6_sigset_t, target_pselect6_sigset_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
