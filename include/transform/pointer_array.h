/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'void*' layout the shim is translating from.
using local_ptr_t = void*;

// Transformation of 'const void*' system call arguments.
using ConstPointerArray = MaybeOnHeapOutputTransformation<const local_ptr_t, target_pointer_t>;

// Helper to determine the length of a zero-pointer terminated array.
static inline size_t nullterminated_array_length(const void* const array[]) noexcept {
  if (uintptr_shim_t{ array }.is_valid_pointer()) {
    const void* const* array_start = array;

    while (uintptr_shim_t{ *array }.is_valid_pointer()) {
      ++array;
    }

    return (array - array_start + 1);
  } else {
    return 0;
  }
}

}  // namespace transform
}  // namespace shim
}  // namespace morello
