/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <signal.h>

#include "base.h"
#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The type 'stack_t' is actually 'struct sigaltstack'.
// The local side 'stack_t' layout the shim is translating from.
using local_stack_t_t = stack_t;

// The target side 'stack_t' layout the shim is translating into.`
struct target_stack_t_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_stack_t_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_stack_t_t& operator<<(const local_stack_t_t& local) noexcept {
    ss_sp_ = local.ss_sp;
    ss_flags_ = local.ss_flags;
    ss_size_ = local.ss_size;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_stack_t_t& operator>>(local_stack_t_t& local) noexcept {
    local.ss_sp = ss_sp_.to_data_pointer<void*>(ss_size_);
    local.ss_flags = ss_flags_;
    local.ss_size = ss_size_;
    return local;
  }

 private:
  target_pointer_t ss_sp_;
  int ss_flags_;
  size_t ss_size_;
};

// Transformation of 'const stack_t*' system call argument.
using ConstStack = OutputTransformation<const local_stack_t_t, target_stack_t_t>;

// Transformation of 'stack_t*' system call argument.
using Stack = InputTransformation<local_stack_t_t, target_stack_t_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
