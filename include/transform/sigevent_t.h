/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <signal.h>

#include "base.h"
#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct sigevent' layout the shim is translating from.
using local_sigevent_t_t = struct sigevent;

// GLIBC does not define 'sigev_notify_thread_id'.
// https://www.cygwin.com/bugzilla/show_bug.cgi?id=27417
#if !defined(sigev_notify_thread_id) && defined(__GLIBC__)
#define sigev_notify_thread_id _sigev_un._tid
#endif

// The target side 'sigval_t' layout the shim is translating into.
union target_sigval_t {
  int sival_int;
  target_pointer_t sival_ptr;
};

static constexpr size_t kSigevMaxSize = 64;  // SIGEV_MAX_SIZE
static constexpr size_t kSigevPreambleSize = sizeof(int) * 2 + sizeof(target_sigval_t);
static constexpr size_t kSigevPadSize = (kSigevMaxSize - kSigevPreambleSize) / sizeof(int);

// The target side 'sigevent_t' layout the shim is translating into.
struct target_sigevent_t_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_sigevent_t_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_sigevent_t_t& operator<<(const local_sigevent_t_t& local) noexcept {
    // See details here:
    //  - http://man7.org/linux/man-pages/man7/sigevent.7.html
    //  - http://man7.org/linux/man-pages/man2/timer_create.2.html

    switch (local.sigev_notify) {
      case SIGEV_NONE:
        break;

      case SIGEV_SIGNAL:
        sigev_value_.sival_int = local.sigev_value.sival_int;
        break;

      case SIGEV_THREAD_ID:
        sigev_un_._tid = local.sigev_notify_thread_id;
        break;

      case SIGEV_THREAD:
        // TODO: check: sival_ptr.
        sigev_value_.sival_ptr = local.sigev_value.sival_ptr;
        // TODO: check: function.
        sigev_un_._sigev_thread._function = local.sigev_notify_function;
        // TODO: check: _attribute is a pointer to pthread_attr_t
        sigev_un_._sigev_thread._attribute = local.sigev_notify_attributes;
        break;

      default:
        stl::panic("Unexpected sigev_notify value: %d.\n", local.sigev_notify);
        break;
    }

    sigev_signo_ = local.sigev_signo;
    sigev_notify_ = local.sigev_notify;

    return *this;
  }

  // Transforms from target into local memory layout.
  //
  // Ought to be constant only but the API says different.
  local_sigevent_t_t& operator>>(local_sigevent_t_t& local) noexcept {
    return local;
  }

 private:
  target_sigval_t sigev_value_;
  int sigev_signo_;
  int sigev_notify_;
  union {
    int _pad[kSigevPadSize];
    int _tid;
    struct {
      target_pointer_t _function;
      target_pointer_t _attribute;
    } _sigev_thread;
  } sigev_un_;
};

// Size check to be on the safe side.
static_assert(sizeof(target_sigevent_t_t) <= kSigevMaxSize, "Target sigevent_t is too big.");

// Transformation of 'sigevent_t*' system call argument.
using SigEvent = InputTransformation<local_sigevent_t_t, target_sigevent_t_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
