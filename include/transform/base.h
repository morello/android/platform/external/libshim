/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "check.h"
#include "pointer.h"
#include "stl/array.h"
#include "stl/iterator.h"
#include "stl/noncopy.h"
#include "stl/std.h"

namespace morello {
namespace shim {
namespace transform {

using morello::shim::check::Check;
using morello::shim::stl::Arg;

// Provides information about a target process given by its PID or pidfd.
struct TargetedProcessInfo final {
  TargetedProcessInfo() = default;

  explicit TargetedProcessInfo(Pid pid) noexcept : target_pid_{ pid.value } {
    if (target_pid_ == Pid::Invalid().value) {
      // This will always be different.
      current_pid_ = 0;
    } else if (target_pid_ != 0) {
      current_pid_ = LIBSHIM_SVC_NC(0, 0, 0, 0, 0, 0, __NR_getpid);
    }
  }

  // TODO: Set 'target_pid_' to 0 to indicate "self". This results in checks
  // being the least restrictive.  Later on this should be developed properly,
  // which means reading and parsing a file.
  explicit TargetedProcessInfo(PidFd pidfd) noexcept : target_pid_{ 0 } {
  }

  // Returns true if the targeted process is the currently running process.
  bool is_same_process() const noexcept {
    return (target_pid_ == 0) || (target_pid_ == current_pid_);
  }

 private:
  int target_pid_{ 0 };
  int current_pid_{ 0 };
};

// Represents a one-way, input to output transformation for a single object
// instance.
//
// Term: 'input' means the local layout, 'output' means the target layout.
//
// This class requires the following bitwise left shift operator:
//   O& O::operator<<(const I&);
//
// This operator is used to insert data from the local layout into a type
// representing the target layout.
template <typename I, typename O, typename D = bool>
class OutputTransformation : stl::NonCopy {
 public:
  // Default constructor to create a new instance.
  OutputTransformation() noexcept : input_{ nullptr }, data_{} {
  }

  // Constructor to create a new instance.
  explicit OutputTransformation(Check& check, Arg input, D data = {}) noexcept
      : input_{ nullptr }, data_{ data } {
    // Do not proceed if input is a true nullptr.
    if (check::is_nullptr(input)) {
      return;
    }

    if (stl::is_const<I>::value) {
      // Check that input is a pointer to a readable I.
      check.pointer_to_const<I>(input);
    } else {
      // Check that input is a pointer to a readable and writable I.
      check.pointer<I>(input).can_store_cap(input);
    }

    if (!check.can_load_cap(input).passed()) {
      // There is some problem with the input data, do not proceed with marshaling.
      return;
    }

    transform(check, input);
  }

  // Move assignment operator.
  OutputTransformation& operator=(OutputTransformation&& other) noexcept {
    this->input_ = other.input_;
    this->output_ = other.output_;
    this->data_ = other.data_;
    other.input_ = nullptr;
    return *this;
  }

  // Implicit (and so explicit) conversion operator to uintptr_shim_t.
  //
  // This operator comes handy in the generated shim code.
  operator uintptr_shim_t() noexcept {
    return data();
  }

  // Returns reference to the output element.
  O* data() {
    if (input_ != nullptr) {
      return &output_;
    } else {
      return nullptr;
    }
  }

 protected:
  void transform(Check& check, Arg input) noexcept {
    I* input_value = input.value;

    // Perform type-specific validation on the input data.
    O::validate(check, input, *input_value, data_);
    if (!check.passed()) {
      // There is some problem with the input data, do not proceed with marshaling.
      return;
    }

    input_ = input_value;
    // Input to output transformation
    output_ << *input_;
  }

  I* input_;
  O output_;
  D data_;
};

// Represents a two-way input to output and output to input transformation
// for a single object instance.
//
// Term: 'input' means the local layout, 'output' means the target layout.
//
// This class requires the following bitwise shift operators:
//   O& O::operator<<(const I&);
//   I& O::operator>>(I&);
//
// These operators are used to insert and extract data, respectively, from the
// local layout into a type representing the target layout and the other way
// around.
template <typename I, typename O, typename D = bool>
class InputTransformation : public OutputTransformation<I, O, D> {
 public:
  // Default constructor to create a new instance.
  InputTransformation() noexcept : OutputTransformation<I, O, D>() {
  }

  // Constructor to create a new instance.
  explicit InputTransformation(Check& check, Arg input, D data = {}) noexcept
      : OutputTransformation<I, O, D>(check, input, data) {
  }

  // Move assignment operator.
  InputTransformation& operator=(InputTransformation&& other) noexcept {
    OutputTransformation<I, O, D>::operator=(stl::move(other));
    return *this;
  }

  // Destructor which writes-back data from the output (target) layout into the
  // input (local) layout.
  ~InputTransformation() noexcept {
    if (this->input_ != nullptr) {
      this->output_ >> *this->input_;
    }
  }
};

// Represents a one-way, input to output transformation for multiple object
// instances which are allocated linearly in memory.
//
// Term: 'input' means the local layout, 'output' means the target layout.
//
// This class requires the following bitwise left shift operator:
//   O& O::operator<<(const I&);
//
// This operator is used to insert data from the local layout into a type
// representing the target layout.
//
// This class may use heap allocation.
// Below a threshold there is no heap allocation.
// This is to support possible system calls before the global allocator gets
// initialized. However, there are no guarantees that the allocation can not
// fail.
//
// The threshold is configurable with the 'SIZE' template argument which
// defaults to the default threshold of stl::Array.
template <typename I, typename O, typename D = bool,
          const size_t SIZE = stl::Array<O>::default_threshold()>
class MaybeOnHeapOutputTransformation : public stl::Array<O, SIZE> {
 public:
  // Default constructor to create a new instance.
  MaybeOnHeapOutputTransformation() noexcept : stl::Array<O, SIZE>(0), input_{ nullptr }, data_{} {
  }

  // Constructor to create a new instance.
  explicit MaybeOnHeapOutputTransformation(Check& check, Arg input, Arg length,
                                           D data = {}) noexcept
      : stl::Array<O, SIZE>(length.value), input_{ nullptr }, data_{ data } {
    // Do not proceed if input is a true nullptr.
    if (check::is_nullptr(input)) {
      return;
    }

    if (stl::is_const<I>::value) {
      // Check that input is a pointer to a readable I[length].
      check.pointer_to_const_array<I>(input, length);
    } else {
      // Check that input is a pointer to a readable and writable I[length].
      check.pointer_to_array<I>(input, length).can_store_cap(input);
    }

    if (!check.can_load_cap(input).passed()) {
      // There is some problem with the input pointer, do not proceed with marshaling.
      return;
    }

    input_ = input.value;

    if (!transform(check, input)) {
      input_ = nullptr;
    }
  }

  // Move assignment operator
  MaybeOnHeapOutputTransformation& operator=(MaybeOnHeapOutputTransformation&& other) noexcept {
    if (this != &other) {
      stl::Array<O, SIZE>::operator=(stl::move(other));
      input_ = other.input_;
      other.input_ = nullptr;
    }

    return *this;
  }

  // Implicit, and also explicit, conversion operator to uintptr_shim_t.
  //
  // This operator comes handy in the generated shim code.
  operator uintptr_shim_t() noexcept {
    return this->data();
  }

  // Returns an iterator zipping input and output arrays.
  stl::ZipIterator<I, O> zip() noexcept {
    return stl::ZipIterator<I, O>(input_, this->data(), this->size());
  }

 protected:
  bool transform(Check& check, Arg input) noexcept {
    size_t sub_idx = 0;
    for (auto iter : this->zip()) {
      // Perform type-specific validation on the input data.
      O::validate(check, Arg{ &iter.input, input.idx, sub_idx }, iter.input, data_);
      if (!check.passed()) {
        // There is some problem with the input data, do not proceed with marshaling.
        return false;
      }

      iter.output << iter.input;
      ++sub_idx;
    }

    return true;
  }

  I* input_;
  D data_;
};

// Represents a two-way, input to output transformation for multiple object
// instances which are allocated linearly in memory.
//
// The relation of this class to MaybeOnHeapOutputTransformation is just like the
// relation between InputTransformation and OutputTransformation (see above).
template <typename I, typename O, typename D = bool,
          const size_t SIZE = stl::Array<O>::default_threshold()>
class MaybeOnHeapInputTransformation : public MaybeOnHeapOutputTransformation<I, O, D> {
 public:
  // Constructor to create a new instance.
  explicit MaybeOnHeapInputTransformation(Check& check, Arg input, Arg length, D data = {}) noexcept
      : MaybeOnHeapOutputTransformation<I, O, D, SIZE>(check, input, length, data) {
  }

  // Destructor which writes-back data from the output (target) layout into the
  // input (local) layout.
  ~MaybeOnHeapInputTransformation() noexcept {
    if (this->input_) {
      for (auto iter : this->zip()) {
        iter.output >> iter.input;
      }
    }
  }
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
