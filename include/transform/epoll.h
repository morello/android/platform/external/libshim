/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/epoll.h>

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct epoll_event' layout the shim is translating from.
using local_epoll_event_t = struct epoll_event;

// The target side 'struct epoll_event' layout the shim is translating into.
struct target_epoll_event_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_epoll_event_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_epoll_event_t& operator<<(const local_epoll_event_t& local) noexcept {
    events_ = local.events;
    data_ = local.data.ptr;  // FIXME: change once FD tracking is implemented.
    return *this;
  }

  // Transforms from target into local memory layout.
  local_epoll_event_t& operator>>(local_epoll_event_t& local) noexcept {
    local.events = events_;
    // FIXME: change once FD tracking is implemented.
    local.data.ptr = data_.to_data_pointer<void*>();
    return local;
  }

 private:
  // Note: 'struct epoll_event' is packed on x86_64 but not on arm64.
  uint32_t events_;
  target_pointer_t data_;
};

// Transformation of 'struct epoll_event[]' system call arguments.
using EpollEvents = MaybeOnHeapInputTransformation<local_epoll_event_t, target_epoll_event_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
