/*
 * Copyright (c) 2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/ucontext.h>

#include "base.h"
#include "shim.h"
#include "transform/mcontext_t.h"
#include "transform/pointer.h"
#include "transform/stack_t.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'ucontext_' layout the shim is translating from.
using local_ucontext_t_t = ucontext_t;

// The target side 'ucontext_t' layout the shim is translating into.
struct target_ucontext_t_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_ucontext_t_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_ucontext_t_t& operator<<(const local_ucontext_t_t& local) noexcept {
    uc_flags_ = local.uc_flags;
    // uc_link is ignored by sigreturn(), do nothing about it.
    uc_stack_ << local.uc_stack;
    uc_sigmask_ = local.uc_sigmask;
    uc_mcontext_ << local.uc_mcontext;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_ucontext_t_t& operator>>(local_ucontext_t_t& local) noexcept {
    local.uc_flags = uc_flags_;
    // Always 0 when a signal is delivered on both arm64 and x86, so no
    // transformation is needed.
    local.uc_link = nullptr;
    uc_stack_ >> local.uc_stack;
    local.uc_sigmask = uc_sigmask_;
    uc_mcontext_ >> local.uc_mcontext;
    return local;
  }

 private:
  unsigned long uc_flags_;
  target_pointer_t uc_link_ __attribute__((unused));
  target_stack_t_t uc_stack_;
#if defined(__aarch64__)
  sigset_t uc_sigmask_;
  __u8 __unused_[1024 / 8 - sizeof(sigset_t)] __attribute__((unused));
  target_mcontext_t_t uc_mcontext_;
#elif defined(__x86_64__)
  target_mcontext_t_t uc_mcontext_;
  sigset_t uc_sigmask_;
#else
#error "Unrecognized architecture"
#endif
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
