/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <signal.h>

#include "base.h"
#include "shim.h"
#include "stl/signal.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct sigaction' layout the shim is translating from.
struct local_sigaction_t final {
  void (*handler)(int);
  unsigned long flags;
#if defined(SA_RESTORER)
  void (*restorer)(void);
#endif
  stl::shim_sigset_t mask;
};

// The target side 'struct sigaction' layout the shim is translating into.
struct target_sigaction_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, local_sigaction_t&, bool) noexcept {
    // Empty on purpose: non-const 'struct sigaction' is always 'oldact'.
  }

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_sigaction_t& local,
                              bool) noexcept {
    // Note: const 'struct sigaction' is always 'act'.
    if ((local.handler != SIG_DFL) && (local.handler != SIG_IGN)) {
      check.has_executive_permission(Arg{ input, local.handler });
    }

#if defined(SA_RESTORER) && defined(__CHERI_PURE_CAPABILITY__) && defined(__aarch64__)
    // PCuABI requires that '.sa_restorer' is the null capability.
    check.require_null(Arg{ input, local.restorer });
#endif
  }

  // Transforms from local into target memory layout.
  target_sigaction_t& operator<<(const local_sigaction_t& local) noexcept {
    sa_handler_ = local.handler;
    sa_flags_ = local.flags;
#if defined(SA_RESTORER)
    sa_restorer_ = local.restorer;
#endif
    sa_mask_ = local.mask;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_sigaction_t& operator>>(local_sigaction_t& local) noexcept {
    // Note: 'local.handler' is set in rt_sigaction().
    local.handler = nullptr;
    local.flags = sa_flags_;
#if defined(SA_RESTORER)
    local.restorer = sa_restorer_.to_function_pointer<void (*)(void)>();
#endif
    local.mask = sa_mask_;
    return local;
  }

 private:
  target_pointer_t sa_handler_;
  unsigned long sa_flags_;
#if defined(SA_RESTORER)
  target_pointer_t sa_restorer_;
#endif
  stl::shim_sigset_t sa_mask_;
};

// Transformation of 'const struct sigaction*' system call argument.
using ConstSigAction = OutputTransformation<const local_sigaction_t, target_sigaction_t>;

// Transformation of 'struct sigaction*' system call argument.
using SigAction = InputTransformation<local_sigaction_t, target_sigaction_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
