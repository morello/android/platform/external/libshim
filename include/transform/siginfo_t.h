/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <linux/posix_types.h>
#include <signal.h>
#include <sys/types.h>

#include "base.h"
#include "shim.h"
#include "transform/sigevent_t.h"

namespace morello {
namespace shim {
namespace transform {

// This is in uapi/asm.
#ifndef SI_MAX_SIZE
#define SI_MAX_SIZE 128
#endif

// Possible layouts of a `siginfo_t` structure.
enum class SignalLayout {
  kNone,
  kMesgq,
  kRt,
  kTimer,
  kKill,
  kSigchld,
  kSigpoll,
  kSigsys,
  kSigfault,
};

// kKill
struct SiginfoKill {
  __kernel_pid_t pid;
  __kernel_uid32_t uid;
};

// kSigchld
struct SiginfoSigchld {
  __kernel_pid_t pid;
  __kernel_uid32_t uid;
  int status;
  __kernel_clock_t utime;
  __kernel_clock_t stime;
};

// kSigpoll
struct SiginfoSigpoll {
  long band;
  int fd;
};

// kSigsys
struct SiginfoSigsys {
  target_pointer_t call_addr;
  int syscall;
  unsigned int arch;
};

// kTimer
struct SiginfoTimer {
  __kernel_timer_t tid;
  int overrun;
  target_sigval_t sigval;
  int sys_private;
};

// kRt
struct SiginfoRt {
  __kernel_pid_t pid;
  __kernel_uid32_t uid;
  target_sigval_t sigval;
};

// kSigfault according to uapi/asm-generic
struct SiginfoSigfault {
  target_pointer_t addr;
  short addr_lsb;
};

// Data associated with any 'local_siginfo_t_t' check.
struct siginfo_t_data final {
  siginfo_t_data() = default;

  explicit siginfo_t_data(Pid pid) noexcept : info_{ pid } {
  }

  explicit siginfo_t_data(PidFd pidfd) noexcept : info_{ pidfd } {
  }

  // Returns true if no tagged capability should be present in a 'local_siginfo_t_t' instance.
  bool require_untagged_capabilities() const noexcept {
    return !info_.is_same_process();
  }

 private:
  TargetedProcessInfo info_;
};

// The local side 'siginto_t' layout the shim is translating from.
using local_siginfo_t_t = siginfo_t;

// The target side 'siginfo_t' layout the shim is translating into.
struct target_siginfo_t_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_siginfo_t_t& local,
                              const siginfo_t_data data) noexcept {
    if (data.require_untagged_capabilities()) {
      check.has_no_valid_capabilities(Arg{ input, &local }, sizeof(local_siginfo_t_t));
    }
  }

  // Transforms from local into target memory layout.
  target_siginfo_t_t& operator<<(const local_siginfo_t_t& local) noexcept {
    si_signo_ = local.si_signo;
    si_errno_ = local.si_errno;
    si_code_ = local.si_code;

#define SI_FIELD_OUT(__t, __l) _sifields_.__t = local.__l

    auto layout = sifields_layout(local.si_signo, local.si_code);
    switch (layout) {
      case SignalLayout::kKill:
        SI_FIELD_OUT(_kill.pid, si_pid);
        SI_FIELD_OUT(_kill.uid, si_uid);
        break;

      case SignalLayout::kSigchld:
        SI_FIELD_OUT(_sigchld.pid, si_pid);
        SI_FIELD_OUT(_sigchld.uid, si_uid);
        SI_FIELD_OUT(_sigchld.status, si_status);
        SI_FIELD_OUT(_sigchld.utime, si_utime);
        SI_FIELD_OUT(_sigchld.stime, si_stime);
        break;

      case SignalLayout::kSigpoll:
        SI_FIELD_OUT(_sigpoll.band, si_band);
        SI_FIELD_OUT(_sigpoll.fd, si_fd);
        break;

      case SignalLayout::kSigsys:
        SI_FIELD_OUT(_sigsys.call_addr, si_call_addr);
        SI_FIELD_OUT(_sigsys.syscall, si_syscall);
        SI_FIELD_OUT(_sigsys.arch, si_arch);
        break;

      case SignalLayout::kTimer:
        SI_FIELD_OUT(_timer.tid, si_timerid);
        SI_FIELD_OUT(_timer.overrun, si_overrun);
        // TODO: SI_FIELD_OUT(_sigsys.sigval., ?); This is not in the man page.
#if defined(si_sys_private)
        SI_FIELD_OUT(_timer.sys_private, si_sys_private);
#endif
        break;

      case SignalLayout::kRt:
      case SignalLayout::kMesgq:
        SI_FIELD_OUT(_rt.pid, si_pid);
        SI_FIELD_OUT(_rt.uid, si_uid);
        SI_FIELD_OUT(_rt.sigval.sival_ptr,
                     si_ptr);  // NOTE: Be conservative and treat this as pointer.
        break;

      case SignalLayout::kSigfault:
        SI_FIELD_OUT(_sigfault.addr, si_addr);
#if defined(si_addr_lsb)
        SI_FIELD_OUT(_sigfault.addr_lsb, si_addr_lsb);
#endif
        break;

      case SignalLayout::kNone:
        break;

      default:
        stl::panic("The transformation of %d layout is not implemented.", layout);
        break;
    }

    return *this;
  }

  // Transforms from target into local memory layout.
  local_siginfo_t_t& operator>>(local_siginfo_t_t& local) noexcept {
    local.si_signo = si_signo_;
    local.si_errno = si_errno_;
    local.si_code = si_code_;

#define SI_FIELD_IN(__t, __l) local.__l = _sifields_.__t

    auto layout = sifields_layout(si_signo_, si_code_);
    switch (layout) {
      case SignalLayout::kKill:
        SI_FIELD_IN(_kill.pid, si_pid);
        SI_FIELD_IN(_kill.uid, si_uid);
        break;

      case SignalLayout::kSigchld:
        SI_FIELD_IN(_sigchld.pid, si_pid);
        SI_FIELD_IN(_sigchld.uid, si_uid);
        SI_FIELD_IN(_sigchld.status, si_status);
        SI_FIELD_IN(_sigchld.utime, si_utime);
        SI_FIELD_IN(_sigchld.stime, si_stime);
        break;

      case SignalLayout::kSigpoll:
        SI_FIELD_IN(_sigpoll.band, si_band);
        SI_FIELD_IN(_sigpoll.fd, si_fd);
        break;

      case SignalLayout::kSigsys:
        SI_FIELD_IN(_sigsys.call_addr.to_function_pointer<void*>(), si_call_addr);
        SI_FIELD_IN(_sigsys.syscall, si_syscall);
        SI_FIELD_IN(_sigsys.arch, si_arch);
        break;

      case SignalLayout::kTimer:
        SI_FIELD_IN(_timer.tid, si_timerid);
        SI_FIELD_IN(_timer.overrun, si_overrun);
        // TODO: SI_FIELD_OUT(_sigsys.sigval., ?); This is not in the man page.
#if defined(si_sys_private)
        SI_FIELD_IN(_timer.sys_private, si_sys_private);
#endif
        break;

      case SignalLayout::kRt:
      case SignalLayout::kMesgq:
        SI_FIELD_IN(_rt.pid, si_pid);
        SI_FIELD_IN(_rt.uid, si_uid);
        SI_FIELD_IN(_rt.sigval.sival_ptr.to_data_pointer<void*>(),
                    si_ptr);  // NOTE: Be conservative and treat this as pointer.
        break;

      case SignalLayout::kSigfault:
        SI_FIELD_IN(_sigfault.addr.to_data_pointer<void*>(), si_addr);
#if defined(si_addr_lsb)
        SI_FIELD_IN(_sigfault.addr_lsb, si_addr_lsb);
#endif
        break;

      case SignalLayout::kNone:
        break;

      default:
        stl::panic("The transformation of %d layout is not implemented.", layout);
        break;
    }

    return local;
  }

  // Determines the layout of __sifields according to available description at
  // http://man7.org/linux/man-pages/man2/sigaction.2.html
  SignalLayout sifields_layout(const int signo, const int si_code) {
    switch (si_code) {
      case SI_USER:
        return SignalLayout::kKill;

      case SI_KERNEL:
        break;

      case SI_QUEUE:
        return SignalLayout::kRt;

      case SI_TIMER:
        return SignalLayout::kTimer;

      case SI_MESGQ:
        return SignalLayout::kMesgq;

      case SI_ASYNCIO:
        break;

      case SI_SIGIO:
        return SignalLayout::kSigpoll;

      case SI_TKILL:
        return SignalLayout::kKill;

      default:
        break;
    }

    switch (signo) {
      case SIGCHLD:
        return SignalLayout::kSigchld;

      case SIGSYS:
        return SignalLayout::kSigsys;

      case SIGILL:
      case SIGFPE:
      case SIGSEGV:
      case SIGBUS:
      case SIGTRAP:
        return SignalLayout::kSigfault;

      case SIGCONT:
      case SIGSTOP:
        return SignalLayout::kNone;

      default:
        break;
    }

    stl::error_message("Unknown siginfo_t with signo = %d and si_code = %d\n", signo, si_code);
    return SignalLayout::kNone;
  }

 private:
  union {
    struct {
      int si_signo_;
      int si_errno_;
      int si_code_;
      union {
        SiginfoKill _kill;          // kKill
        SiginfoSigchld _sigchld;    // kSigchld
        SiginfoSigpoll _sigpoll;    // kSigpoll
        SiginfoSigsys _sigsys;      // kSigsys
        SiginfoTimer _timer;        // kTimer
        SiginfoRt _rt;              // kRt
        SiginfoSigfault _sigfault;  // kSigfault
      } _sifields_;
    };

    int _si_pad[SI_MAX_SIZE / sizeof(int)];
  };
};  // namespace transform

static_assert(sizeof(target_siginfo_t_t) == SI_MAX_SIZE,
              "Unfortunately target_siginfo_t_t grew too big.");

// Transformation of 'const siginfo_t*' system call argument.
using ConstSigInfo =
    OutputTransformation<const local_siginfo_t_t, target_siginfo_t_t, const siginfo_t_data>;

// Transformation of 'siginfo_t*' system call argument.
using SigInfo = InputTransformation<local_siginfo_t_t, target_siginfo_t_t, const siginfo_t_data>;

// Transformation of 'siginfo_t[]' system call arguments.
using SigInfos =
    MaybeOnHeapInputTransformation<local_siginfo_t_t, target_siginfo_t_t, const siginfo_t_data>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
