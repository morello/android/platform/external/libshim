/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <linux/filter.h>

#include "base.h"
#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct sock_fprog' layout the shim is translating from.
using local_sock_fprog_t = struct sock_fprog;

// The target side 'struct sock_fprog' layout the shim is translating into.
struct target_sock_fprog_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_sock_fprog_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_sock_fprog_t& operator<<(const local_sock_fprog_t& local) noexcept {
    len_ = local.len;
    filter_ = local.filter;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_sock_fprog_t& operator>>(local_sock_fprog_t& local) noexcept {
    local.len = len_;
    return local;
  }

 private:
  unsigned short len_;
  target_pointer_t filter_;
};

// Transformation of 'cons tstruct sock_fprog*' system call argument.
using ConstSockFprog = OutputTransformation<const local_sock_fprog_t, target_sock_fprog_t>;

// Transformation of 'struct sock_fprog*' system call argument.
using SockFprog = InputTransformation<local_sock_fprog_t, target_sock_fprog_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
