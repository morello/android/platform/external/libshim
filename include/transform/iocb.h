/*
 * Copyright (c) 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {

namespace transform {

using aio_context_t = unsigned long;

// The local side 'struct iocb' layout the shim is translating from.
struct local_iocb_t {
  using kernel_rwf_t = int;

  uint64_t aio_data;
  uint32_t aio_key;
  kernel_rwf_t aio_rw_flags;
  uint16_t aio_lio_opcode;
  int16_t aio_reqprio;
  uint32_t aio_fildes;
  uintptr_t aio_buf;
  uint64_t aio_nbytes;
  int64_t aio_offset;
  uint64_t aio_reserved2;
  uint32_t aio_flags;
  uint32_t aio_resfd;
};

// The target side 'struct iocb' layout the shim is translating into.
struct target_iocb_t final {
  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_iocb_t& local, bool) noexcept {
    check.buffer(Arg{ input, local.aio_buf }, Arg{ input, local.aio_nbytes });
  }

  // Transforms from local into target memory layout.
  target_iocb_t& operator<<(const local_iocb_t& local) noexcept {
    aio_data_ = local.aio_data;
    aio_key_ = local.aio_key;
    aio_rw_flags_ = local.aio_rw_flags;
    aio_lio_opcode_ = local.aio_lio_opcode;
    aio_reqprio_ = local.aio_reqprio;
    aio_fildes_ = local.aio_fildes;
    aio_buf_ = local.aio_buf;
    aio_nbytes_ = local.aio_nbytes;
    aio_offset_ = local.aio_offset;
    aio_reserved2_ = local.aio_reserved2;
    aio_flags_ = local.aio_flags;
    aio_resfd_ = local.aio_resfd;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_iocb_t& operator>>(local_iocb_t& local) noexcept {
    return local;
  }

 private:
  uint64_t aio_data_;
  uint32_t aio_key_;
  local_iocb_t::kernel_rwf_t aio_rw_flags_;
  uint16_t aio_lio_opcode_;
  int16_t aio_reqprio_;
  uint32_t aio_fildes_;
  target_pointer_t aio_buf_;
  uint64_t aio_nbytes_;
  int64_t aio_offset_;
  uint64_t aio_reserved2_;
  uint32_t aio_flags_;
  uint32_t aio_resfd_;
};

// Transformation of a single 'struct iocb*' system call argument.
using IoCB = InputTransformation<local_iocb_t, target_iocb_t>;

// Transformation of 'struct iocb**' system call arguments.
struct IoCBPP final {
  explicit IoCBPP(Check& check, Arg iocbpp, Arg length) noexcept
      : iocbpp_{ check, iocbpp, length }, iocb_array_{ length.value } {
    if (!check.passed()) {
      // There is some problem with the input data, do not proceed with marshaling.
      return;
    }

    // Iterator over the array of 'struct iocb' transformations.
    auto iocb_array_iterator = iocb_array_.iter();
    size_t sub_idx = 0;
    // Iterate over all input pointers,
    for (auto iocbpp_iterator : iocbpp_.zip()) {
      // perform marshalling of the pointed 'struct iocb' instance,
      (*iocb_array_iterator) = IoCB(check, Arg{ iocbpp_iterator.input, iocbpp.idx, sub_idx });
      // and if the transformation was successful, write the pointer to the marshalled instance into
      // the pointer array.
      if (!check.passed()) {
        return;
      }
      iocbpp_iterator.output.ptr_ = uintptr_shim_t{ (*iocb_array_iterator).data() };
      // Finally, advance iterators and indices.
      ++iocb_array_iterator;
      ++sub_idx;
    }
  }

  operator uintptr_shim_t() noexcept {
    return iocbpp_.data();
  }

 private:
  // An input pointer to a 'struct iocb' instance which is turned into an output pointer.
  struct target_iocb_ptr_t final {
    // Performs type-specific validation on the input data.
    static inline void validate(Check& check, Arg input, uintptr_shim_t& local, bool) noexcept {
      check.pointer<struct local_iocb_t>(Arg{ input, local });
    }

    // Transforms from local into target memory layout.
    target_iocb_ptr_t& operator<<(const uintptr_shim_t&) noexcept {
      // Does nothing on purpose. Setting 'ptr_' is done by IoCBPP constructor.
      return *this;
    }

    // Transforms from target into local memory layout.
    uintptr_shim_t& operator>>(uintptr_shim_t& local) noexcept {
      // Does nothing on purpose: there is no data to write back from kernel to
      // user space.
      return local;
    }

    uintptr_target_t ptr_;
  };

  // Holds the array of output pointers converted from input pointers.
  MaybeOnHeapInputTransformation<uintptr_shim_t, target_iocb_ptr_t> iocbpp_;
  stl::Array<IoCB> iocb_array_;
};

}  // namespace transform

// Libshim has to provide all the io* types because the kernel header defines
// 'struct iocb' incorrectly for pure-capability ABI.
using aio_context_t = transform::aio_context_t;
using iocb = transform::local_iocb_t;

struct io_event final {
  uint64_t data;
  uint64_t obj;
  int64_t res;
  int64_t res2;
};

}  // namespace shim
}  // namespace morello
