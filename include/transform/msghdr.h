/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sys/socket.h>

#include "base.h"
#include "iovec.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct msghdr' layout the shim is translating from.
using local_msghdr_t = struct msghdr;

// The target side 'struct msghdr' layout the shim is translating into.
struct target_msghdr_t final {
  target_msghdr_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, local_msghdr_t& local, bool) noexcept {
    check.buffer(Arg{ input, local.msg_name }, Arg{ input, local.msg_namelen });
    // Note: { .msg_iov, .msg_iovlen } is checked in the transformations, see below.
    check.buffer(Arg{ input, local.msg_control }, Arg{ input, local.msg_controllen });
  }

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, const local_msghdr_t& local, bool) noexcept {
    check.const_buffer(Arg{ input, local.msg_name }, Arg{ input, local.msg_namelen });
    // Note: { .msg_iov, .msg_iovlen } is checked in the transformations, see below.
    check.const_buffer(Arg{ input, local.msg_control }, Arg{ input, local.msg_controllen });
  }

  // Transforms from local into target memory layout.
  //
  // This method is required by OutputTransformation but it is not used.
  target_msghdr_t& operator<<(const local_msghdr_t& local) noexcept {
    msg_name_ = local.msg_name;
    msg_namelen_ = local.msg_namelen;
    // Note: '.msg_iov_' is set in the transformations.
    msg_iovlen_ = local.msg_iovlen;
    msg_control_ = local.msg_control;
    msg_controllen_ = local.msg_controllen;
    msg_flags_ = local.msg_flags;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_msghdr_t& operator>>(local_msghdr_t& local) noexcept {
    local.msg_namelen = msg_namelen_;
    local.msg_controllen = msg_controllen_;
    return local;
  }

 private:
  target_pointer_t msg_name_;
  socklen_t msg_namelen_;
  target_pointer_t msg_iov_;
  size_t msg_iovlen_;
  target_pointer_t msg_control_;
  size_t msg_controllen_;
  int msg_flags_;

  // These will need access to private members.
  friend struct ConstMsgHdr;
  friend struct MsgHdr;
  friend struct MMsgHdrs;
};

// Common code for MsgHdr transformations.
template <typename I, typename O>
class MsgHdrMixin {
 protected:
  MsgHdrMixin(Check& check, Arg msghdr, I*& input) {
    if (!input) {
      return;
    }

    // Transform { .msg_iov, .msg_iovlen } and check that it was successful.
    iovecs_ = ConstIoVecs{ check, Arg{ msghdr, input->msg_iov }, Arg{ msghdr, input->msg_iovlen } };
    if (!check.passed()) {
      input = nullptr;
      return;
    }
  }

  ConstIoVecs iovecs_;
};

// Transformation of 'const struct msghdr*' system call argument.
struct ConstMsgHdr : public OutputTransformation<const local_msghdr_t, target_msghdr_t>,
                     MsgHdrMixin<const local_msghdr_t, target_msghdr_t> {
  explicit ConstMsgHdr(Check& check, Arg msghdr) noexcept
      : OutputTransformation(check, msghdr), MsgHdrMixin(check, msghdr, input_) {
    output_.msg_iov_ = iovecs_.data();
  }
};

// Transformation of 'struct msghdr*' system call arguments.
struct MsgHdr : public InputTransformation<local_msghdr_t, target_msghdr_t>,
                MsgHdrMixin<local_msghdr_t, target_msghdr_t> {
  // Constructor to create a new instance.
  explicit MsgHdr(Check& check, Arg msghdr) noexcept
      : InputTransformation(check, msghdr), MsgHdrMixin(check, msghdr, input_) {
    output_.msg_iov_ = iovecs_.data();
  }
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
