/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <net/if.h>

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct ifconf' layout the shim is translating from.
using local_ifconf_t = struct ifconf;

// The target side 'struct ifconf' layout the shim is translating into.
struct target_ifconf_t final {
  target_ifconf_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check& check, Arg input, local_ifconf_t& local, bool) noexcept {
    // ifc_req (ifc_buf) should contain a pointer to an array of ifc_len number of ifreq structures.
    check.buffer(Arg{ input, local.ifc_buf }, Arg{ input, local.ifc_len });
  }

  // Transforms from local into target memory layout.
  target_ifconf_t& operator<<(const local_ifconf_t& local) noexcept {
    ifc_len_ = local.ifc_len;
    ifc_buf_ = local.ifc_buf;
    return *this;
  }

  // Transforms from target into local memory layout.
  local_ifconf_t& operator>>(local_ifconf_t& local) noexcept {
    local.ifc_len = ifc_len_;
    return local;
  }

 private:
  int ifc_len_;
  target_pointer_t ifc_buf_;
};

// Transformation of a single 'struct ifconf*' system call argument.
using IfConf = InputTransformation<local_ifconf_t, target_ifconf_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
