/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <elf.h>
#include <link.h>
#include <sys/prctl.h>

#include "base.h"
#include "elf_auxv_t.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'struct prctl_mm_map' layout the shim is translating from.
using local_prctl_mm_map_t = struct prctl_mm_map;

// The target side 'struct prctl_mm_map' layout the shim is translating to.
struct target_prctl_mm_map_t final {
  target_prctl_mm_map_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_prctl_mm_map_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  explicit target_prctl_mm_map_t(const local_prctl_mm_map_t& local,
                                 target_elf_auxv_t* auxv) noexcept {
    start_code_ = local.start_code;
    end_code_ = local.end_code;
    start_data_ = local.start_data;
    end_data_ = local.end_data;
    start_brk_ = local.start_brk;
    brk_ = local.brk;
    start_stack_ = local.start_stack;
    arg_start_ = local.arg_start;
    arg_end_ = local.arg_end;
    env_start_ = local.env_start;
    env_end_ = local.env_end;
    auxv_ = auxv;
    auxv_size_ = local.auxv_size;
    exe_fd_ = local.exe_fd;
  }

  // Transforms from local into target memory layout.
  //
  // This method is required by OutputTransformation but it is not used.
  target_prctl_mm_map_t& operator<<(const local_prctl_mm_map_t&) noexcept {
    // This method should do nothing.
    return *this;
  }

 private:
  uint64_t start_code_;
  uint64_t end_code_;
  uint64_t start_data_;
  uint64_t end_data_;
  uint64_t start_brk_;
  uint64_t brk_;
  uint64_t start_stack_;
  uint64_t arg_start_;
  uint64_t arg_end_;
  uint64_t env_start_;
  uint64_t env_end_;
  target_pointer_t auxv_;
  uint32_t auxv_size_;
  uint32_t exe_fd_;
};

// Transformation of 'const struct prctl_mm_map*' system call argument.
class ConstPrctlMmMap
    : public OutputTransformation<const local_prctl_mm_map_t, target_prctl_mm_map_t> {
 public:
  explicit ConstPrctlMmMap(Check& check, Arg prctl_mm_map) noexcept
      : OutputTransformation<const local_prctl_mm_map_t, target_prctl_mm_map_t>(check,
                                                                                prctl_mm_map) {
    // Input to output transformation
    if (this->input_ != nullptr) {
      // Note:  'struct prctl_mm_map'.auxv_size is the number of ElfW(Addr) in the array pointed to
      // and it is not the number of ElfW(auxv) instances.
      const auto auxv_count = this->input_->auxv_size / 2;
      elf_auxv_list_ =
          ConstElfAuxvs{ check,
                         Arg{ prctl_mm_map, reinterpret_cast<ElfW(auxv_t)*>(this->input_->auxv) },
                         Arg{ prctl_mm_map, auxv_count } };
      this->output_ = target_prctl_mm_map_t(*this->input_, elf_auxv_list_.data());
    }
  }

 private:
  ConstElfAuxvs elf_auxv_list_;
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
