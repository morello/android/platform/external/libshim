/*
 * Copyright (c) 2020-2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cheriintrin.h>
#include <unistd.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"
#include "stl/std.h"

namespace morello {
namespace shim {
namespace transform {

// Structure to simplify conversion between local and target pointer types.
struct target_pointer_t final {
  // Performs type-specific validation on the input data.
  template <typename T>
  static inline void validate(check::Check&, stl::Arg, const T&, bool) noexcept {
    // Empty on purpose.
  }

  // Copy assignable from pointer types.
  template <typename F, typename stl::enable_if<stl::is_pointer<F>::value, int>::type = 0>
  target_pointer_t& operator=(F other) noexcept {
    value_ = static_cast<uintptr_target_t>(reinterpret_cast<uintptr_t>(other));
    return *this;
  }

  // Copy assignable from integer types.
  template <typename F, typename stl::enable_if<stl::is_integral<F>::value, int>::type = 0>
  target_pointer_t& operator=(F other) noexcept {
    value_ = static_cast<uintptr_target_t>(other);
    return *this;
  }

  // Returns a data pointer.
  template <typename T, typename stl::enable_if<stl::is_pointer<T>::value, int>::type = 0>
  T to_data_pointer() const noexcept {
    if (check::is_nullptr(stl::Arg{ value_ })) {
      return nullptr;
    }

    return capability::build_no_exec<T>(value_);
  }

  // Returns a data pointer bounded according to the requested size.
  template <typename T, typename stl::enable_if<stl::is_pointer<T>::value, int>::type = 0>
  T to_data_pointer(size_t size) const noexcept {
    if (check::is_nullptr(stl::Arg{ value_ })) {
      return nullptr;
    }

#ifdef __CHERI_PURE_CAPABILITY__
    return cheri_bounds_set(to_data_pointer<T>(), size);
#else
    (void)size;
    return reinterpret_cast<T>(value_);
#endif
  }

  // Returns an executable pointer.
  template <typename T, typename stl::enable_if<stl::is_pointer<T>::value, int>::type = 0>
  T to_function_pointer() const noexcept {
    if (check::is_nullptr(stl::Arg{ value_ })) {
      return nullptr;
    }

    return capability::build_no_write<T>(value_);
  }

  // Transforms from local into target memory layout.
  template <typename T, typename stl::enable_if<stl::is_pointer<T>::value, int>::type = 0>
  target_pointer_t& operator<<(const T& local) noexcept {
    *this = local;
    return *this;
  }

 private:
  uintptr_target_t value_;
};

}  // namespace transform
}  // namespace shim
}  // namespace morello
