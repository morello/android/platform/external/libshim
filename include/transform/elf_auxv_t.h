/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <elf.h>
#include <link.h>
#include <sys/prctl.h>

#include "base.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace transform {

// The local side 'ElfW(auxv_t)' layout the shim is translating from.
using local_elf_auxv_t = ElfW(auxv_t);

// The target side 'ElfW(auxv_t)' layout the shim is translating into.
struct target_elf_auxv_t final {
  target_elf_auxv_t() = default;

  // Performs type-specific validation on the input data.
  static inline void validate(Check&, Arg, const local_elf_auxv_t&, bool) noexcept {
    // Empty on purpose.
  }

  // Transforms from local into target memory layout.
  target_elf_auxv_t& operator<<(const local_elf_auxv_t& local) noexcept {
    a_type_ = local.a_type;
    a_val_ = local.a_un.a_val;
    return *this;
  }

  // Note: shimming-back ElfW(auxv_t) to C64 would be possible but not required
  // in general use-cases. It can be developed once there is a need for it.

 private:
  ElfW(Addr) a_type_;
  target_pointer_t a_val_;
};

// Transformation of 'const struct ElfW(auxv_t)[]' system call arguments.
using ConstElfAuxvs = MaybeOnHeapOutputTransformation<const local_elf_auxv_t, target_elf_auxv_t>;

}  // namespace transform
}  // namespace shim
}  // namespace morello
