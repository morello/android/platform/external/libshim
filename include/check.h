/*
 * Copyright (c) 2020-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cheriintrin.h>
#include <string.h>
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
#include <sys/cheri.h>
#endif  // LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED

#include "shim.h"
#include "stl/panic.h"

namespace morello {
namespace shim {
namespace check {

// Shortens code below.
using morello::shim::stl::Arg;

// The implied (through function call) type of a check.
enum CheckType {
  // Expects a null terminated string or nullptr.
  STRING,
  // Expects a pointer to a constant memory of some size.
  CONST_BUFFER,
  // Expects a pointer to a RW memory of some size.
  BUFFER,
  // Expects that a capability is an owning capability.
  OWNING_CAPABILITY,
  // Expects that a set of protection flags is appropriate.
  PROT_IS_IN_MAX,
  // Expects that protections flags are a subset of that of the supplied capability.
  MAY_SET_PROT,
  // Expects some permission is set.
  PERMISSION,
  // Expected that a capability is null-derived.
  NULL_DERIVED,
  // Expects that some memory region holds only invalid (tag cleared) capabilities.
  HAS_NO_VALID_CAPABILITIES,
  // Expect that a value is the null pointer (capability).
  NULLPTR,
};

// This function scans a range of memory forwards, looking for a '\0' character.
// Returns true, if '\0' was found, otherwise false.
static inline bool is_null_terminated(const char* str, size_t length) {
  for (const char* end = &str[length]; str != end; ++str) {
    if ('\0' == *str) {
      return true;
    }
  }

  return false;
}

// Tests if a pointer (capability) is null.
static inline bool is_nullptr(const void* ptr) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
  uintptr_t null_cap = 0;
  return cheri_is_equal_exact(ptr, null_cap);
#else
  return !ptr;
#endif
}

// Tests if a pointer (capability) argument is null.
static inline bool is_nullptr(Arg ptr) noexcept {
  return is_nullptr(ptr.value);
}

// Class which can be used to chain checks and eventually error out.
class Check final {
 public:
  // Constructor to create a new instance.
  Check(const char* name) noexcept
      : passed_{ true },
        code_{ 0 },
        default_code_{ 0 },
        name_{ name },
        message_{ nullptr },
        type_{ CheckType::STRING },
        arg1_{ 0 },
        arg2_{ 0 } {
    reset_default_code();
  }

  // Returns the name of this check.
  const char* name() const noexcept {
    return name_;
  }

  // Returns true if all the checks passed, otherwise false.
  bool passed() const noexcept {
    return passed_;
  }

  // Returns the message associated with the first failed expectation, or nullptr.
  const char* message() const noexcept {
    return message_;
  }

  // Returns the error code of the first failed expectation.
  int code() const noexcept {
    return code_;
  }

  // Returns the type of the first failed expectation.
  CheckType type() const noexcept {
    return type_;
  }

  // Returns the 1st argument of the first failed expectation.
  Arg arg1() const noexcept {
    return arg1_;
  }

  // Returns the 2nd argument of the first failed expectation.
  Arg arg2() const noexcept {
    return arg2_;
  }

  // Dummy check which can be used in some corner cases to generate correct code.
  Check& skip() noexcept {
    return *this;
  }

  // Checks a pointer to a null-terminated string.
  Check& const_string(Arg ptr) noexcept {
    const char* str = ptr.value;
    if (!passed() || is_nullptr(str)) {
      return *this;
    }

    set_context(CheckType::STRING, ptr);
    expect_tagged_unsealed_global(str);
    expect_has_load_permission(str);
#ifdef __CHERI_PURE_CAPABILITY__
    if (passed()) {
      // With known bounds it is possible to safely tell if the string is null-terminated or not.
      // This is something non-capability aware kernel would not be able to do.
      expect(is_null_terminated(str, cheri_length_get(str)),
             "string is not null terminated within bounds");
    }
#endif

    return *this;
  }

  // TODO: move it into ConstPointerArray
  // Checks a pointer or capability to a constant string-like array object.
  //
  // Example:
  //  const char* foo[2] = { "A", nullptr };
  //  ASSERT_TRUE(Check().const_string_array(foo).passed());
  Check& const_string_array(Arg ptr) noexcept {
    if (!passed() || is_nullptr(ptr)) {
      return *this;
    }

    // TODO: rewrite this check to use bounds, etc.
    // TODO: add proper context once reimplemented.

    if (ptr.value.is_valid_pointer()) {
      const char** array = ptr.value;
      size_t index = 0;
      while (uintptr_shim_t{ *array }.is_valid_pointer()) {
        const_string(Arg{ *array, index });
        ++array;
        ++index;
      }
    }

    // C64: above we checked that:
    //  - the capabilities are valid,
    //  - the capabilities are readable and
    //  - the bounds are greater than or equal to the length of the respective strings.

    return *this;
  }

  // Checks a pointer to a readable memory region of 'size' bytes.
  Check& const_buffer(Arg ptr, Arg size) noexcept {
    if (!passed() || is_nullptr(ptr)) {
      return *this;
    }

    const void* region = ptr.value;
    const size_t region_size = size.value;
    set_context(CheckType::CONST_BUFFER, ptr, size);
    expect_tagged_unsealed_global(region);
    expect_within_bounds(region, region_size);
    expect_has_load_permission(region);
    return *this;
  }

  // Checks a pointer to a readable memory region of 'size' bytes, where 'size' is provided
  // indirectly through a pointer.
  template <typename T>
  Check& const_buffer(Arg ptr, Arg size) noexcept {
    if (!passed() || is_nullptr(ptr)) {
      return *this;
    }

    if (!pointer<T>(size).passed()) {
      return *this;
    }

    T* size_ptr = size.value;
    T pointed_size = size_ptr ? *size_ptr : 0;
    return const_buffer(ptr, Arg{ size, pointed_size });
  }

  // Checks a pointer to a readable and writable memory region of 'size' bytes.
  Check& buffer(Arg ptr, Arg size) noexcept {
    if (!passed() || is_nullptr(ptr)) {
      return *this;
    }

    const void* region = ptr.value;
    const size_t region_size = size.value;
    set_context(CheckType::BUFFER, ptr, size);
    expect_tagged_unsealed_global(region);
    expect_within_bounds(region, region_size);
    expect_has_load_permission(region);
    expect_has_store_permission(region);
    return *this;
  }

  // Checks a pointer to a readable and writable memory region of 'size' bytes, where 'size' is
  // provided indirectly through a pointer.
  template <typename T>
  Check& buffer(Arg ptr, Arg size) noexcept {
    if (!passed() || is_nullptr(ptr)) {
      return *this;
    }

    if (!pointer<T>(size).passed()) {
      return *this;
    }

    T* size_ptr = size.value;
    T pointed_size = size_ptr ? *size_ptr : 0;
    return buffer(ptr, Arg{ size, pointed_size });
  }

  // Checks a pointer to a constant object of type T.
  template <typename T>
  Check& pointer_to_const(Arg ptr) noexcept {
    return const_buffer(ptr, Arg{ sizeof(T) });
  }

  // Checks a pointer to an object of type T.
  template <typename T>
  Check& pointer(Arg ptr) noexcept {
    return buffer(ptr, Arg{ sizeof(T) });
  }

  // Checks a pointer to an array of 'length' number of constant objects of type T.
  template <typename T>
  Check& pointer_to_const_array(Arg ptr, size_t length) noexcept {
    return const_buffer(ptr, Arg{ sizeof(T) * length });
  }

  // Checks a pointer to an array of 'length' number of readable objects of type T.
  template <typename T>
  Check& pointer_to_const_array(Arg ptr, Arg length) noexcept {
    return const_buffer(ptr, Arg{ length, sizeof(T) * length.value });
  }

  // Checks a pointer to an array of 'length' number of writable objects of type T.
  template <typename T>
  Check& pointer_to_array(Arg ptr, Arg length) noexcept {
    return buffer(ptr, Arg{ length, sizeof(T) * length.value });
  }

  // Checks if a capability is a valid unsealed capability with the following properties.
  // - Its bounds include the range
  //       (align_down(start_addr, PAGE_SIZE), align_up(start_addr + length, PAGE_SIZE)).
  // - Its permissions include Global and VMem.
  Check& is_owning_capability(Arg ptr, Arg size) noexcept {
    if (!passed()) {
      return *this;
    }

#ifdef __CHERI_PURE_CAPABILITY__
    set_context(CheckType::OWNING_CAPABILITY, ptr, size);
    // This whole check sets its own code. Save and restore the current one.
    const int prev_code = default_code_;
    const void* region = ptr.value;
    const size_t region_size = size.value;
    expect_tagged_unsealed_global(region);
    expect_within_bounds(region, region_size, /* page_align */ true);
    expect_has_vmem_permission(region);
    default_code_ = prev_code;
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  Check& prot_is_in_max(Arg prot) noexcept {
    if (!passed()) {
      return *this;
    }

#if LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED && defined(__CHERI_PURE_CAPABILITY__)
    set_context(CheckType::PROT_IS_IN_MAX, prot);
    const int requested_prot = prot.value;
    const int max_prot_flags = PROT_MAX_EXTRACT(requested_prot);
    const int prot_flags = PROT_EXTRACT(requested_prot);
    expect((max_prot_flags == 0) || ((max_prot_flags & prot_flags) == prot_flags),
           "neither 'PROT_MAX_EXTRACT(p) == 0' nor '(PROT_EXTRACT(p) & PROT_MAX_EXTRACT(p)) "
           "== PROT_EXTRACT(p)'",
           EINVAL);
#endif /* LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED && __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Checks if a capability's permissions correspond to specified prot flags.
  Check& may_set_prot(Arg ptr, Arg prot) noexcept {
    if (!passed()) {
      return *this;
    }

#if LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED && defined(__CHERI_PURE_CAPABILITY__)
    set_context(CheckType::MAY_SET_PROT, ptr, prot);
    const void* region = ptr.value;
    const int prot_flags = prot.value;
    if (prot_flags & PROT_READ) {
      expect_has_load_permission(region);
    }
    if (prot_flags & PROT_WRITE) {
      expect_has_store_permission(region);
    }
    if (prot_flags & PROT_EXEC) {
      expect_has_execute_permission(region);
    }
#endif /* LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED && __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Checks that an input capability has LOAD_CAP permission.
  Check& can_load_cap(Arg ptr) noexcept {
    const void* region = ptr.value;
    if (!passed() || is_nullptr(region)) {
      return *this;
    }

    set_context(CheckType::PERMISSION, ptr);
    expect_has_load_cap_permission(region);
    return *this;
  }

  // Checks that an input capability has STORE_CAP permission.
  Check& can_store_cap(Arg ptr) noexcept {
    const void* region = ptr.value;
    if (!passed() || is_nullptr(region)) {
      return *this;
    }

    set_context(CheckType::PERMISSION, ptr);
    expect_has_store_cap_permission(region);
    return *this;
  }

  // Checks that an input capability has EXECUTIVE permission.
  Check& has_executive_permission(Arg ptr) noexcept {
    const void* region = ptr.value;
    if (!passed()) {
      return *this;
    }

    set_context(CheckType::PERMISSION, ptr);
    expect_has_executive_permission(region);
    return *this;
  }

  // Checks that a capability is null-derived.
  Check& null_derived(Arg ptr) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
    set_context(CheckType::NULL_DERIVED, ptr);
    const char* region = ptr.value;
    const ptraddr_t address = cheri_address_get(region);
    expect(cheri_is_equal_exact(region, cheri_address_set(nullptr, address)),
           "capability is not null-derived");
#endif
    return *this;
  }

  // Checks that a memory region holds only invalid (tag cleared) capabilities.
  Check& has_no_valid_capabilities(Arg ptr, size_t size) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
    set_context(CheckType::HAS_NO_VALID_CAPABILITIES, ptr, Arg{ size });
    const char* region = ptr.value;
    expect_within_bounds(region, size);
    if (!passed()) {
      return *this;
    }

    static constexpr size_t kStep = sizeof(void*);
    const ptraddr_t address = cheri_address_get(region);
    const auto iter_end = reinterpret_cast<uintcap_t>(__builtin_align_down(region + size, kStep));
    // Use libshim's DDC to create a capability with LOAD and LOAD_CAP permissions.
    // This ensures that testing the memory range works in all cases.
    auto iter = cheri_address_set(__shim_data_cap, __builtin_align_up(address, kStep));
    for (; iter < iter_end; iter += kStep) {
      const void* cap = reinterpret_cast<void**>(iter)[0];
      expect(!cheri_tag_get(cap), "capability is tagged", EINVAL);
    }
#endif
    return *this;
  }

  // Expects that an argument is the null pointer (capability).
  Check& require_null(Arg ptr) noexcept {
    set_context(CheckType::NULLPTR, ptr);
    expect(is_nullptr(ptr), "pointer is not null", EINVAL);
    return *this;
  }

  // Requires that PCC has Executive permission set.
  Check& require_executive() noexcept {
#if defined(__CHERI_PURE_CAPABILITY__) && defined(__ARM_CAP_PERMISSION_EXECUTIVE__)
    const uint64_t pcc_perms = cheri_perms_get(cheri_pcc_get());
    expect(pcc_perms & __ARM_CAP_PERMISSION_EXECUTIVE__, "PCC has no EXECUTIVE permission", EPERM);
#endif
    return *this;
  }

  // Adds an expectation to the check instance.
  //
  // If 'condition' is false, and there is no previous failed expectation, the
  // 'message' to the condition and the default error 'code' is stored.
  Check& expect(bool condition, const char* message) {
    return expect(condition, message, default_code_);
  }

  // Adds an expectation to the check instance.
  //
  // If 'condition' is false, and there is no previous failed expectation, the
  // 'message' to the condition and an associated error 'code' is stored.
  Check& expect(bool condition, const char* message, int code) {
    // Always keep the first failure.
    if (passed() && !condition) {
      passed_ = false;
      message_ = message;
      code_ = code;
    }

    return *this;
  }

  // Sets the error code used for any further checks.
  Check& set_default_code(int code) {
    default_code_ = code;
    return *this;
  }

  // Resets the error code used for any further checks.
  Check& reset_default_code() {
    default_code_ = EFAULT;
    return *this;
  }

 private:
  // Checks that a pointer (capability)
  //  - is not null,
  //  - it is tagged,
  //  - is not sealed and
  //  - has GLOBAL permission.
  Check& expect_tagged_unsealed_global(const void* ptr) noexcept {
    // TODO: add check against view of memory mappings.
    // Checking against null is the minimum.
    expect(!is_nullptr(ptr), "pointer is null");
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_tag_get(ptr), "capability is not tagged");
    expect(!cheri_is_sealed(ptr), "capability is sealed");
#if !__SANITIZE_CHERISEED__
    // GLOBAL permission is not described by CHERI C/C++ Programming Guide as of June 2020.
    expect_has_global_permission(ptr);
#endif /* !__SANITIZE_CHERISEED__ */
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Checks that an access with a pointer (capability) is within bounds.
  Check& expect_within_bounds(const void* ptr, size_t size, bool page_align = false) noexcept {
#ifdef __CHERI_PURE_CAPABILITY__
    // Clear the [63:56] - not used for addressing but still might contain non-zero
    // values on some architectures.
    static constexpr ptraddr_t mask_63_56 = ~(0xFFUL << 56);
    const ptraddr_t address = cheri_address_get(ptr) & mask_63_56;
    ptraddr_t end_address;

    // TODO: remove dependency on getpagesize() and use AT_PAGESZ.
    const ptraddr_t alignment = page_align ? getpagesize() : 1;
    const ptraddr_t aligned_lower = __builtin_align_down(address, alignment);
    const ptraddr_t aligned_upper = __builtin_align_up(address + size, alignment);
    const ptraddr_t base = cheri_base_get(ptr);
    const ptraddr_t limit = base + cheri_length_get(ptr);  // This should not overflow.

    expect(!__builtin_add_overflow(address, size, &end_address), "address + size would overflow");
    expect(base <= aligned_lower, page_align
                                      ? "page-aligned-down address is smaller than capability base"
                                      : "address is smaller than capability base");
    expect(aligned_upper <= limit,
           page_align ? "page-aligned-up address + size is larger than capability limit"
                      : "address + size is larger than capability limit");
#endif
    return *this;
  }

  // Expects that a capability has GLOBAL permission.
  Check& expect_has_global_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_GLOBAL, "capability lacks GLOBAL permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has LOAD permission.
  Check& expect_has_load_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_LOAD, "capability lacks LOAD permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has LOAD_CAP permission.
  Check& expect_has_load_cap_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_LOAD_CAP, "capability lacks LOAD_CAP permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has STORE permission.
  Check& expect_has_store_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_STORE, "capability lacks STORE permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has STORE_CAP permission.
  Check& expect_has_store_cap_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_STORE_CAP, "capability lacks STORE_CAP permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has EXECUTE permission.
  Check& expect_has_execute_permission(const void* ptr) {
#ifdef __CHERI_PURE_CAPABILITY__
    expect(cheri_perms_get(ptr) & CHERI_PERM_EXECUTE, "capability lacks EXECUTE permission");
#endif /* __CHERI_PURE_CAPABILITY__ */
    return *this;
  }

  // Expects that a capability has VMEM permission.
  Check& expect_has_vmem_permission(const void* ptr) {
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED && defined(__CHERI_PURE_CAPABILITY__)
    expect(cheri_perms_get(ptr) & CHERI_PERM_SW_VMEM, "capability lacks VMEM permission");
#endif
    return *this;
  }

  // Expects that a capability has EXECUTIVE permission.
  Check& expect_has_executive_permission(const void* ptr) {
#if defined(__CHERI_PURE_CAPABILITY__) && defined(__ARM_CAP_PERMISSION_EXECUTIVE__)
    expect(cheri_perms_get(ptr) & __ARM_CAP_PERMISSION_EXECUTIVE__,
           "capability lacks EXECUTIVE permission", EINVAL);
#endif
    return *this;
  }

  // Sets the context for a check. This is to provide more useful description if an error happens.
  void set_context(CheckType type, Arg arg) noexcept {
    set_context(type, arg, Arg{ 0 });
  }

  // Sets the context for a check. This is to provide more useful description if an error happens.
  void set_context(CheckType type, Arg arg1, Arg arg2) noexcept {
    // Always keep the first failed context.
    if (passed()) {
      type_ = type;
      arg1_ = arg1;
      arg2_ = arg2;
    }
  }

  bool passed_;
  int code_;
  int default_code_;
  const char* name_;
  const char* message_;
  CheckType type_;
  Arg arg1_;
  Arg arg2_;
};

}  // namespace check
}  // namespace shim
}  // namespace morello
