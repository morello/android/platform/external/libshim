/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <inttypes.h>

#include "shimconfig.h"
#include "shimtypes.h"
#include "stl/errno.h"

namespace morello {
namespace shim {

// Forward declarations.
namespace check {
class Check;
}  // namespace check

// Macros improving code readability.

// Function attributes.
#define LIBSHIM_NOINLINE __attribute__((noinline))
#define LIBSHIM_NAKED __attribute__((naked))
#define LIBSHIM_WEAK __attribute__((weak))

// The long system call parameter list.
#define LIBSHIM_COMMON_ARGS                                                           \
  uintptr_shim_t arg1, uintptr_shim_t arg2, uintptr_shim_t arg3, uintptr_shim_t arg4, \
      uintptr_shim_t arg5, uintptr_shim_t arg6

#if LIBSHIM_CANCELLATION_POINTS
#define LIBSHIM_FN_SYSCALL_ARGS uintptr_shim_t cg, long nr, LIBSHIM_COMMON_ARGS
#define LIBSHIM_FN_ARGS LIBSHIM_COMMON_ARGS, long nr, uintptr_shim_t cg
#define LIBSHIM_FN_CALL(__sym, ...) __sym(__VA_ARGS__, cg)
#define LIBSHIM_FN_CALL_NC(__sym, ...) __sym(__VA_ARGS__, 0)
#else
#define LIBSHIM_FN_SYSCALL_ARGS long nr, LIBSHIM_COMMON_ARGS
#define LIBSHIM_FN_ARGS LIBSHIM_COMMON_ARGS, long nr
#define LIBSHIM_FN_CALL(__sym, ...) __sym(__VA_ARGS__)
#define LIBSHIM_FN_CALL_NC LIBSHIM_FN_CALL
#endif

// The preferred way to call 'svc()'.
#define LIBSHIM_SVC(...) LIBSHIM_FN_CALL(morello::shim::svc, __VA_ARGS__)
#define LIBSHIM_SVC_NC(...) LIBSHIM_FN_CALL_NC(morello::shim::svc, __VA_ARGS__)

// A libshim C++ function prototype.
#define LIBSHIM_FN(__name) LIBSHIM_NOINLINE uintptr_shim_t __name(LIBSHIM_FN_ARGS)

#define LIBSHIM_C_API(__name, __args) extern "C" LIBSHIM_NOINLINE void* __shim_##__name(__args)

// A libshim C function prototype.
//
// Return void* which should be fine for all types.
#define LIBSHIM_FN_C_PROTOTYPE(__name) LIBSHIM_C_API(__name, LIBSHIM_FN_ARGS)

// A libshim C function definition.
#define LIBSHIM_FN_C(__name)                                                \
  LIBSHIM_FN_C_PROTOTYPE(__name) {                                          \
    return LIBSHIM_FN_CALL(__name, arg1, arg2, arg3, arg4, arg5, arg6, nr); \
  }

// Prints a checked and failed system call to stderr.
void PrintFailedSystemCall(check::Check&, unsigned, LIBSHIM_COMMON_ARGS) noexcept;

#define PRINT_CHECKED_CALL_FAILED(__check, __num_args) \
  PrintFailedSystemCall(__check, __num_args, arg1, arg2, arg3, arg4, arg5, arg6)

// Declaration of shim methods.
LIBSHIM_FN(svc);
LIBSHIM_FN(syscall);

#if defined(LIBSHIM_LIBRARY_BUILD)
// The generated shim_gen.h contains declarations of system calls only.
// The layout is similar to lines 32-33.
// Include that file here so that definitions such as uintptr_shim_t are visible.
#include "shim_gen.h"
#endif

}  // namespace shim
}  // namespace morello
