/*
 * Copyright (c) 2021-2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

// LIBSHIM_ERROR_PANICS configuration option:
//  0 - printing an error message doesn't cause shim::stl::panic;
//  1 - shim::stl::panic after printing an error message.
#ifndef LIBSHIM_ERROR_PANICS
#define LIBSHIM_ERROR_PANICS 0
#endif

// LIBSHIM_PRETTY_PRINT configuration option:
//  0 - don't pretty print messages;
//  1 - pretty print messages.
#ifndef LIBSHIM_PRETTY_PRINT
#define LIBSHIM_PRETTY_PRINT 1
#endif

// LIBSHIM_IOCTL_VERBOSE configuration option:
//  0 - no additional prints in ioctl;
//  1 - upon ioctl, print information about the handler processing the call.
#ifndef LIBSHIM_IOCTL_VERBOSE
#define LIBSHIM_IOCTL_VERBOSE 0
#endif

// LIBSHIM_SYSCALL_STRICT configuration option:
//  0 - forward unknown system calls to the kernel;
//  1 - shim::stl::panic upon an unknown system call.
#ifndef LIBSHIM_SYSCALL_STRICT
#define LIBSHIM_SYSCALL_STRICT 0
#endif

// LIBSHIM_SIGACTION_VERBOSE configuration option:
//  0 - no additional prints in rt_sigaction;
//  1 - print details of the signal  associated signal handler
//      and dump the whole signal handler registry in rt_sigaction.
#ifndef LIBSHIM_SIGACTION_VERBOSE
#define LIBSHIM_SIGACTION_VERBOSE 0
#endif

// Default heap allocation threshold.
#ifndef LIBSHIM_DEFAULT_HEAP_THRESHOLD
#define LIBSHIM_DEFAULT_HEAP_THRESHOLD 16
#endif

// Default registry size.
#ifndef LIBSHIM_DEFAULT_REGISTRY_SIZE
#define LIBSHIM_DEFAULT_REGISTRY_SIZE 16
#endif

// LIBSHIM_ZERO_DDC configuration option:
//   0 - don't zero DDC register after saving its value;
//   1 - zero DDC after saving its value and use that to derive capabilities.
// LIBSHIM_ZERO_DDC is only considered when targeting pure capability ABI.
#ifndef LIBSHIM_ZERO_DDC
#define LIBSHIM_ZERO_DDC 0
#endif

// Automatic support for CHERIseed
// Turn on with '-fsanitize=cheriseed' compiler flag.
#ifdef __has_feature
#if __has_feature(cheriseed_sanitizer) && !defined(__SANITIZE_CHERISEED__)
#define __SANITIZE_CHERISEED__ 1
#endif
#endif

// LIBSHIM_CANCELLATION_POINTS configuration option:
//   0 - no support for POSIX-like cancellation points;
//   1 - support for POSIX-like cancellation points.
#ifndef LIBSHIM_CANCELLATION_POINTS
#define LIBSHIM_CANCELLATION_POINTS 0
#endif

// LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED configuration option:
//  0 - PROT_MAX flags not implemented;
//  1 - PROT_MAX flags implemented.
#ifndef LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED
#define LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED 0
#endif

// LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED configuration option:
//   0 - VMem permissions not implemented.
//   1 - VMem permissions implemented.
#ifndef LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
#define LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED 0
#endif
