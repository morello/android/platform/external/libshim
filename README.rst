*******
Libshim
*******

Libshim is a thin user-space layer between a regular user-space application and
kernel system calls. Its main purpose is to translate system call arguments
and return values between user-space Morello pure capability ABI and the base
arm64 kernel ABI. Therefore libshim wraps all the system calls.

The need for libshim arises from the fact that the Morello kernel ABI is not yet
defined. The fundamental differences in the size and alignment of passed
structures require some mechanism so that pure capability applications can
still use the existing Arm64 base kernel. Capabilities are 128 (+ 1) bits wide
and must be aligned to 16 bytes. In contrast, regular A64 pointers are 64-bit
integer-like values. These differences mean that any structure which has at
least one pointer member must be transformed because of in-memory layout
mismatches, need to reconstruct bounds and permissions, etc.

For example, the code below prints the size and alignment of
`struct iovec <https://www.man7.org/linux/man-pages/man2/readv.2.html#DESCRIPTION>`_
to stdout.

.. code-block:: cpp

  #include <iostream>
  #include <sys/uio.h>

  int main() {
    std::cout << "sizeof(struct iovec): " << sizeof(struct iovec) << " bytes\n";
    std::cout << "alignof(struct iovec): " << alignof(struct iovec) << " bytes\n";
    return 0;
  }

Output in AArch64:

.. code-block:: cpp

  sizeof(struct iovec): 16 bytes
  alignof(struct iovec): 8 bytes

Output in purecap:

.. code-block:: cpp

  sizeof(struct iovec): 32 bytes
  alignof(struct iovec): 16 bytes


Note
  :code:`Pointer` means capability in pure ABI but regular pointer
  otherwise throughout the rest of this document.

Abbreviations
=============

+----------------+--------------------------------------------------+
| ABI            | Application Binary Interface                     |
+----------------+--------------------------------------------------+
| API            | Application Programming Interface                |
+----------------+--------------------------------------------------+
| Base           | Base Arm64 ABI                                   |
+----------------+--------------------------------------------------+
| ISA            | Instruction Set Architecture                     |
+----------------+--------------------------------------------------+
| Purecap        | Morello pure capability ABI                      |
+----------------+--------------------------------------------------+

Feature list
============

Libshim has the following features:

- Bi-directional transformation between Purecap and Base ABIs.
- Additional checks on pointers, including tag bit, bounds and address.
- Most of the shim layer is generated based on an input list of system calls
  during build-time.
- Allocation of temporary structures either on stack or heap, depending on size.
- Supports adding and removing generation of system calls to easily adopt to
  the requirements of systems.
- Supports fine-grained modifications to generated system call implementations.
- Supports implementing system calls manually.
- Prints messages to stderr if verbose logs are enabled or if something
  unexpected is detected such as an unknown system call number is encountered,
  an invalid pointer is passed, etc.

Compile-time control switches:

- :code:`LIBSHIM_ERROR_PANICS`: Printing an error message panics too.
- :code:`LIBSHIM_PRETTY_PRINT`: Pretty-print messages. On by default.
  Disabling pretty printing can be useful when porting a libc. When
  pretty-printing is disabled only format strings will be printed.
- :code:`LIBSHIM_IOCTL_VERBOSE`: When calling :code:`ioctl()` it prints which
  handler processed the call, if any.
- :code:`LIBSHIM_SYSCALL_STRICT`: If :code:`syscall()` encounters an unknown
  system call number it panics instead of forwarding the call to the kernel.
- :code:`LIBSHIM_SIGACTION_VERBOSE`: when calling :code:`sigaction()` prints
  details of the signal and its associated handler. Also dumps the whole
  signal handler registry.
- :code:`LIBSHIM_DEFAULT_HEAP_THRESHOLD`: threshold parameter for shim::stl::Array,
  which determines highest number of array elements allocated inlined into the Array
  object - larger arrays are allocated in separate dynamically allocated regions.
- :code:`LIBSHIM_DEFAULT_REGISTRY_SIZE`: this is a workaround. Local static
  initializers are used to perform initialization of some user-space registries.
  When init process starts services there are crashes. Hypothesis is that
  the allocator is not yet initialised. The workaround is to use some
  static-allocated storage to avoid the need of early allocations.
- :code:`LIBSHIM_ZERO_DDC`: see section below for details.
- :code:`LIBSHIM_CANCELLATION_POINTS`: see section below for details.

In addition, libshim features support for raw arguments marshalling.
When a process is loaded its raw arguments are layed out in memory according to
the Base ABI. To support Purecap executables these arguments should be
transformed. The function :code:`__shim_marshal_program_arguments()` converts
raw arguments from Base to Purecap ABI in memory. For details please refer to
the comments around the implementation.

High level design decisions
===========================

The high level design requirements were the following:

1. The shim layer should be implemented in C++11.
2. The shim layer should have its public APIs in C.
3. The shim layer should not depend on any STL.
4. It should be possible to compile the shim for Purecap ABI.
5. The shim layer should be a static library.
6. It should be possible to test data transformations with the Base ABI.
7. The shim layers should mimic the behavior of a kernel providing the Purecap
   ABI as much as possible.
8. The shim layer should return tightly bounded capabilities wherever possible.
9. The shim layer should check most of the capabilities it receives against
   available pieces of information to verify the following properties:
   validity of the pointers, bounds and permissions.
   Read more in section `Pointer checks`_.

Pointer checks
==============

Libshim assumes that pointers should always be valid. A pointer is considered
to be invalid if

- its tag bit is cleared or
- it points to zero page (virtual address is in [0, 4095]) or
- has bit 55 set.

The overall logic is a bit more complex. In some cases a null pointer may be
treated valid if it has an associated length which is also zero.

Bounds checks are only performed if the underlying type's size is known.
Similarly, tightly bounded capabilities are only returned if the size is known.
For example, in case of function pointers bounds are not adjusted.

Permission checks are based on system call semantics.
For example, in case of :code:`readv()` it is required that the linear
memory region pointed to by :code:`iov` is readable but other permissions are
not enforced. In contrast, :code:`ptrace(PTRACE_GETREGSET)` requires that the
:code:`iov` array is both readable and writable.

Dependencies
============

Libshim has some external dependencies listed below:

1. The generator requires python3.6+.

Usage
=====

Libshim is expected to be part of the system libc and therefore it should
transparently handle system calls without the need of further configuration
or user action.

However, if there is a system call argument which needs further shimming,
see below in the `Limitations`_ section, transformations may need to be
implemented either

- by contributing to libshim, if the case is generic, or
- by adding those to user-space code at the call sites of respective system
  calls.

Existing transformations implemented in libshim can be re-used, see the files
under :code:`include/transform/`.

Warning
  Only :code:`include/transform/*.h` (C++) are supported to be included into
  user source files.

Limitations
===========

The shim layer has some limitations listed in this section.

Note
  Limitations below are only supported to an extent so that workloads of
  interest are functional.

EFAULT and SIGSEGV
-------------------------

Libshim returns :code:`-1` with errno set to :code:`EFAULT` if it
is sure that a system call argument is not valid. This is expected to be
in-line with the behavior of Purecap ABI kernels.

Returning with :code:`EFAULT` is provided on a best-effort basis and it does
not cover all cases.

Because libshim checks for bounds of parameters which are string-like, but
without associated size, execution might result in a SIGSEGV during system call.
This is because the shim runs :code:`strlen` to check that the pointed memory is
readable (permissions check) and is NULL-terminated (bounds check).

ioctl()
-------

It is not feasible to support all the possible devices with all of their
respective `ioctl() <https://www.man7.org/linux/man-pages/man2/ioctl.2.html>`_
requests. In addition, libshim should only contain support for
devices in mainline kernel. As of writing this document the following
:code:`ioctl()` requests are supported:

- `binder <https://elixir.bootlin.com/linux/v5.8-rc3/source/include/uapi/linux/android/binder.h>`_
- `netdevice <https://man7.org/linux/man-pages/man7/netdevice.7.html>`_
- Limited set of `blockdevice requests <https://elixir.bootlin.com/linux/v5.8-rc3/source/include/uapi/linux/fs.h>`_
- Limited set of tty and BSD pty requests, see `here <https://elixir.bootlin.com/linux/v5.8-rc3/source/include/uapi/asm-generic/ioctls.h>`_

For details, please check the respective file under :code:`src/ioctl/`.

To extend :code:`ioctl()` with additional requests append to the code directly.
Please contribute.

keyctl()
--------

Manipulating the kernel's key management facility with
`keyctl() <https://man7.org/linux/man-pages/man2/keyctl.2.html>`_
is not implemented in details and the system call is simply forwarded.

Warning
  Calling :code:`keyctl` might result in unexpected behavior if shimming
  between different memory layouts would be necessary.

Please add support to :code:`keyctl` on-demand and contribute, if possible.

prctl() and ptrace()
--------------------

Some of the
`prctl() <https://man7.org/linux/man-pages/man2/prctl.2.html>`_
options and
`ptrace() <https://man7.org/linux/man-pages/man2/ptrace.2.html>`_
requests are not tested.

vfork()
-------

vfork is incompatible with libshim. For example, one issue is that the kernel
signal handlers registry isn't shared between child and parent processes, while
the libshim signal handlers registry is shared. Consequently, emulation of
sigaction in libshim leads to inconsistency between kernel and libshim signal
handlers registries. vfork should be replaced with a wrapper for fork in the
libc implementation.

brk()
-------

brk is unimplemented according to the pure capability kernel Linux ABI
specification. It is particularly unfriendly to the capability model, as its
mechanism relies on implicit address space reservation by moving the program
break. We assume that it is not essential, already mostly deprecated and the
allocators that currently use it can move to using mmap() instead.

sigaltstack()
-------------

Alternate signal stacks are supported with the limitation that the concrete
capability supplied in `.ss_sp`` is not preserved. Instead, a different
capability is derived from PCC found at the start of signal delivery.

The bounds of the alternate signal stack are set based on the information
provided by sigaltstack() system call. The permissions are taken from the
CSP when the signal handler is invoked.

Permissions and bounds
----------------------

It is not always possible to set strict permissions and tightest bounds when
returning capabilities from system calls because some pieces of information are
missing. For example,
`mremap <https://man7.org/linux/man-pages/man2/mremap.2.html>`_
returns capabilities with read, write and execute permissions regardless
of the input because of the API design.

User-space registries
---------------------

In some cases user-space registries are necessary to support correct runtime
behavior. For example, rt_sigaction requires a user-space map to track signal
handlers associated with different signals.

It would be best to have only one instance of such a dedicated map per process.
However, for dynamic binaries linker might have its own instances of these maps.
This is not expected to cause issues because the last-registered handler will
be executed.

Integration with libc
=====================

Libshim integrates well with Android's Bionic libc.
Roughly, the following key steps are required to use libshim:

1. Remove existing system call stubs.
2. Remove existing syscall() implementation.
3. Add libshim as a dependency to libc as whole static library.
   See :code:`bionic/libc/Android.bp` for an example.
4. Review and extend list of system calls in
   :code:`external/libshim/<libc name>_<architecture>.json`.
5. Fixup issues with generated system calls using
   :code:`external/libshim/tools/fixups.py`.
   See comments in the file for details.
6. Marshall raw arguments.

To support other libc implementations, such as glibc or musl, the above steps
should be repeated appropriately. This means that the respective Android.bp
rules have to be adopted to the build system of the targeted libc.

Supported command line arguments of libshim's generator can be listed by
invoking it without arguments: :code:`libshim/tools/main.py`.

Libshim does not expose a header to be included into libc implementations to
simplify dependencies. Therefore it is required that declarations of used
libshim interface members are directly added to a libc.

.. code-block:: cpp

  // Declaration of libshim's syscall entry point.
  uintptr_t __shim_syscall(uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t, uintptr_t);

It is generally considered a good practice to enable warnings. Many warnings,
especially those which are cast-related, are proven to be useful when porting
code to a capability architecture. It is recommended to enable compiler warnings
at least during the initial integration.

Description of syscalls.json
----------------------------

The file is a standard JSON file listing available system calls.
The following keys are available:

- decl (mandatory): The prototype of the system call without name of
  the arguments.
- symbol: the name which should be used as symbol instead of the one derived
  from "decl".
- aliases: List of symbol names which are synonyms for the system call.
- custom: True if the system call needs custom shimming.

LIBSHIM_ZERO_DDC
-------------------

The LIBSHIM_ZERO_DDC option determines whether libshim clears DDC after saving
its value or not. Clearing DDC prevents deriving tagged capabilities from it.

It is mandated that libc implementation uses
:code:`__shim_marshal_program_arguments()` whenever LIBSHIM_ZERO_DDC is enabled.
Though this is a hard requirement, in practice this even simplifies porting
various libc implementations to pure capability ABI.

LIBSHIM_CANCELLATION_POINTS
---------------------------

Some libc implementations, such as musl-libc, expect that some system calls
can be cancelled, if some conditions are met.
:code:`LIBSHIM_CANCELLATION_POINTS` control whether libshim supports such
schema. If it is set to nonzero, libshim exports two symbols,
:code:`__shim_cp_begin` and :code:`__shim_cp_end`, and changes the
system call interfaces to take an additional parameter :code:`cg`. Furthermore,
libshim expects that :code:`<ImplArgTy> __shim_cancel_syscall(void);` is
implemented.

The new parameter :code:`cg` is a pointer to an :code:`int`, or :code:`nullptr`.
When the system call is between :code:`__shim_cp_begin` and
:code:`__shim_cp_end` libshim checks :code:`cg` and jumps to
to :code:`ImplArgTy __shim_cancel_syscall(void);` only if

- :code:`cg` is not nullptr and
- the value pointed to is nonzero.

To check if a system call is cancellable, simply calculate the following in
a signal handler:

.. code-block:: cpp

  bool is_pc_cancellable(ImplArgTy pc) {
    return (ImplArgTy)__shim_cp_begin <= pc && pc < (ImplArgTy)__shim_cp_end;
  }

The type :code:`ImplArgTy` is determined as follows:

- if using :code:`-fsanitize=cheriseed` it is `ptraddr_t`,
- :code:`uintptr_t` otherwise.

Testing cancellation points is hard, therefore libshim offers an optional
feature. If :code:`int __shim_pause_in_cp(void);` is implemented, libshim calls
it, and if it returns nonzero, libshim will loop forever somewhere between
:code:`__shim_cp_begin` and :code:`__shim_cp_end` for that system call.

The new API :code:`bool __shim_supports_cancellation_points(void);` returns
whether libshim was compiled with support for cancellation points or not.

Note
  Wrappers do not support cancellation points.

Testing
=======

Libshim has some unit tests which can be considered smoke tests only.
Appropriate testing requires running the unit tests for the system components
of interest, for example bionic-unit-tests or binder tests for Android.
