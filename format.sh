#!/usr/bin/env bash
#
# Copyright (c) 2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#

clang-format -i --verbose $(find . -type f -name "*.h" -o -name "*.cpp")
black -Sv --exclude 'transforms.py|fixups.py' ./tools/
