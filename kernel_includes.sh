#!/usr/bin/env bash
#
# This script tries to figure out what include paths to use for kernel headers
# according to Multiarch specification [1]. The input is 'ARCH' environment
# variable which should be set to the name of the target architecture.
#
# For example, ARCH="aarch64" ./multiarchspec.sh
#
# [1]: https://wiki.ubuntu.com/MultiarchSpec

# Exits with a message printed to stderr.
error() {
  echo -e "ERROR"
  echo -e "$@" 1>&2
  exit 1
}

# The mode of the inclusion.
INCLUDE_MODE="${INCLUDE_MODE:=-isystem}"

# Get the host architecture.
HOST_ARCH="$(uname -m)"
case "${HOST_ARCH}" in
  x86_64|aarch64)
    ;;
  *)
    error "Compilation is not supported on ${HOST_ARCH}."
    ;;
esac

# Convert to lower case.
TARGET_ARCH="${ARCH,,}"
# Fall back to target = host, if not set.
TARGET_ARCH="${TARGET_ARCH:-${HOST_ARCH}}"
TARGET_ARCH="${TARGET_ARCH/arm64/aarch64}"
TARGET_ARCH="${TARGET_ARCH/morello/aarch64}"

# Get the name of the host OS.
if [[ -f /etc/redhat-release ]]; then
  HOST_OS="RHEL"
elif [[ -f /etc/centos-release ]]; then
  HOST_OS="CentOS"
elif [[ -f /etc/os-release ]]; then
  . /etc/os-release
  HOST_OS="${NAME}"
elif type lsb_release >/dev/null 2>&1; then
  HOST_OS="$(lsb_release -si)"
elif [[ -f /etc/lsb-release ]]; then
  . /etc/lsb-release
  HOST_OS="${DISTRIB_ID}"
elif [ -f /etc/debian_version ]; then
  HOST_OS="Debian"
else
  HOST_OS="$(uname -s)"
fi

# The default kernel include paths for a native build.
KERNEL_HDRS_ASM_INCLUDE="/usr/include/${HOST_ARCH}-linux-gnu"
KERNEL_HDR_INCLUDES="/usr/include"

case "${HOST_OS}" in
  Ubuntu*|Debian*|"Arch Linux")
    if [[ "${HOST_ARCH}" != "${TARGET_ARCH}" ]]; then
      KERNEL_HDRS_ASM_INCLUDE=
      KERNEL_HDR_INCLUDES="/usr/${TARGET_ARCH}-linux-gnu/include"
    fi
    ;;
  RHEL*|CentOS*)
    if [[ "${HOST_ARCH}" != "${TARGET_ARCH}" ]]; then
      error "Cross compilation on ${HOST_OS} is not supported."
    fi
    KERNEL_HDRS_ASM_INCLUDE=
    ;;
  *)
    error "Host configuration is not supported. Please consider contributing."
    ;;
esac

# Checks if a directory exists.
check_dir_exists() {
  [[ ! -d "$1" ]] && error "Incorrect include path detected: \"$1\" does not exist.\n\n\
    HOST_OS=${HOST_OS}\n\
    HOST_ARCH=${HOST_ARCH}\n\
    TARGET_ARCH=${TARGET_ARCH}\n\n\
    \rPlease make sure kernel headers are installed."
}

# Compose the includes.
INCLUDES=""

if [[ "${KERNEL_HDRS_ASM_INCLUDE}" ]]; then
  check_dir_exists "${KERNEL_HDRS_ASM_INCLUDE}/asm"
  INCLUDES+="${INCLUDE_MODE} ${KERNEL_HDRS_ASM_INCLUDE} "
else
  check_dir_exists "${KERNEL_HDR_INCLUDES}/asm"
fi

INCLUDES+="${INCLUDE_MODE} ${KERNEL_HDR_INCLUDES}"
check_dir_exists "${KERNEL_HDR_INCLUDES}/asm-generic"
check_dir_exists "${KERNEL_HDR_INCLUDES}/linux"

# Output the result.
echo "${INCLUDES}"
