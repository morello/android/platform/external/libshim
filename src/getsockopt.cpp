/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/filter.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"
#include "transform/sock_fprog.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int getsockopt (int, int, int, void*, socklen_t*);
// http://man7.org/linux/man-pages/man2/getsockopt.2.html
LIBSHIM_FN(getsockopt) {
  static constexpr unsigned int kNumArgs = 5;
  auto check = check::Check("getsockopt").buffer<socklen_t>(Arg{ arg4, 4 }, Arg{ arg5, 5 });

  switch (static_cast<int>(arg3)) {
#if defined(SO_ATTACH_REUSEPORT_CBPF)
    case SO_ATTACH_REUSEPORT_CBPF:
#endif
    case SO_ATTACH_FILTER: {
      transform::SockFprog sock_fprog_4{ check, Arg{ arg4, 4 } };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, sock_fprog_4, arg5, arg6, nr);
      }
    } break;

    case SO_ACCEPTCONN:
    case SO_BINDTODEVICE:
    case SO_BROADCAST:
    case SO_BSDCOMPAT:
    case SO_DEBUG:
    case SO_DETACH_FILTER:
    case SO_DOMAIN:
    case SO_ERROR:
    case SO_DONTROUTE:
#if defined(SO_INCOMING_CPU)
    case SO_INCOMING_CPU:
#endif
    case SO_KEEPALIVE:
    case SO_LINGER:
#if defined(SO_LOCK_FILTER)
    case SO_LOCK_FILTER:
#endif
    case SO_MARK:
    case SO_OOBINLINE:
    case SO_PASSCRED:
    case SO_PASSSEC:
    case SO_PEEK_OFF:
    case SO_PEERCRED:
    case SO_PRIORITY:
    case SO_PROTOCOL:
    case SO_RCVBUF:
    case SO_RCVBUFFORCE:
    case SO_RCVLOWAT:
    case SO_SNDLOWAT:
    case SO_RCVTIMEO:
    case SO_SNDTIMEO:
    case SO_REUSEADDR:
#if defined(SO_REUSEPORT)
    case SO_REUSEPORT:
#endif
    case SO_RXQ_OVFL:
#if defined(SO_SELECT_ERR_QUEUE)
    case SO_SELECT_ERR_QUEUE:
#endif
    case SO_SNDBUF:
    case SO_SNDBUFFORCE:
    case SO_TIMESTAMP:
    case SO_TIMESTAMPNS:
    case SO_TYPE:
#if defined(SO_BUSY_POLL)
    case SO_BUSY_POLL:
#endif
#if defined(SO_PEERSEC)
    // https://lwn.net/Articles/62370/
    case SO_PEERSEC:
#endif
      break;

    default:
      stl::error_message("Unknown optname value in getsockopt() shim: 0x%x.\n", arg3);
      break;
  }

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
