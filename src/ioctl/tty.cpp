/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <sys/ioctl.h>
#include <termios.h>

#include "check.h"
#include "handler.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace handler {

using morello::shim::stl::Arg;

// https://www.man7.org/linux/man-pages/man7/pty.7.html
// https://www.man7.org/linux/man-pages/man2/ioctl_tty.2.html
// TODO: this is not complete implementation. Please add cases on-demand.
LIBSHIM_IOCTL_HANDLER(tty) {
  switch (static_cast<unsigned int>(arg2)) {
    case TIOCGPTN:
      check.pointer<unsigned int>(Arg{ arg3, 3 });
      break;

    case TIOCSPTLCK:
      check.pointer<int>(Arg{ arg3, 3 });
      break;

    case TIOCGWINSZ:
    case TIOCSWINSZ:
      check.pointer<struct winsize>(Arg{ arg3, 3 });
      break;

    case TIOCGPGRP:
      check.pointer<pid_t>(Arg{ arg3, 3 });
      break;

    case TIOCSPGRP:
      check.pointer_to_const<pid_t>(Arg{ arg3, 3 });
      break;

    case TCGETS:
      check.pointer<struct termios>(Arg{ arg3, 3 });
      break;

    case TCSETSW:
      check.pointer_to_const<struct termios>(Arg{ arg3, 3 });
      break;

    case TIOCSCTTY:
      break;

    default:
      return HandlerResult::kNext;
  }

  return HandlerResult::kChecked;
}

}  // namespace handler
}  // namespace shim
}  // namespace morello
