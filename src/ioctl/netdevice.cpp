/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <net/if.h>
#include <sys/ioctl.h>

#include "check.h"
#include "handler.h"
#include "shim.h"
#include "transform/ifconf.h"

namespace morello {
namespace shim {
namespace handler {

using morello::shim::stl::Arg;

// https://www.man7.org/linux/man-pages/man7/netdevice.7.html
// Only the standard commands are handled. Please add cases on-demand.
LIBSHIM_IOCTL_HANDLER(netdevice) {
  switch (static_cast<unsigned int>(arg2)) {
    case SIOCGIFNAME:
    case SIOCGIFINDEX:
    case SIOCGIFFLAGS:
    case SIOCSIFFLAGS:
    case SIOCGIFPFLAGS:
    case SIOCSIFPFLAGS:
    case SIOCGIFADDR:
    case SIOCSIFADDR:
    case SIOCGIFDSTADDR:
    case SIOCSIFDSTADDR:
    case SIOCGIFBRDADDR:
    case SIOCSIFBRDADDR:
    case SIOCGIFNETMASK:
    case SIOCSIFNETMASK:
    case SIOCGIFMETRIC:
    case SIOCSIFMETRIC:
    case SIOCGIFMTU:
    case SIOCSIFMTU:
    case SIOCGIFHWADDR:
    case SIOCSIFHWADDR:
    case SIOCSIFHWBROADCAST:
    case SIOCGIFMAP:
    case SIOCSIFMAP:
    case SIOCADDMULTI:
    case SIOCDELMULTI:
    case SIOCGIFTXQLEN:
    case SIOCSIFTXQLEN:
    case SIOCSIFNAME: {
      if (check.pointer<struct ifreq>(Arg{ arg3, 3 }).passed()) {
        retval = LIBSHIM_SVC_NC(arg1, arg2, arg3, 0, 0, 0, __NR_ioctl);
        return HandlerResult::kHandled;
      }
    } break;

    case SIOCGIFCONF: {
      transform::IfConf ifconf_3{ check, Arg{ arg3, 3 } };
      if (check.passed()) {
        retval = LIBSHIM_SVC_NC(arg1, arg2, ifconf_3, 0, 0, 0, __NR_ioctl);
        return HandlerResult::kHandled;
      }
    } break;

    default:
      return HandlerResult::kNext;
  }

  return HandlerResult::kChecked;
}

}  // namespace handler
}  // namespace shim
}  // namespace morello
