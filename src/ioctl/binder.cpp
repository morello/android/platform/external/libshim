/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

// Needed for __BIONIC__
#include <features.h>

#if defined(__BIONIC__)

// clang-format off
// In linux/android/binder.h there are references to pid_t and uid_t but
// those are defined in sys/types.h. This inclusion should not be reordered
// after inclusion of linux/android/binder.h.
#include <sys/types.h>
// clang-format on

#include <linux/android/binder.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "check.h"
#include "handler.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace handler {

using morello::shim::stl::Arg;

// https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/android/binder.h
// TODO: this is not complete implementation. Please add cases on-demand.
LIBSHIM_IOCTL_HANDLER(binder) {
  switch (static_cast<unsigned int>(arg2)) {
    case BINDER_WRITE_READ:
      check.pointer<struct binder_write_read>(Arg{ arg3, 3 });
      break;

    case BINDER_SET_IDLE_TIMEOUT:
      check.pointer_to_const<__s64>(Arg{ arg3, 3 });
      break;

    case BINDER_SET_MAX_THREADS:
      check.pointer_to_const<__u32>(Arg{ arg3, 3 });
      break;

    case BINDER_SET_IDLE_PRIORITY:
    case BINDER_SET_CONTEXT_MGR:
    case BINDER_THREAD_EXIT:
      check.pointer_to_const<__s32>(Arg{ arg3, 3 });
      break;

    case BINDER_VERSION:
      check.pointer<struct binder_version>(Arg{ arg3, 3 });
      break;

    case BINDER_GET_NODE_DEBUG_INFO:
      check.pointer<struct binder_node_debug_info>(Arg{ arg3, 3 });
      break;

    case BINDER_GET_NODE_INFO_FOR_REF:
      check.pointer<struct binder_node_info_for_ref>(Arg{ arg3, 3 });
      break;

    case BINDER_SET_CONTEXT_MGR_EXT:
      check.pointer<struct flat_binder_object>(Arg{ arg3, 3 });
      break;

    default:
      return HandlerResult::kNext;
  }

  return HandlerResult::kChecked;
}

}  // namespace handler
}  // namespace shim
}  // namespace morello

#endif  // defined(__BIONIC__)
