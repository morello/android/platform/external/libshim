/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/fs.h>

#include "check.h"
#include "handler.h"
#include "shim.h"

namespace morello {
namespace shim {
namespace handler {

using morello::shim::stl::Arg;

// TODO: this is not complete implementation. Please add cases on-demand.
LIBSHIM_IOCTL_HANDLER(block) {
  switch (static_cast<unsigned int>(arg2)) {
    case BLKROSET:
      check.pointer_to_const<int>(Arg{ arg3, 3 });
      break;

    case BLKROGET:
    case BLKDISCARDZEROES:
      check.pointer<int>(Arg{ arg3, 3 });
      break;

    case BLKDISCARD:
      check.pointer<uint64_t[2]>(Arg{ arg3, 3 });
      break;

    default:
      return HandlerResult::kNext;
  }

  return HandlerResult::kChecked;
}

}  // namespace handler
}  // namespace shim
}  // namespace morello
