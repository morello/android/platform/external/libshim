/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"

namespace morello {
namespace shim {

using namespace check;

// clang-format off
// long keyctl(int, __kernel_ulong_t, __kernel_ulong_t, __kernel_ulong_t, __kernel_ulong_t);
// http://man7.org/linux/man-pages/man2/keyctl.2.html
// clang-format on
LIBSHIM_FN(keyctl) {
  // TODO: implement this according to the needs
  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
