/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <sys/mman.h>

#include "check.h"
#include "shim.h"
#if __SANITIZE_CHERISEED__
#include <sanitizer/cheriseed_interface.h>
#endif

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int mprotect(void *addr, size_t len, int prot);
// https://man7.org/linux/man-pages/man2/mprotect.2.html
LIBSHIM_FN(mprotect) {
  static constexpr unsigned int kNumArgs = 3;
  check::Check check{ "mprotect" };

  check.prot_is_in_max(Arg{ arg3, 3 });

  check.is_owning_capability(Arg{ arg1, 1 }, Arg{ arg2, 2 });

  check.may_set_prot(Arg{ arg1, 1 }, Arg{ arg3, 3 });

  if (check.passed()) {
    return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
