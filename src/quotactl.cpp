/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/dqblk_xfs.h>
#include <sys/quota.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int quotactl (int, const char*, int, char*);
// http://man7.org/linux/man-pages/man2/quotactl.2.html
LIBSHIM_FN(quotactl) {
  static constexpr unsigned int kNumArgs = 4;
  auto check = check::Check("quotactl").const_string(Arg{ arg2, 2 });

  // See QCMD(cmd,type) for why this shift is here.
  switch (static_cast<unsigned int>(arg1) >> SUBCMDSHIFT) {
    case Q_QUOTAON:
      check.const_string(Arg{ arg4, 4 });
      break;

    case Q_GETQUOTA:
      check.pointer<struct dqblk>(Arg{ arg4, 4 });
      break;

#if defined(Q_GETNEXTQUOTA)
    case Q_GETNEXTQUOTA:
      check.pointer<struct if_nextdqblk>(Arg{ arg4, 4 });
      break;
#endif

    case Q_SETQUOTA:
      check.pointer_to_const<struct dqblk>(Arg{ arg4, 4 });
      break;

    case Q_GETINFO:
      check.pointer<struct dqinfo>(Arg{ arg4, 4 });
      break;

    case Q_SETINFO:
      check.pointer_to_const<struct dqinfo>(Arg{ arg4, 4 });
      break;

    case Q_GETFMT:
      check.pointer<uint8_t[4]>(Arg{ arg4, 4 });
      break;

#if defined(Q_GETSTATS)
    case Q_GETSTATS:
      check.pointer<struct dqstats>(Arg{ arg4, 4 });
      break;
#endif

    case Q_XQUOTAON:
    case Q_XQUOTAOFF:
    case Q_XQUOTARM:
      check.pointer_to_const<unsigned int>(Arg{ arg4, 4 });
      break;

    case Q_XGETQUOTA:
#if defined(Q_XGETNEXTQUOTA)
    case Q_XGETNEXTQUOTA:
#endif
      check.pointer<struct fs_disk_quota>(Arg{ arg4, 4 });
      break;

    case Q_XSETQLIM:
      check.pointer_to_const<struct fs_disk_quota>(Arg{ arg4, 4 });
      break;

    case Q_XGETQSTAT:
      check.pointer<struct fs_quota_stat>(Arg{ arg4, 4 });
      break;

#if defined(Q_XGETQSTATV)
    case Q_XGETQSTATV:
      check.pointer<struct fs_quota_statv>(Arg{ arg4, 4 });
      break;
#endif

    case Q_QUOTAOFF:
    case Q_SYNC:
    case Q_XQUOTASYNC:
      break;

    default:
      stl::error_message("Unknown cmd value in quotactl() shim: 0x%x.\n", arg1);
      break;
  }

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
