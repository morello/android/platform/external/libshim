/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <sys/ptrace.h>
#include <sys/user.h>
#include <unistd.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"
#include "transform/iovec.h"
#include "transform/siginfo_t.h"
#include "transform/sock_fprog.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

#if defined(__aarch64__)
typedef struct user_fpsimd_struct __libshim_user_fp_struct_t;
#elif defined(__x86_64__)
// Required by 'struct user_desc'
#include <asm/ldt.h>

typedef struct user_fpregs_struct __libshim_user_fp_struct_t;
#else
#error "Please fix this for your target."
#endif

#if defined(__BIONIC__)
typedef struct ptrace_peeksiginfo_args __libshim_ptrace_peeksiginfo_args_t;
#else
typedef struct __ptrace_peeksiginfo_args __libshim_ptrace_peeksiginfo_args_t;
#endif

// int __ptrace (int request, int pid, void* addr, void* data);
// http://man7.org/linux/man-pages/man2/ptrace.2.html
LIBSHIM_FN(ptrace) {
  static constexpr unsigned int kNumArgs = 4;
  auto check = check::Check("ptrace");

  switch (static_cast<unsigned int>(arg1)) {
    case PTRACE_TRACEME:
    case PTRACE_PEEKTEXT:
    case PTRACE_PEEKDATA:
    case PTRACE_PEEKUSER:
    case PTRACE_POKETEXT:
    case PTRACE_POKEDATA:
    case PTRACE_POKEUSER:
    case PTRACE_SETOPTIONS:
    case PTRACE_CONT:
    case PTRACE_SYSCALL:
    case PTRACE_SINGLESTEP:
#if defined(PTRACE_SYSEMU)
    case PTRACE_SYSEMU:
#endif
#if defined(PTRACE_SYSEMU_SINGLESTEP)
    case PTRACE_SYSEMU_SINGLESTEP:
#endif
    case PTRACE_LISTEN:
    case PTRACE_KILL:
    case PTRACE_INTERRUPT:
    case PTRACE_ATTACH:
    case PTRACE_SEIZE:
    case PTRACE_DETACH:
      break;

#if defined(PTRACE_GETREGS)
    case PTRACE_GETREGS:
      check.pointer<struct user_regs_struct>(Arg{ arg4, 4 });
      break;
#endif

#if defined(PTRACE_GETFPREGS)
    case PTRACE_GETFPREGS:
      check.pointer<__libshim_user_fp_struct_t>(Arg{ arg4, 4 });
      break;
#endif

    case PTRACE_GETREGSET: {
      transform::IoVec iovec_4{ check, Arg{ arg4, 4 } };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, iovec_4, arg5, arg6, nr);
      }
    } break;

#if defined(PTRACE_SETREGS)
    case PTRACE_SETREGS:
      check.pointer_to_const<struct user_regs_struct>(Arg{ arg4, 4 });
      break;
#endif

#if defined(PTRACE_SETFPREGS)
    case PTRACE_SETFPREGS:
      check.pointer_to_const<__libshim_user_fp_struct_t>(Arg{ arg4, 4 });
      break;
#endif

    case PTRACE_SETREGSET: {
      transform::ConstIoVec iovec_4{ check, Arg{ arg4, 4 } };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, iovec_4, arg5, arg6, nr);
      }
    } break;

    case PTRACE_GETSIGINFO: {
      // No tags are preserved during the copy between the processes.
      transform::SigInfo siginfo_4{ check, Arg{ arg4, 4 } };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, siginfo_4, arg5, arg6, nr);
      }
    } break;

    case PTRACE_SETSIGINFO: {
      // PCuABI mandates that siginfo_t must not contain any capabilities.
      // Setting an invalid PID on the data will ensure that the relevant check
      // is performed during marshaling.
      transform::siginfo_t_data data{ Pid::Invalid() };
      transform::ConstSigInfo siginfo_4{ check, Arg{ arg4, 4 }, data };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, siginfo_4, arg5, arg6, nr);
      }
    } break;

#if defined(PTRACE_PEEKSIGINFO)
    case PTRACE_PEEKSIGINFO:
      // No tags are preserved during the copy between the processes.
      check.pointer<__libshim_ptrace_peeksiginfo_args_t>(Arg{ arg3, 3 });
      if (check.passed()) {
        const __libshim_ptrace_peeksiginfo_args_t* peeksiginfo_args = arg3;
        transform::SigInfos siginfos_4{ check, Arg{ arg4, 4 }, Arg{ peeksiginfo_args->nr, 3 } };
        if (check.passed()) {
          return LIBSHIM_SVC(arg1, arg2, arg3, siginfos_4, arg5, arg6, nr);
        }
      }
      break;
#endif

#if defined(PTRACE_GETSIGMASK)
    case PTRACE_GETSIGMASK:
      check.buffer(Arg{ arg4, 4 }, Arg{ arg3, 3 });
      break;
#endif

#if defined(PTRACE_GET_SYSCALL_INFO)
    case PTRACE_GET_SYSCALL_INFO:
      check.buffer(Arg{ arg4, 4 }, Arg{ arg3, 3 });
      break;
#endif

#if defined(PTRACE_SETSIGMASK)
    case PTRACE_SETSIGMASK:
      check.const_buffer(Arg{ arg4, 4 }, Arg{ arg3, 3 });
      break;
#endif

    case PTRACE_GETEVENTMSG:
      check.pointer<unsigned long>(Arg{ arg4, 4 });
      break;

#if defined(PTRACE_SECCOMP_GET_FILTER)
    case PTRACE_SECCOMP_GET_FILTER: {
      transform::SockFprog sock_fprog_4{ check, Arg{ arg4, 4 } };
      if (check.passed()) {
        return LIBSHIM_SVC(arg1, arg2, arg3, sock_fprog_4, arg5, arg6, nr);
      }
    } break;
#endif

#if defined(__x86_64__) && defined(PTRACE_GET_THREAD_AREA)
    case PTRACE_GET_THREAD_AREA:
      check.pointer<struct user_desc>(Arg{ arg4, 4 });
      break;
#endif

#if defined(__x86_64__) && defined(PTRACE_SET_THREAD_AREA)
    case PTRACE_SET_THREAD_AREA:
      check.pointer_to_const<struct user_desc>(Arg{ arg4, 4 });
      break;
#endif

    default:
      stl::error_message("Unknown request value in ptrace() shim: 0x%x.\n", arg1);
      break;
  }

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
