/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int mincore(void *addr, size_t length, unsigned char *vec);
// https://man7.org/linux/man-pages/man2/mincore.2.html
LIBSHIM_FN(mincore) {
  static constexpr unsigned int kNumArgs = 3;
  auto check = check::Check("mincore").buffer(Arg{ arg3, 3 }, Arg{ arg2.round_to_page_size(), 2 });

  check.is_owning_capability(Arg{ arg1, 1 }, Arg{ arg2, 2 });
  if (check.passed()) {
    return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
