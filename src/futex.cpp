/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/futex.h>
#include <sys/time.h>

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

using namespace check;
using morello::shim::stl::Arg;

// int __futex (int*, int, int, void*, int*, int);
// http://man7.org/linux/man-pages/man2/futex.2.html
LIBSHIM_FN(futex) {
  static constexpr unsigned int kNumArgs = 6;
  auto check = Check("futex").pointer<uint32_t>(Arg{ arg1, 1 });

  switch (arg2 & FUTEX_CMD_MASK) {
    case FUTEX_WAIT:
    case FUTEX_WAIT_BITSET:
    case FUTEX_LOCK_PI:
      check.pointer_to_const<struct timespec>(Arg{ arg4, 4 });
      break;

    case FUTEX_REQUEUE:
    case FUTEX_CMP_REQUEUE:
    case FUTEX_CMP_REQUEUE_PI:
    case FUTEX_WAKE_OP:
      check.pointer<uint32_t>(Arg{ arg5, 5 });
      break;

    case FUTEX_WAIT_REQUEUE_PI:
      check.pointer_to_const<struct timespec>(Arg{ arg4, 4 });
      check.pointer<uint32_t>(Arg{ arg5, 5 });
      break;

    case FUTEX_WAKE:
    case FUTEX_FD:
    case FUTEX_WAKE_BITSET:
    case FUTEX_TRYLOCK_PI:
    case FUTEX_UNLOCK_PI:
      break;

    default:
      stl::error_message("Unknown futex_op value in futex() shim: 0x%x.\n", arg2);
      break;
  }

  if (check.passed()) {
    return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
