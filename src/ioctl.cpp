/*
 * Copyright (c) 2020-2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

// Needed for __BIONIC__
#include <features.h>

#include "check.h"
#include "handler.h"
#include "shim.h"
#include "shimconfig.h"
#include "stl/panic.h"

namespace morello {
namespace shim {

using namespace handler;

struct IoctlHandler {
  IoctlHandler(const char* name, IoctlHandlerFn fn) : name{ name }, fn{ fn } {
  }

  const char* name;
  IoctlHandlerFn fn;
};

// List of ioctl handlers.
// Some help: https://www.man7.org/linux/man-pages/man2/ioctl_list.2.html

// clang-format off
#define LIBSHIM_IOCTL(__name) \
  IoctlHandler{ #__name, LIBSHIM_HANDLER_NAME(ioctl, __name) }
// clang-format on

// List of default handlers provided by libshim.
static const IoctlHandler kIoctlHandlers[] = {
#if defined(__BIONIC__)
  LIBSHIM_IOCTL(binder),
#endif
  LIBSHIM_IOCTL(netdevice),
  LIBSHIM_IOCTL(tty),
  LIBSHIM_IOCTL(block),
};

// int __ioctl (int, int, void*);
// http://man7.org/linux/man-pages/man2/ioctl.2.html
LIBSHIM_FN(ioctl) {
  // TODO: fd registry lookup
  // TODO: support the case where ioctl is handled incorrectly because of
  // overlapping command values.
  static constexpr unsigned int kNumArgs = 3;
  auto check = check::Check("ioctl");

  uintptr_shim_t retval;
  for (auto handler : kIoctlHandlers) {
    auto result = handler.fn(arg1, arg2, arg3, retval, check);

#if LIBSHIM_IOCTL_VERBOSE
    if (result != HandlerResult::kNext) {
      stl::error_message("ioctl(%4d, 0x%08x, ...) handled by '%s'\n", arg1, arg2, handler.name);
    }
#endif

    switch (result) {
      case HandlerResult::kChecked:
        if (check.passed()) {
          return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
        } else {
          PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
          return stl::SCReturnError(check.code());
        }

      case HandlerResult::kHandled:
        return retval;

      case HandlerResult::kNext:
      default:
        break;
    }
  }

  // Fall back to a simple svc() call and hope for the best.
  stl::error_message("Unknown request value in ioctl() shim: 0x%x.\n", arg2);
  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
