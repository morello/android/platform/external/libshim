/*
 * Copyright (c) 2021-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"
#include "stl/array.h"
#include "stl/signal.h"
#include "transform/pointer.h"

#if __SANITIZE_CHERISEED__
#include <sanitizer/cheriseed_interface.h>
#endif

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

#if __SANITIZE_CHERISEED__
// Call mmap to clear the associated tags.
static void clear_tags_with_mmap(uintptr_shim_t address, uintptr_shim_t length) {
  if (length != 0) {
    // Create a null-derived capability because of MAP_FIXED_NOREPLACE. See PCuABI for details.
    void *null_derived_addr = cheri_address_set(nullptr, address);
    void *ret_addr = ::mmap(null_derived_addr, length, PROT_READ | PROT_WRITE,
                            MAP_FIXED_NOREPLACE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (ret_addr != MAP_FAILED) {
      ::munmap(ret_addr, length);
    }
  }
}
#endif

// void* mremap(void *old_address, size_t old_size, size_t new_size, int flags, void *new_address);
// http://man7.org/linux/man-pages/man2/mremap.2.html
LIBSHIM_FN(mremap) {
  static constexpr unsigned int kNumArgs = 5;
  check::Check check{ "mremap" };

  // Check the old range if old_size is non-zero.
  // If old_size is zero, that means we are trying to duplicate a shared mapping
  // of length new_size, so check the range with this size.
  size_t length_id = arg2 != 0 ? 2 : 3;
  size_t length = arg2 != 0 ? arg2 : arg3;
  check.is_owning_capability(Arg{ arg1, 1 }, Arg{ length, length_id });

  // TODO: With respect to reservations:
  //       - If (old_address.address, new_size) doesn't fit in the old
  //         reservation, force the mapping to be moved if MREMAP_MAYMOVE
  //         is specified, or fail the call with -ENOMEM if not;
  //       - If new_address is null-derived and we have established the mapping
  //         can be resized in-place, i.e. without clashing with other mappings
  //         within the reservation, and can be resized within the reservation's
  //         bounds, return old_address on success;
  //       - If new_address is not null-derived, this would only be valid
  //         if MREMAP_FIXED and MREMAP_MAYMOVE are passed, regardless of whether
  //         the mapping can be resized in-place;
  //       - If the mapping cannot be resized in-place, the call is equivalent to
  //         mmap(new_address, new_size, prot, mmap_flags, ...), where:
  //         - prot is the old mapping's memory protection flags, which can be
  //           obtained from the capability permissions in new_address
  //           (until the relevant open issue is resolved in the spec);
  //         - mmap_flags is set to match the old mapping - either MAP_PRIVATE
  //           or MAP_SHARED - bitwise-or'ed with MAP_FIXED if flags includes
  //           MREMAP_FIXED;
  //       - If a mapping is moved from a previous reservation, destroy the
  //         the old reservation if it was the only mapping in the reservation;
  //       - If a mapping is an shm mapping, update the shm registry:
  //         duplicate the entry in partial remap case, or update the same entry
  //         if it was fully remapped.

  if (check.passed()) {
    const size_t page_size = getpagesize();
    size_t new_size = (arg3 + page_size - 1) & ~(page_size - 1);
#if __SANITIZE_CHERISEED__
    auto old_mem = arg1;
    size_t old_size = (arg2 + page_size - 1) & ~(page_size - 1);
    int flags = arg4;
    size_t copy_length = (old_size < new_size) ? old_size : new_size;

    // Create temp to store previous values of tag.
    stl::Array<char> temp(copy_length);
    void *temp_mem = temp.data();

    // Temporarily block all signals on the current thread.
    stl::ScopedSignalBlock block;

    // Locks the tags associated to the address at 'old_mem' and copies its
    // previous tag values to the tags associated to the address at 'temp_mem'.
    __cheriseed_lock_and_copy_all_tags(temp_mem, old_mem, copy_length);
#endif
    auto new_mem = LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);

    // Check for error before converting to a valid capability
    if (!stl::IsSCErrorValue(new_mem)) {
      new_mem = new_mem.to_root_pointer(new_size);
#if __SANITIZE_CHERISEED__
      char *new_start = new_mem;
      char *old_start = old_mem;
      char *new_end = new_start + new_size;
      char *old_end = old_start + old_size;

      // Clear tags associated to extended memory beyond old_size.
      if (new_size > old_size) {
        __cheriseed_clear_all_tags(new_start + old_size, new_size - old_size);
      }
      // If MREMAP_DONTUNMAP, then unlock the previously locked tags from the
      // old mapping.
      if (flags & MREMAP_DONTUNMAP) {
        __cheriseed_copy_all_tags(old_mem, temp_mem, copy_length);
      }
      // Copy all tags from the old mapping to new mapping.
      __cheriseed_copy_all_tags(new_mem, temp_mem, copy_length);

      // Unlock old tags using mmap because there might be race condition
      // for the unmapped mapping at the old_mem.
      if (!(flags & MREMAP_DONTUNMAP)) {
        // No Overlapping
        //----------------------------------------------------------------------
        //      Case 1: Outside on left
        //           New -> |...|
        //           Old ->      |...........|
        //         Clear ->      ^^^^^^^^^^^^
        //      Case 2: Outside on right
        //           New ->              |...|
        //           Old -> |...........|
        //         Clear -> ^^^^^^^^^^^^
        //----------------------------------------------------------------------
        // If [new_start, new_end) is outside of [old_start, old_end)
        if ((new_end <= old_start) || (new_start >= old_end)) {
          clear_tags_with_mmap(old_start, old_size);
        }

        // Overlapping
        //----------------------------------------------------------------------
        //     Case 1: (new_start <= old_start) && (new_end >= old_end)
        //          New -> |.............|
        //          Old ->  |...........|
        //        Clear ->     NO CLEAR
        //     Case 2: (new_start > old_start) && (new_end < old_end)
        //          New ->     |...|
        //          Old -> |...........|
        //        Clear -> ^^^^    ^^^^
        //     Case 3: Overlap on left
        //          New -> |.......|
        //          Old ->    |...........|
        //        Clear ->         ^^^^^^^
        //     Case 4: Overlap on right
        //          New ->        |.......|
        //          Old -> |...........|
        //        Clear -> ^^^^^^^
        //----------------------------------------------------------------------
        // If new_start is inside [old_start, old_end)
        if ((new_start > old_start) && (new_start < old_end)) {
          clear_tags_with_mmap(old_start, new_start - old_start);
        }
        // If new_end is inside [old_start, old_end)
        if ((new_end > old_start) && (new_end < old_end)) {
          clear_tags_with_mmap(new_end, old_end - new_end);
        }
      }
    } else {
      // Unlocks and copies the tag back to old_mem.
      __cheriseed_copy_all_tags(old_mem, temp_mem, copy_length);
#endif
    }

    return new_mem;
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
