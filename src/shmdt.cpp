/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

// int __shmdt (const void *shmaddr);
// https://man7.org/linux/man-pages/man3/shmdt.3p.html
LIBSHIM_FN(shmdt) {
  static constexpr unsigned int kNumArgs = 1;
  check::Check check{ "shmdt" };

  // TODO: With respect to reservations:
  //       - Check CapabilityOwnsRange(addr, addr.address, length),
  //         where length is the length of the shared memory segment
  //         to detach, fail with -EINVAL if conditions are not met;
  //       - The length is obtained via lookup in a registry that
  //         tracks shm information;
  //       - If the shared memory mapping to be unmapped from the
  //         calling process' address space is the only remaining
  //         mapping in its reservation, destroy the reservation;
  //       - Once unmapped, remove the corresponding shm entry from
  //         registry.

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
