/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#define _GNU_SOURCE 1

#include <fcntl.h>
#include <inttypes.h>
#include <unistd.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int fcntl (int, int, void*);
// http://man7.org/linux/man-pages/man2/fcntl.2.html
LIBSHIM_FN(fcntl) {
  static constexpr unsigned int kNumArgs = 3;
  auto check = check::Check("fcntl");

  switch (static_cast<int>(arg2)) {
    case F_SETLK:
    case F_SETLKW:
#if defined(F_OFD_SETLK)
    case F_OFD_SETLK:
#endif
#if defined(F_OFD_SETLKW)
    case F_OFD_SETLKW:
#endif
      check.pointer_to_const<struct flock>(Arg{ arg3, 3 });
      break;

    case F_GETLK:
#if defined(F_OFD_GETLK)
    case F_OFD_GETLK:
#endif
      check.pointer<struct flock>(Arg{ arg3, 3 });
      break;

    case F_GETOWN_EX:
      check.pointer<struct f_owner_ex>(Arg{ arg3, 3 });
      break;

    case F_SETOWN_EX:
      check.pointer_to_const<struct f_owner_ex>(Arg{ arg3, 3 });
      break;

#if defined(F_SET_RW_HINT)
    case F_SET_RW_HINT:
      check.pointer_to_const<uint64_t>(Arg{ arg3, 3 });
      break;
#endif

#if defined(F_SET_FILE_RW_HINT)
    case F_SET_FILE_RW_HINT:
      check.pointer_to_const<uint64_t>(Arg{ arg3, 3 });
      break;
#endif

#if defined(F_GET_RW_HINT)
    case F_GET_RW_HINT:
      check.pointer<uint64_t>(Arg{ arg3, 3 });
      break;
#endif

#if defined(F_GET_FILE_RW_HINT)
    case F_GET_FILE_RW_HINT:
      check.pointer<uint64_t>(Arg{ arg3, 3 });
      break;
#endif

    case F_DUPFD:
    case F_DUPFD_CLOEXEC:
    case F_GETFD:
    case F_SETFD:
    case F_GETFL:
    case F_SETFL:
    case F_GETOWN:
    case F_SETOWN:
    case F_GETSIG:
    case F_SETSIG:
    case F_SETLEASE:
    case F_GETLEASE:
    case F_NOTIFY:
    case F_SETPIPE_SZ:
    case F_GETPIPE_SZ:
#if defined(F_ADD_SEALS)
    case F_ADD_SEALS:
#endif
#if defined(F_GET_SEALS)
    case F_GET_SEALS:
#endif
      break;

    default:
      stl::error_message("Unknown cmd value in fcntl() shim: 0x%x.\n", arg2);
      break;
  }

  if (check.passed()) {
    return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
