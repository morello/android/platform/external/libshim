/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/reboot.h>
#include <unistd.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int __reboot (int, int, int, void*);
// http://man7.org/linux/man-pages/man2/reboot.2.html
LIBSHIM_FN(reboot) {
  static constexpr unsigned int kNumArgs = 4;
  auto check = check::Check("reboot");

  switch (static_cast<unsigned int>(arg3)) {
    case LINUX_REBOOT_CMD_RESTART2:
      check.const_string(Arg{ arg4, 4 });
      break;

    case LINUX_REBOOT_CMD_CAD_OFF:
    case LINUX_REBOOT_CMD_CAD_ON:
    case LINUX_REBOOT_CMD_HALT:
    case LINUX_REBOOT_CMD_KEXEC:
    case LINUX_REBOOT_CMD_POWER_OFF:
    case LINUX_REBOOT_CMD_RESTART:
    case LINUX_REBOOT_CMD_SW_SUSPEND:
      break;

    default:
      stl::error_message("Unknown cmd value in reboot() shim: 0x%x.\n", arg3);
      break;
  }

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
