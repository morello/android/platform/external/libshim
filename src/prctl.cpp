/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/seccomp.h>
#include <sys/prctl.h>
#include <unistd.h>

#include "check.h"
#include "shim.h"
#include "stl/panic.h"
#include "transform/prctl_mm_map.h"
#include "transform/sock_fprog.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int prctl (int, unsigned long, unsigned long, unsigned long, unsigned long);
// http://man7.org/linux/man-pages/man2/prctl.2.html
LIBSHIM_FN(prctl) {
  static constexpr unsigned int kNumArgs = 5;
  auto check = check::Check("prctl");

  switch (static_cast<unsigned int>(arg1)) {
#if defined(PR_CAP_AMBIENT)
    case PR_CAP_AMBIENT:
#endif
    case PR_CAPBSET_READ:
    case PR_CAPBSET_DROP:
    case PR_SET_CHILD_SUBREAPER:
    case PR_SET_DUMPABLE:
    case PR_GET_DUMPABLE:
    case PR_SET_ENDIAN:
#if defined(PR_SET_FP_MODE)
    case PR_SET_FP_MODE:
#endif
#if defined(PR_GET_FP_MODE)
    case PR_GET_FP_MODE:
#endif
    case PR_SET_FPEMU:
    case PR_SET_FPEXC:
    case PR_SET_KEEPCAPS:
    case PR_GET_KEEPCAPS:
    case PR_MCE_KILL:
    case PR_MCE_KILL_GET:
#if defined(PR_MPX_ENABLE_MANAGEMENT)
    case PR_MPX_ENABLE_MANAGEMENT:
#endif
#if defined(PR_MPX_DISABLE_MANAGEMENT)
    case PR_MPX_DISABLE_MANAGEMENT:
#endif
    case PR_SET_NO_NEW_PRIVS:
    case PR_GET_NO_NEW_PRIVS:
    case PR_SET_PDEATHSIG:
    case PR_SET_PTRACER:
    case PR_GET_SECCOMP:
    case PR_SET_SECUREBITS:
    case PR_GET_SECUREBITS:
#if defined(PR_GET_SPECULATION_CTRL)
    case PR_GET_SPECULATION_CTRL:
#endif
#if defined(PR_SET_SPECULATION_CTRL)
    case PR_SET_SPECULATION_CTRL:
#endif
#if defined(PR_SET_THP_DISABLE)
    case PR_SET_THP_DISABLE:
#endif
#if defined(PR_GET_THP_DISABLE)
    case PR_GET_THP_DISABLE:
#endif
    case PR_TASK_PERF_EVENTS_DISABLE:
    case PR_TASK_PERF_EVENTS_ENABLE:
    case PR_SET_TIMERSLACK:
    case PR_GET_TIMERSLACK:
    case PR_SET_TIMING:
    case PR_GET_TIMING:
    case PR_SET_TSC:
    case PR_SET_UNALIGN:
#if defined(PR_SET_IO_FLUSHER)
    case PR_SET_IO_FLUSHER:
#endif
#if defined(PR_GET_IO_FLUSHER)
    case PR_GET_IO_FLUSHER:
#endif
#if defined(PR_SET_TAGGED_ADDR_CTRL)
    case PR_SET_TAGGED_ADDR_CTRL:
    case PR_GET_TAGGED_ADDR_CTRL:
#endif
      break;

    case PR_GET_CHILD_SUBREAPER:
    case PR_GET_ENDIAN:
    case PR_GET_FPEMU:
    case PR_GET_FPEXC:
    case PR_GET_PDEATHSIG:
    case PR_GET_TSC:
      check.pointer<int>(Arg{ arg2, 2 });
      break;

    case PR_SET_NAME:
      check.const_string(Arg{ arg2, 2 });
      break;

    case PR_GET_NAME:
      check.buffer(Arg{ arg2, 2 }, Arg{ 16 });
      break;

    case PR_SET_SECCOMP: {
      switch (static_cast<unsigned long>(arg2)) {
        case SECCOMP_MODE_FILTER: {
          transform::ConstSockFprog sock_fprog_3{ check, Arg{ arg3, 3 } };
          if (check.passed()) {
            return LIBSHIM_SVC(arg1, arg2, sock_fprog_3, arg4, arg5, arg6, nr);
          }
        } break;

        case SECCOMP_MODE_DISABLED:
        case SECCOMP_MODE_STRICT:
          break;

        default:
          stl::error_message("Unknown PR_SET_SECCOMP moder prctl() shim: 0x%x.\n", arg2);
          break;
      }
    } break;

    case PR_GET_TID_ADDRESS:
      check.pointer<int*>(Arg{ arg2, 2 });
      break;

    case PR_GET_UNALIGN:
      check.pointer<unsigned int>(Arg{ arg2, 2 });
      break;

#if defined(PR_SET_VMA)
    case PR_SET_VMA: {
      switch (arg2) {
#if defined(PR_SET_VMA_ANON_NAME)
        case PR_SET_VMA_ANON_NAME:
          check.const_buffer(Arg{ arg3, 3 }, Arg{ arg4, 4 }).const_string(Arg{ arg5, 5 });
          break;
#endif

        default:
          stl::error_message("Unknown arg2 in prctl(PR_SET_VMA) shim: 0x%X.\n", arg2);
          break;
      }
    } break;
#endif

    case PR_SET_MM:
      switch (arg2) {
        case PR_SET_MM_START_CODE:
        case PR_SET_MM_END_CODE:
          // FIXME: arg3
          // The corresponding memory area must be readable and executable, but not writable or
          // shareable.
          break;

        case PR_SET_MM_START_DATA:
        case PR_SET_MM_END_DATA:
          // FIXME: arg3
          // The corresponding memory area must be readable and writable, but not executable or
          // shareable.
          break;

        case PR_SET_MM_START_STACK:
        case PR_SET_MM_ARG_START:
        case PR_SET_MM_ARG_END:
        case PR_SET_MM_ENV_START:
        case PR_SET_MM_ENV_END:
          // FIXME: arg3
          // The corresponding memory area must be readable and writable.
          break;

        case PR_SET_MM_AUXV:
          // FIXME: arg3, arg4
          // The arg3 argument should provide the address of the vector.  The arg4 is the size of
          // the vector.
          break;

#if defined(PR_SET_MM_MAP)
        case PR_SET_MM_MAP: {
          if (!check.buffer(Arg{ arg3, 3 }, Arg{ arg4, 4 }).passed()) {
            break;
          }

          transform::ConstPrctlMmMap prctl_mm_map_3{ check, Arg{ arg3, 3 } };
          if (check.passed()) {
            return LIBSHIM_SVC(arg1, arg2, prctl_mm_map_3, arg4, arg5, arg6, nr);
          }
        } break;
#endif

#if defined(PR_SET_MM_MAP_SIZE)
        case PR_SET_MM_MAP_SIZE:
          check.pointer<unsigned int>(Arg{ arg4, 4 });
          break;

        case PR_SET_MM_START_BRK:
        case PR_SET_MM_BRK:
        case PR_SET_MM_EXE_FILE:
          break;

        default:
          stl::error_message("Unknown arg2 in prctl(PR_SET_MM) shim: 0x%X.\n", arg2);
          break;
#endif
      }
      break;

    default:
      stl::error_message("Unknown option value in prctl() shim: 0x%X.\n", arg1);
      break;
  }

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
}

}  // namespace shim
}  // namespace morello
