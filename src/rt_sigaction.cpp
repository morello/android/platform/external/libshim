/*
 * Copyright (c) 2020-2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <signal.h>

#include "check.h"
#include "shim.h"
#include "shimconfig.h"
#include "stl/panic.h"
#include "stl/registry.h"
#include "stl/signal.h"
#include "transform/sigaction.h"
#include "transform/sigevent_t.h"
#include "transform/siginfo_t.h"
#include "transform/ucontext_t.h"

namespace morello {
namespace shim {

using namespace check;
using namespace transform;

// Structure to hold either sa_handler or sa_sigaction function pointers.
struct SigActionHandler {
  SigActionHandler() noexcept : handler{ SIG_DFL }, is_sigaction{ false } {
  }

  SigActionHandler(uintptr_shim_t handler, bool is_sigaction) noexcept
      : handler{ handler }, is_sigaction{ is_sigaction } {
  }

  // Callable as 'sa_handler'.
  void operator()(int sig) const noexcept {
    void (*sa_handler_ptr)(int) = handler;
    sa_handler_ptr(sig);
  }

  // Callable as 'sa_sigaction'.
  void operator()(int sig, siginfo_t* info, void* ucontext) const noexcept {
    void (*sa_sigaction_ptr)(int, siginfo_t*, void*) = handler;
    sa_sigaction_ptr(sig, info, ucontext);
  }

  // Return true if the registered handler seems to be valid.
  bool is_valid() const noexcept {
    return handler.is_valid_pointer();
  }

  // Pretty-prints the structure to the given fd.
  void display(int fd) const noexcept {
    const char* type_str;
    if (is_sigaction) {
      type_str = "[SA_SIGACTION]";
    } else {
      type_str = "[SA_HANDLER]";
    }

    void* ptr = handler;
    stl::dnprintf(fd, 64, "0x%016x %s\n", ptr, type_str);
  }

  uintptr_shim_t handler;
  bool is_sigaction;
};

// SigActionHandler can safely be constructed with setting its memory to zeros.
ZERO_CONSTRUCTIBLE(SigActionHandler);

// The global SigActionHandler registry.
//
// This variable is placed into .bss and therefore is cleared on load.
// However, accessing this registry races with running its constructor,
// which does nothing, but ~clears the underlying memory. Could have used a
// local static variable but that would have posed dependence on __cxa_* guards.
// This could have been worked around with `-fno-threadsafe-statics` flag or by
// implementing the required functions. Yet, to provide a solution which is
// expected to work in various environments, the implementation chooses to
// reserve a zeroed memory region, cast that and use it as a registry.
// This further mandates that the registry is always mmap-backed.
static stl::StaticRegistry<SigActionHandler> SigActionHandlerRegistry;

// This is an implementation of a wrapper for signal handlers.
// The handler looks up the registered signal handler and invokes it
// appropriately. This is required because signal handler arguments need to be
// transformed.
static void SignalHandlerWrapper(int sig, target_pointer_t t_info, target_pointer_t t_ucontext) {
  auto& registry = SigActionHandlerRegistry();
  const SigActionHandler sigaction = registry[sig];

  if (!sigaction.is_valid()) {
    stl::panic("Sigaction not found for signal = %d in pid = %d, tid = %d.\n", sig, getpid(),
               gettid());
  } else if (sigaction.is_sigaction) {
    // Transform siginfo_t and ucontext_t from target layout into local layout.
    target_siginfo_t_t* info =
        t_info.to_data_pointer<target_siginfo_t_t*>(sizeof(target_siginfo_t_t));
    local_siginfo_t_t local_info;
    *info >> local_info;

    target_ucontext_t_t* ucontext =
        t_ucontext.to_data_pointer<target_ucontext_t_t*>(sizeof(target_ucontext_t_t));
    local_ucontext_t_t local_ucontext;
    *ucontext >> local_ucontext;

    sigaction(sig, &local_info, &local_ucontext);

    // Write back ucontext in case it got modified by the signal handler.
    *ucontext << local_ucontext;
  } else {
    sigaction(sig);
  }
}

#if defined(__CHERI_PURE_CAPABILITY__) && !defined(__SANITIZE_CHERISEED__)
extern "C" {

// This is a wrapper for SignalHandlerWrapper with unmangled name.
__attribute__((used)) static void SignalHandlerWrapperC(int sig, target_pointer_t t_info,
                                                        target_pointer_t t_ucontext) {
  SignalHandlerWrapper(sig, t_info, t_ucontext);
}

// This is a helper for the wrapper trampoline which queries whether the
// current thread is running on an alternate signal stack, and if that's
// the case, prepares a capability for CSP for this signal handler invocation.
// Otherwise, it returns the original CSP supplied to the function.
__attribute__((used)) static uintcap_t SignalHandlerWrapperTrampolineHelper(
    uintcap_t cap_for_new_csp, uintcap_t original_csp) {
  stack_t signal_stack_info;
  sigaltstack(NULL, &signal_stack_info);

  if (signal_stack_info.ss_flags & SS_ONSTACK) {
    ptraddr_t stack_base = cheri_address_get(signal_stack_info.ss_sp);
    size_t stack_size = signal_stack_info.ss_size;

    uintptr_t new_csp = cheri_address_set(cap_for_new_csp, stack_base);
    new_csp = cheri_bounds_set(new_csp, stack_size);
    new_csp = cheri_perms_and(new_csp, cheri_perms_get(original_csp));
    new_csp = cheri_address_set(new_csp, cheri_address_get(original_csp));

    return new_csp;
  }

  return original_csp;
}

}  // extern "C"

// This is the signal handler wrapper trampoline which corrects CSP bounds in
// case the signal handler is invoked on an alternate signal stack.
// This is required because the capability-unaware kernel can't receive
// the capability metadata available via the stack_t in sigaltstack system call,
// and so can't set it in the CSP when invoking the signal handler.
LIBSHIM_NAKED
static void SignalHandlerWrapperTrampoline(int sig, target_pointer_t t_info,
                                           target_pointer_t t_ucontext) {
  __asm__(
      // Preserve the signal handler parameters, the stack and return capabilities.
      // Callee-saved registers can be used here because their values are going to
      // be restored by the kernel upon return from the signal handler.
      "mov     x19, x0\n\t"
      "mov     x20, x1\n\t"
      "mov     x21, x2\n\t"
      "mov     c22, clr\n\t"
      "mov     c23, csp\n\t"
      // Load __shim_data_cap.
      "adrp    c3, :got:__shim_data_cap\n\t"
      "ldr     c3, [c3, :got_lo12:__shim_data_cap]\n\t"
      "ldr     c3, [c3]\n\t"
      // Get current CSP value.
      "gcvalue x4, csp\n\t"
      // Combine it with the global data capability to derive new temporary
      // capability for CSP that isn't sufficiently bounded however is suitable
      // to allow the helper function to allocate a stack frame.
      "scvalue c4, c3, x4\n\t"
      // Set CSP to the temporary capability.
      "mov     csp, c4\n\t"
      // Derive and set the CSP value for this signal handler invocation.
      "mov     c0, csp\n\t"
      "mov     c1, c23\n\t"
      "bl      SignalHandlerWrapperTrampolineHelper\n\t"
      "mov     csp, c0\n\t"
      // Restore the signal handler parameters and proceed to handling the signal.
      "mov     x0, x19\n\t"
      "mov     x1, x20\n\t"
      "mov     x2, x21\n\t"
      "bl      SignalHandlerWrapperC\n\t"
      // Restore the original stack and return capabilities
      "mov     clr, c22\n\t"
      "mov     csp, c23\n\t"
      "ret\n\t");
}
#endif /* __CHERI_PURE_CAPABILITY__ && !__SANITIZE_CHERISEED__ */

// clang-format off
// int __rt_sigaction (int, const struct sigaction*, struct sigaction*, size_t);
// http://man7.org/linux/man-pages/man2/rt_sigaction.2.html
// clang-format on
LIBSHIM_FN(rt_sigaction) {
  static constexpr unsigned int kNumArgs = 4;
  // clang-format off
  auto check = Check("rt_sigaction")
    .require_executive()
    .pointer_to_const<const local_sigaction_t>(Arg{arg2, 2})
    .pointer<local_sigaction_t>(Arg{arg3, 3});
  // clang-format on

  // Early return in case any of the inputs would be incorrect.
  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  // Temporarily block all signals on the current thread to avoid
  // potential deadlocks and racing with the signal handler.
  stl::ScopedSignalBlock block;

  const int signum = arg1;
  const local_sigaction_t* const orig_act = arg2;
  const local_sigaction_t* act = orig_act;
  local_sigaction_t new_act = {};  // Zero-initialize
  bool is_sigaction = false;

  // Check if 'act' is set.
  const bool is_act_valid = (act != nullptr);
  if (is_act_valid) {
    is_sigaction = ((act->flags & SA_SIGINFO) == SA_SIGINFO);
#if LIBSHIM_SIGACTION_VERBOSE
    if (is_sigaction) {
      stl::error_message("signum = %d rt_sigaction.sa_sigaction = %p pid = %d tid = %d\n", signum,
                         act->handler, getpid(), gettid());
    } else {
      stl::error_message("signum = %d rt_sigaction.sa_handler = %p pid = %d tid = %d\n", signum,
                         act->handler, getpid(), gettid());
    }
#endif

    // Exclude SIG_DFL, SIG_IGN, SIG_HOLD and any other invalid pointers.
    uintptr_shim_t handler = act->handler;
    if (handler.is_valid_pointer()) {
      new_act = *act;
#if defined(__SANITIZE_CHERISEED__) || !defined(__CHERI_PURE_CAPABILITY__)
      new_act.handler = reinterpret_cast<sighandler_t>(&SignalHandlerWrapper);
#else  /* __SANITIZE_CHERISEED__ || !__CHERI_PURE_CAPABILITY__ */
      new_act.handler = reinterpret_cast<sighandler_t>(&SignalHandlerWrapperTrampoline);
#endif /* !__SANITIZE_CHERISEED__ && __CHERI_PURE_CAPABILITY__ */
      act = &new_act;
    }
  }

  // Using a block to invoke destructors of transformations.
  int ret;
  {
    ConstSigAction sigaction_2{ check, Arg{ act, 2 } };
    SigAction sigaction_3{ check, Arg{ arg3, 3 } };
    if (!check.passed()) {
      PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
      return stl::SCReturnError(check.code());
    }
    ret = LIBSHIM_SVC(arg1, sigaction_2, sigaction_3, arg4, arg5, arg6, nr);
  }

  // If the call was successful, handle 'oldact'.
  if (ret == 0) {
    local_sigaction_t* oldact = arg3;
    auto& registry = SigActionHandlerRegistry();

    if (is_act_valid) {
      // Update the handler in the registry and return the previous one.
      const SigActionHandler old_sigaction =
          registry.update(signum, SigActionHandler{ orig_act->handler, is_sigaction });
      if (oldact != nullptr) {
        oldact->handler = old_sigaction.handler;
      }
    } else {
      if (oldact != nullptr) {
        // Return the handler from registry.
        oldact->handler = registry[signum].handler;
      }
    }

#if LIBSHIM_SIGACTION_VERBOSE
    SigActionHandlerRegistry().display();
#endif
  }

  return ret;
}

}  // namespace shim
}  // namespace morello
