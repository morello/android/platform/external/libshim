/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <sys/mman.h>

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

// int munmap(void *addr, size_t length);
// https://man7.org/linux/man-pages/man3/munmap.3p.html
LIBSHIM_FN(munmap) {
  static constexpr unsigned int kNumArgs = 2;
  check::Check check{ "munmap" };

  // TODO: With respect to reservations, destroy a mapping's reservation
  //       if it was the only remaining mapping.

  check.is_owning_capability(Arg{ arg1, 1 }, Arg{ arg2, 2 });

  if (check.passed()) {
    return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
