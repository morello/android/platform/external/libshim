/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

// void* __shmat (int shmid, const void *shmaddr, int shmflg);
// https://man7.org/linux/man-pages/man3/shmat.3p.html
LIBSHIM_FN(shmat) {
  static constexpr unsigned int kNumArgs = 3;
  check::Check check{ "shmat" };

  // TODO: With respect to reservations:
  //       - The call to shmat() behaves as mmap(addr, length, prot, flags, ...)
  //         - handle appropriate setting of each of these parameters;
  //       - The length is the length of the shared memory segment identified
  //         by shmid, obtained via a call to shmctl(shmid);
  //       - Upon success, insert the corresponding new entry into the
  //         shm registry.

  if (!check.passed()) {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  auto ret = LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
  return ret.to_valid_data_pointer_unbounded();
}

}  // namespace shim
}  // namespace morello
