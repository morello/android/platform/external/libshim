/*
 * Copyright (c) 2021-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <sys/mman.h>
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
#include <sys/cheri.h>
#endif

#include "check.h"
#include "shim.h"
#if __SANITIZE_CHERISEED__
#include <sanitizer/cheriseed_interface.h>
#endif

namespace morello {
namespace shim {

using morello::shim::stl::Arg;

#ifdef __CHERI_PURE_CAPABILITY__
#if LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED
static bool may_have_prot_flag(int prot, int flag) {
  int prot_max = PROT_MAX_EXTRACT(prot);
  if (prot_max != 0) {
    return (prot_max & flag) != 0;
  } else {
    return (prot & flag) != 0;
  }
}

static void *build_owning_capability(uintptr_shim_t start_addr, size_t length, int prot,
                                     bool has_tag_access) {
  // TODO: use exact bounds once the reservation manager ensures they are representable
  void *cap = start_addr.to_root_pointer(length);

  uint64_t perms = __CHERI_CAP_PERMISSION_GLOBAL__
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
                   | CHERI_PERM_SW_VMEM
#endif  // LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
      ;
  if (may_have_prot_flag(prot, PROT_READ)) {
    perms |= __CHERI_CAP_PERMISSION_PERMIT_LOAD__;
    if (has_tag_access) {
#ifdef __ARM_CAP_PERMISSION_MUTABLE_LOAD__
      perms |= __ARM_CAP_PERMISSION_MUTABLE_LOAD__;
#endif
      perms |= __CHERI_CAP_PERMISSION_PERMIT_LOAD_CAPABILITY__;
    }
  }
  if (may_have_prot_flag(prot, PROT_WRITE)) {
    perms |= __CHERI_CAP_PERMISSION_PERMIT_STORE__;
    if (has_tag_access) {
      perms |= __CHERI_CAP_PERMISSION_PERMIT_STORE_CAPABILITY__;
      perms |= __CHERI_CAP_PERMISSION_PERMIT_STORE_LOCAL__;
    }
  }
  if (may_have_prot_flag(prot, PROT_EXEC)) {
    perms |= __CHERI_CAP_PERMISSION_PERMIT_EXECUTE__;
    // The PCC obtained below does not necessary reflect the libc PCC,
    // as the binary could have been dynamically linked.
    // However, until there is compartmentalisation support we would always
    // expect the executive permission to be present, so for now it doesn't
    // make a difference in practice.
    // TODO: obtain permissions of true PCC.
    uint64_t pcc_perms = cheri_perms_get(cheri_pcc_get());
#ifdef __ARM_CAP_PERMISSION_EXECUTIVE__
    if (pcc_perms & __ARM_CAP_PERMISSION_EXECUTIVE__) {
      perms |= __ARM_CAP_PERMISSION_EXECUTIVE__;
    }
#endif
    if (pcc_perms & __CHERI_CAP_PERMISSION_ACCESS_SYSTEM_REGISTERS__) {
      perms |= __CHERI_CAP_PERMISSION_ACCESS_SYSTEM_REGISTERS__;
    }
  }
#ifdef __ARM_CAP_PERMISSION_BRANCH_SEALED_PAIR__
  if (prot & PROT_CAP_INVOKE) {
    perms |= __ARM_CAP_PERMISSION_BRANCH_SEALED_PAIR__;
  }
#endif
  return cheri_perms_and(cap, perms);
}
#else  /* !LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED */
static void *build_owning_capability(uintptr_shim_t start_addr, size_t length, int prot,
                                     bool has_tag_access) {
  // TODO: use exact bounds once the reservation manager ensures they are representable
  return start_addr.to_root_pointer(length);
}
#endif /* !LIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED */

// Check for null-derived capability - tag not set, top bits cleared.
static bool is_null_derived(void *ptr) {
  uintptr_t null_cap = 0;
  const ptraddr_t address = cheri_address_get(ptr);
  null_cap = cheri_address_set(null_cap, address);
  return cheri_is_equal_exact(ptr, null_cap);
}
#endif /* !__CHERI_PURE_CAPABILITY__ */

// void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
// https://man7.org/linux/man-pages/man2/mmap.2.html
LIBSHIM_FN(mmap) {
  check::Check check{ "mmap" };

  // TODO: With respect to reservations:
  //       - If addr is null-derived, attempt to create a new reservation
  //         in accordance with the rules (as specified under no. 1 for mmap
  //         in the spec);
  //       - If CapabilityOwnsRange(addr, addr.address, length) holds within
  //         an existing reservation, return addr if the call succeeds or
  //         fail with -EINVAL if CapabilityMaySetProt(addr, prot) doesn't hold
  //         or if the flags do not include MAP_FIXED nor MAP_FIXED_NOREPLACE;
  //       - For any other addr (i.e. not an owning capability nor null-derived),
  //         fail with -EINVAL.

  void *addr = arg1;
  size_t length = arg2;
  int prot = arg3;
  int flags = arg4;
  int fd = arg5;
  off_t offset = arg6;

#ifdef __CHERI_PURE_CAPABILITY__
  static constexpr unsigned int kNumArgs = 6;
  uintptr_shim_t ret;

  check.expect((flags & MAP_GROWSDOWN) == 0, "MAP_GROWSDOWN is not supported", EOPNOTSUPP);
  check.prot_is_in_max(Arg{ prot, 3 });

  // pre-checks
  if (is_null_derived(addr)) {
    if (flags & MAP_FIXED) {
      flags &= ~MAP_FIXED;
      flags |= MAP_FIXED_NOREPLACE;
    }
  } else {
    check.is_owning_capability(Arg{ addr, 1 }, Arg{ length, 2 });
    check.may_set_prot(Arg{ addr, 1 }, Arg{ prot, 3 });
    check.expect(flags & (MAP_FIXED | MAP_FIXED_NOREPLACE),
                 "neither MAP_FIXED nor MAP_FIXED_NOREPLACE are set", EINVAL);
  }

  if (check.passed()) {
    ret = LIBSHIM_SVC(addr, length, prot, flags, fd, offset, nr);
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }

  // Check for error before converting to a valid capability
  if (!stl::IsSCErrorValue(ret)) {
    // The below check is for backward compatibility for older kernels.
    // Please refer to the notes under MAP_FIXED_NOREPLACE
    // in the man page - https://man7.org/linux/man-pages/man2/mmap.2.html
    if (((flags & MAP_FIXED_NOREPLACE) != 0) && (addr != ret)) {
      // Ignore the result of munmap.
      ::munmap(ret, length);

      PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
      return stl::SCReturnError(EEXIST);
    }

    if (is_null_derived(addr)) {
      length = __builtin_align_up(length, getpagesize());
      ret = build_owning_capability(ret, length, prot, flags & MAP_PRIVATE);
    } else {
      ret = addr;
    }

#if __SANITIZE_CHERISEED__
    __cheriseed_clear_all_tags(ret, length);
#endif
  }

  return ret;
#else
  return LIBSHIM_SVC(addr, length, prot, flags, fd, offset, nr);
#endif
}

}  // namespace shim
}  // namespace morello
