/*
 * Copyright (c) 2021-2022 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"
#include "transform/pointer.h"

namespace morello {
namespace shim {

// void* __brk (void*);
// http://man7.org/linux/man-pages/man2/brk.2.html
LIBSHIM_FN(brk) {
#ifdef __CHERI_PURE_CAPABILITY__
  // brk is unimplemented and will always fail with ENOSYS.
  return stl::SCReturnError(ENOSYS);
#else
  return LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
#endif
}

}  // namespace shim
}  // namespace morello
