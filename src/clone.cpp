/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <linux/sched.h>

#include "check.h"
#include "shim.h"

namespace morello {
namespace shim {

using namespace check;

// int __clone (int, void*, pid_t*, void*, pid_t*);
// http://man7.org/linux/man-pages/man2/clone.2.html
LIBSHIM_FN(clone) {
  static constexpr unsigned int kNumArgs = 5;
  // clang-format off
  auto check = Check("clone")
    .buffer(Arg{arg2, 2}, Arg{1}) // child_stack
    .const_buffer(Arg{arg4, 4}, Arg{1}) // TLS
    ;
  // clang-format on

  if (check.passed()) {
    int ret = LIBSHIM_SVC(arg1, arg2, arg3, arg4, arg5, arg6, nr);
#if defined(__CHERI_PURE_CAPABILITY__) && !defined(__SANITIZE_CHERISEED__)
    if (!stl::IsSCErrorValue(ret)) {
      // Kernel does not yet support setting CTPIDR_EL0 via clone()
      // because kernel doesn't accept capabilities in general.
      // However, it does context-switch CTPIDR_EL0.
      if ((arg1 & CLONE_SETTLS) == CLONE_SETTLS) {
        void* ctpidr_el0 = arg4;
        // clang-format off
        asm volatile(
            "msr  ctpidr_el0, %0\n\t"
            : : "C"(ctpidr_el0) :
        );
        // clang-format on
      }
    }
#endif
    return ret;
  } else {
    PRINT_CHECKED_CALL_FAILED(check, kNumArgs);
    return stl::SCReturnError(check.code());
  }
}

}  // namespace shim
}  // namespace morello
