/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <elf.h>
#include <inttypes.h>
#include <signal.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/syscall.h>

#include "shim.h"

extern "C" {

// Returns true if compiled for pure-capability ABI, otherwise false.
bool __shim_is_pure_capability() {
#if defined(__CHERI_PURE_CAPABILITY__)
  return true;
#else
  return false;
#endif
}

// Returns true if cancellation points are supported, otherwise false.
bool __shim_supports_cancellation_points() {
#if LIBSHIM_CANCELLATION_POINTS
  return true;
#else
  return false;
#endif
}

}  // extern "C"

// -----------------------------------------------------------------------------
// Everything below is only available when targeting pure-capability ABI.

#if defined(__CHERI_PURE_CAPABILITY__)

namespace morello {
namespace shim {

// Size of the { argc, argv, envp, auxv } output array/registry size
static constexpr size_t kOutputsRegistrySize = 4 * sizeof(uintcap_t);

// clang-format off

static constexpr uint64_t kReadCapPerms =
#ifdef __ARM_CAP_PERMISSION_MUTABLE_LOAD__
    __ARM_CAP_PERMISSION_MUTABLE_LOAD__ |
#endif
    __CHERI_CAP_PERMISSION_PERMIT_LOAD__ |
    __CHERI_CAP_PERMISSION_PERMIT_LOAD_CAPABILITY__;

static constexpr uint64_t kWriteCapPerms =
    __CHERI_CAP_PERMISSION_PERMIT_STORE__ |
    __CHERI_CAP_PERMISSION_PERMIT_STORE_CAPABILITY__ |
    __CHERI_CAP_PERMISSION_PERMIT_STORE_LOCAL__;

static constexpr uint64_t kExeCapPerms =
#ifdef __ARM_CAP_PERMISSION_EXECUTIVE__
    __ARM_CAP_PERMISSION_EXECUTIVE__ |
#endif
    __CHERI_CAP_PERMISSION_PERMIT_EXECUTE__ |
    __CHERI_CAP_PERMISSION_ACCESS_SYSTEM_REGISTERS__;

static constexpr uint64_t kRootCapPerms =
#ifdef __ARM_CAP_PERMISSION_BRANCH_SEALED_PAIR__
    __ARM_CAP_PERMISSION_BRANCH_SEALED_PAIR__ |
#endif
#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
    CHERI_PERM_SW_VMEM |
#endif
    __CHERI_CAP_PERMISSION_GLOBAL__;

static constexpr uint64_t kStackCapPerms =
    __CHERI_CAP_PERMISSION_GLOBAL__ |
    kReadCapPerms | kWriteCapPerms;

static constexpr uint64_t kSealCapPerms =
    __CHERI_CAP_PERMISSION_GLOBAL__ |
    __CHERI_CAP_PERMISSION_PERMIT_SEAL__ |
    __CHERI_CAP_PERMISSION_PERMIT_UNSEAL__;

static constexpr uint64_t kCidCapPerms =
#ifdef __ARM_CAP_PERMISSION_COMPARTMENT_ID__
    __ARM_CAP_PERMISSION_COMPARTMENT_ID__ |
#endif
    __CHERI_CAP_PERMISSION_GLOBAL__;

// clang-format on

// This number is different across architectures.
// Should it be defined by the toolchain?
#define OTYPE_NUM_BITS 15

// There are some new AT_CHERI* auxiliary values.
#define AUXV_EXTRA_COUNT 11

// Stand-in value for cheri.max_stack_size syscall parameter.
#define LIBSHIM_MAX_STACK_SIZE 0x800000

// Size in bytes of the random value.
#define LIBSHIM_AT_RANDOM_SIZE 16

extern "C" {
// Global data capability for the shim layer.
uintcap_t __shim_data_cap;
}

// Layout of the auxiliary values we are marshalling from.
struct Elf64_auxv_kernel_t {
  uint64_t a_type;
  uint64_t a_val;
};

// Define own type of 'Elf64_auxv_t': '__padding' named member allows
// this code to zero those bytes. This is what kernel would do. For libc
// implementations it is not mandatory to have '__padding' in the definition.
// Having this type defined here means that this is now libc independent.
// An alternative would be to use memset, but we cannot call any library
// functions because relocations may not have been resolved yet.
struct Elf64_auxv_t {
  uint64_t a_type;
  uint64_t __padding;
  union {
    uint64_t a_val;
    void *a_ptr;
  } a_un;
};

template <typename T>
struct PtrRange {
  PtrRange() : start(nullptr), end(nullptr) {
  }

  PtrRange(const T *s, const T *e) : start(s), end(e) {
  }

  const T *start;
  const T *end;
};

struct PhdrScanInfo {
  ptraddr_t min_rw, max_rw, min_rx, max_rx;
  bool rw_parsed, rx_parsed;
};

// Simple helper to make code a little bit shorter.
template <typename T = uintcap_t>
static T cap_address_set(uintcap_t cap, ptraddr_t value) {
  return reinterpret_cast<T>(cheri_address_set(cap, value));
}

// Restricts bounds and permissions of a null-terminated string.
static uintcap_t restrict_string_use(uintcap_t cap, int perms) {
  const char *const start = reinterpret_cast<char *>(cap);
  const char *end = start;
  for (; *end; ++end)
    ;
  // Set bounds to 'strlen(arg) + 1', which includes the terminating '\0'.
  // TODO: using exact bounds will require realignment of arguments.
  cap = cheri_bounds_set(cap, end - start + 1);
  return cheri_perms_and(cap, perms);
}

// Writes an auxilary value as 'a_val'.
static void write_auxval(Elf64_auxv_t *loc, uint64_t a_type, uint64_t a_val) {
  loc->a_type = a_type;
  loc->__padding = 0;
  loc->a_un.a_ptr = nullptr;  // Clear the whole 'a_un'.
  loc->a_un.a_val = a_val;
}

// Writes an auxilary value as 'a_ptr'.
static void write_auxval(Elf64_auxv_t *loc, uint64_t a_type, uintcap_t a_ptr) {
  loc->a_type = a_type;
  loc->__padding = 0;
  loc->a_un.a_ptr = reinterpret_cast<void *>(a_ptr);
}

// Simple helper to make code a little bit shorter.
static uintcap_t global_data_cap() {
  return reinterpret_cast<uintcap_t>(cheri_ddc_get());
}

// A helper triggering SIGABRT.
//
// Main abort implementation isn't used here because the dynamic linkage hasn't
// happened at this point.
static void early_helper_abort(void) {
  LIBSHIM_FN_CALL_NC(svc, 0, SIGABRT, 0, 0, 0, 0, __NR_kill);

  /* should not be reached */
  __builtin_trap();
}

// A helper to obtain the max. stack size via call to getrlimit().
// Returns:
//   The stack region resource limit.
static size_t early_helper_getrlimit_max_stack_size() {
  rlimit rlim;
  long ret = LIBSHIM_FN_CALL_NC(svc, RLIMIT_STACK, &rlim, 0, 0, 0, 0, __NR_getrlimit);
  if (ret < 0) {
    early_helper_abort();
  }
  return rlim.rlim_max;
}

// A wrapper over capability-unaware mmap system call for mapping a new private
// anonymous memory region.
//
// Main mmap implementation isn't used here because the dynamic linkage hasn't
// happened at this point.
static ptraddr_t early_helper_mmap(ptraddr_t addr, size_t size, int prot, int additional_flags) {
  long ret = LIBSHIM_FN_CALL_NC(svc, addr, size, prot,
                                MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0, __NR_mmap);

  if (ret < 0) {
    early_helper_abort();
  }

  return static_cast<ptraddr_t>(ret);
}

// A wrapper over capability-unaware munmap system call.
//
// Main munmap implementation isn't used here because the dynamic linkage
// hasn't happened at this point.
static void early_helper_munmap(ptraddr_t addr, size_t size) {
  if (size == 0) {
    return;
  }

  long ret = LIBSHIM_FN_CALL_NC(svc, addr, size, 0, 0, 0, 0, __NR_munmap);

  if (ret < 0) {
    early_helper_abort();
  }
}

// A helper for mapping a new private anonymous memory region. Memory outside
// the requested region but within the bounds of the returned capability is
// guaranteed to be reserved i.e. mapped with PROT_NONE.
// Optionally, leave two guard pages - right before and right after the region.
//
// Main mmap implementation isn't used here because the dynamic linkage hasn't
// happened at this point.
static uintptr_t early_helper_mmap_region(size_t size, int additional_flags, size_t page_size,
                                          bool add_guard_pages = false) {
  size_t guard_area_size = add_guard_pages ? page_size : 0;
  size_t page_aligned_size = __builtin_align_up(size, page_size);

  size_t alignment_mask = cheri_representable_alignment_mask(page_aligned_size);
  size_t req_alignment = (~alignment_mask) + 1;
  if (req_alignment < page_size) {
    req_alignment = page_size;
  }
  size_t req_size = cheri_representable_length(page_aligned_size);
  /*
   * Given that the initially mapped region as well as required size and
   * required alignment value are guaranteed to be at least page-aligned,
   * it is always possible to find a region of the required size with the
   * required alignment within the initially mapped region of the size computed
   * below.
   *  - if the initially mapped region is already aligned as required, then the
   *    condition is satisfied: (region % req_alignment == 0)
   *  - else, the region starts at some point region + alignment_offset
   *    which will satisfy ((region + alignment_offset) % req_alignment == 0)
   *    condition,  where alignment_offset is also page-aligned and is less than
   *    req_alignment.
   *    alignment_offset less than req_alignment is always sufficient as
   *    for any suitable larger alignment_offset there can be found
   *      alignment_offset2 = alignment_offset - N * req_alignment >= 0
   *    for which
   *      ((region + alignment_offset) % req_alignment) will be equivalent to
   *      ((region + alignment_offset2 + N * req_alignment) % req_alignment
   *      equal to ((region + alignment_offset2) % req_alignment)
   *    i.e. in that case it will be possible to find
   *      alignment_offset2 < req_alignment which also satisfies the condition.
   */
  size_t initial_mmap_size = req_size + req_alignment - page_size;

  initial_mmap_size += 2 * guard_area_size;

  // Reserve enough space to guarantee we can find suitably aligned region of
  // the required size within it.
  ptraddr_t addr = early_helper_mmap(0, initial_mmap_size, PROT_NONE, 0);

  // Find the suitably aligned region.
  ptraddr_t addr_aligned = __builtin_align_up(addr + guard_area_size, req_alignment);

  size_t trim_size_before = addr_aligned - addr;
  size_t trim_size_after = initial_mmap_size - trim_size_before - req_size;

  // Unmap the part of the earlier mapped area until addr_aligned and after
  // the end of the aligned region.
  early_helper_munmap(addr, trim_size_before - guard_area_size);
  early_helper_munmap(addr_aligned + req_size + guard_area_size, trim_size_after - guard_area_size);

  // Update permissions and flags of the mapped region as initially requested
  early_helper_mmap(addr_aligned, req_size, PROT_READ | PROT_WRITE, additional_flags | MAP_FIXED);

  uintptr_t ptr = global_data_cap();
  ptr = cheri_address_set(ptr, addr_aligned);
  ptr = cheri_bounds_set_exact(ptr, req_size);
  ptr = cheri_perms_and(ptr, kReadCapPerms | kWriteCapPerms | kRootCapPerms);

  return ptr;
}

// Allocates new stack region.
// Returns:
//   The new stack pointer value.
static uintcap_t allocate_stack(size_t page_size) {
  uintcap_t new_stack_pointer;
  size_t stack_size = early_helper_getrlimit_max_stack_size();
  if (stack_size > LIBSHIM_MAX_STACK_SIZE) {
    stack_size = LIBSHIM_MAX_STACK_SIZE;
  }
  new_stack_pointer = early_helper_mmap_region(stack_size, MAP_GROWSDOWN, page_size, true);
  new_stack_pointer = cheri_offset_set(new_stack_pointer, cheri_length_get(new_stack_pointer));
  return cheri_perms_and(new_stack_pointer, kReadCapPerms | kWriteCapPerms | kRootCapPerms);
}

static PhdrScanInfo parse_phdrs(Elf64_Phdr *phdr, uint64_t phnum, size_t alignment) {
  PhdrScanInfo result;
  result.rw_parsed = false;
  result.rx_parsed = false;
  result.max_rw = result.max_rx = 0;
  result.min_rw = result.min_rx = SIZE_MAX;
  ptraddr_t load_bias = 0;
  for (int i = 0; i < phnum; i++) {
    if (phdr[i].p_type == PT_PHDR) {
      load_bias = cheri_address_get(phdr) - phdr[i].p_vaddr;
    } else if (phdr[i].p_type == PT_LOAD) {
      ptraddr_t upper = load_bias + phdr[i].p_vaddr + phdr[i].p_memsz;
      ptraddr_t lower = load_bias + phdr[i].p_vaddr;
      upper = __builtin_align_up(upper, alignment);
      lower = __builtin_align_down(lower, alignment);
      // RX segment (all PT_LOAD)
      result.rx_parsed = true;
      result.min_rx = (result.min_rx > lower) ? lower : result.min_rx;
      result.max_rx = (result.max_rx < upper) ? upper : result.max_rx;
      // RW segment (all PT_LOAD with PF_W set)
      if ((phdr[i].p_flags & PF_W) == PF_W) {
        result.rw_parsed = true;
        result.min_rw = (result.min_rw > lower) ? lower : result.min_rw;
        result.max_rw = (result.max_rw < upper) ? upper : result.max_rw;
      }
    }
  }
  return result;
}

static uintcap_t derive_cheri_entry(int perms, ptraddr_t min, ptraddr_t max) {
  const uintcap_t gd_cap = global_data_cap();
  uintcap_t at_cheri_cap = cheri_perms_and(gd_cap, perms);
  at_cheri_cap = cheri_address_set(at_cheri_cap, min);
  at_cheri_cap = cheri_bounds_set(at_cheri_cap, max - min);
  return at_cheri_cap;
}

// Helper for locating input argc, argv, envp, auxv from the kernel
static void find_arguments(const ptraddr_t *old_sp_cap, int &argc, PtrRange<ptraddr_t> &input_argv,
                           PtrRange<ptraddr_t> &input_envp,
                           PtrRange<Elf64_auxv_kernel_t> &input_auxv) {
  // Get argc
  argc = *reinterpret_cast<const int *>(old_sp_cap);

  // Find argv.
  // There are 'argc' + 1 count of them including terminating NULL.
  const size_t input_argv_count = argc + 1;

  input_argv.start = reinterpret_cast<const ptraddr_t *>(old_sp_cap + 1);
  input_argv.end = input_argv.start + input_argv_count;

  // Find envp.
  input_envp.start = input_argv.end;
  const ptraddr_t *envp_iter = input_envp.start;
  while (*envp_iter) {
    ++envp_iter;
  }
  // Count terminating NULL too
  ++envp_iter;
  input_envp.end = envp_iter;

  // Find auxv.
  input_auxv.start = reinterpret_cast<const Elf64_auxv_kernel_t *>(input_envp.end);
  const Elf64_auxv_kernel_t *auxv_iter = input_auxv.start;
  while (auxv_iter->a_type != AT_NULL) {
    auxv_iter++;
  }
  // Count AT_NULL too
  ++auxv_iter;
  input_auxv.end = auxv_iter;
}

// Helper for figuring out the system page size.
// Unless there is AT_PAGESZ, the assumed page size is 4K.
// Returns:
//   the page size
static size_t find_page_size(PtrRange<Elf64_auxv_kernel_t> input_auxv) {
  // First, look for AT_PAGESZ.
  for (const Elf64_auxv_kernel_t *input_auxv_iter = input_auxv.start;
       input_auxv_iter->a_type != AT_NULL; ++input_auxv_iter) {
    if (input_auxv_iter->a_type == AT_PAGESZ) {
      return input_auxv_iter->a_val;
    }
  }

  // If not found in auxv, assume 4K.
  return 4096;
}

// Helper to prepare argv or envp arrays, updating the layout.
// Returns:
//   pointer to the new location of the array
static const uintcap_t *prepare_argv_or_envp(const PtrRange<ptraddr_t> input, size_t page_size) {
  const uintcap_t gd_cap = global_data_cap();
  const size_t input_count = input.end - input.start;
  const size_t output_area_size = input_count * sizeof(uintcap_t);

  uintptr_t output_area = early_helper_mmap_region(output_area_size, 0, page_size);

  uintcap_t *output_iter = reinterpret_cast<uintcap_t *>(output_area);

  output_iter = cheri_bounds_set(output_iter, output_area_size);
  const uintcap_t *const output_start = output_iter;
  for (const ptraddr_t *input_iter = input.start; *input_iter != 0; ++input_iter) {
    *output_iter++ =
        restrict_string_use(cap_address_set(gd_cap, *input_iter),
                            __CHERI_CAP_PERMISSION_GLOBAL__ | __CHERI_CAP_PERMISSION_PERMIT_LOAD__ |
                                __CHERI_CAP_PERMISSION_PERMIT_STORE__);
  }
  // The last item is NULL.
  *output_iter++ = static_cast<uintcap_t>(0);

  return output_start;
}

// Helper to prepare auxv array, updating the layout.
// Returns:
//   pointer to the new location of the array
static const Elf64_auxv_t *prepare_auxv(const PtrRange<Elf64_auxv_kernel_t> input,
                                        uintcap_t &out_new_stack_pointer,
                                        uintcap_t &out_entry_point_rx_cap, size_t at_argc,
                                        size_t at_envc, const uintcap_t *at_argv,
                                        const uintcap_t *at_envp, size_t page_size) {
  const uintcap_t gd_cap = global_data_cap();
  const size_t input_count = input.end - input.start;
  const size_t output_area_size = (input_count + AUXV_EXTRA_COUNT) * sizeof(Elf64_auxv_t);
  uintptr_t output_area = early_helper_mmap_region(output_area_size, 0, page_size);

  Elf64_auxv_t *output_iter = reinterpret_cast<Elf64_auxv_t *>(output_area);

  Elf64_Half exec_phnum;
  Elf64_Ehdr *interp_base = nullptr;
  ptraddr_t entry_addr = 0;
  ptraddr_t exec_phdr_addr = 0;

  output_iter = cheri_bounds_set(output_iter, output_area_size);
  const Elf64_auxv_t *const output_start = output_iter;
  for (const Elf64_auxv_kernel_t *input_iter = input.start; input_iter->a_type != AT_NULL;
       ++input_iter) {
    if (input_iter->a_type == AT_ENTRY) {
      entry_addr = input_iter->a_val;
      continue;  // skip ++output_iter
    } else if (input_iter->a_type == AT_PHDR) {
      exec_phdr_addr = input_iter->a_val;
      continue;  // skip ++output_iter
    } else if (input_iter->a_type == AT_EXECFN || input_iter->a_type == AT_PLATFORM) {
      uintcap_t cheri_string_cap = cheri_address_set(gd_cap, input_iter->a_val);
      cheri_string_cap = restrict_string_use(
          cheri_string_cap, __CHERI_CAP_PERMISSION_GLOBAL__ | __CHERI_CAP_PERMISSION_PERMIT_LOAD__);
      write_auxval(output_iter, input_iter->a_type, cheri_string_cap);
    } else if (input_iter->a_type == AT_RANDOM) {
      uintcap_t cheri_random_cap = cheri_address_set(gd_cap, input_iter->a_val);
      cheri_random_cap = cheri_bounds_set_exact(cheri_random_cap, LIBSHIM_AT_RANDOM_SIZE);
      cheri_random_cap = cheri_perms_and(
          cheri_random_cap, __CHERI_CAP_PERMISSION_GLOBAL__ | __CHERI_CAP_PERMISSION_PERMIT_LOAD__);
      write_auxval(output_iter, input_iter->a_type, cheri_random_cap);
    } else if (input_iter->a_type == AT_SYSINFO_EHDR) {
      write_auxval(output_iter, input_iter->a_type, static_cast<uintcap_t>(0));
    } else {
      if (input_iter->a_type == AT_PHNUM) {
        exec_phnum = input_iter->a_val;
      } else if (input_iter->a_type == AT_BASE) {
        if (input_iter->a_val) {
          interp_base = cap_address_set<Elf64_Ehdr *>(gd_cap, input_iter->a_val);
        }
      }
      write_auxval(output_iter, input_iter->a_type, input_iter->a_val);
    }
    ++output_iter;
  }

  uintcap_t cheri_exec_rx_cap, cheri_exec_rw_cap, cheri_interp_rx_cap, cheri_interp_rw_cap;

  if (!exec_phdr_addr || !exec_phnum) {
    early_helper_abort();
  }
  uintcap_t cheri_phdr_cap = cheri_address_set(gd_cap, exec_phdr_addr);
  cheri_phdr_cap = cheri_bounds_set(cheri_phdr_cap, exec_phnum * sizeof(Elf64_Phdr));
  cheri_phdr_cap = cheri_perms_and(
      cheri_phdr_cap, __CHERI_CAP_PERMISSION_GLOBAL__ | __CHERI_CAP_PERMISSION_PERMIT_LOAD__);
  write_auxval(output_iter++, AT_PHDR, cheri_phdr_cap);

  // Establish the ranges of the initial reservations for the executable

  PhdrScanInfo exec =
      parse_phdrs(reinterpret_cast<Elf64_Phdr *>(cheri_phdr_cap), exec_phnum, page_size);
  if (!exec.rx_parsed) {
    early_helper_abort();
  }

  cheri_exec_rx_cap =
      derive_cheri_entry(kReadCapPerms | kExeCapPerms | kRootCapPerms, exec.min_rx, exec.max_rx);

  if (exec.rw_parsed) {
    cheri_exec_rw_cap = derive_cheri_entry(kReadCapPerms | kWriteCapPerms | kRootCapPerms,
                                           exec.min_rw, exec.max_rw);
  } else {
    cheri_exec_rw_cap = 0;
  }

  if (interp_base != nullptr) {  // the executable has an interpreter
    Elf64_Phdr *interp_phdr = reinterpret_cast<Elf64_Phdr *>(
        reinterpret_cast<unsigned char *>(interp_base) + interp_base->e_phoff);
    Elf64_Half interp_phnum = interp_base->e_phnum;
    PhdrScanInfo interp = parse_phdrs(interp_phdr, interp_phnum, page_size);
    if (!interp.rx_parsed) {
      early_helper_abort();
    }

    cheri_interp_rx_cap = derive_cheri_entry(kReadCapPerms | kExeCapPerms | kRootCapPerms,
                                             interp.min_rx, interp.max_rx);

    if (interp.rw_parsed) {
      cheri_interp_rw_cap = derive_cheri_entry(kReadCapPerms | kWriteCapPerms | kRootCapPerms,
                                               interp.min_rw, interp.max_rw);
    } else {
      cheri_interp_rw_cap = 0;
    }
  } else {
    cheri_interp_rx_cap = cheri_interp_rw_cap = 0;
  }

  uintcap_t cheri_stack_cap = allocate_stack(page_size);

  uintcap_t cheri_seal_cap = cheri_perms_and(gd_cap, kSealCapPerms);
  cheri_seal_cap =
      cheri_bounds_set_exact(cheri_seal_cap, static_cast<uint64_t>(1) << OTYPE_NUM_BITS);

  uintcap_t cheri_cid_cap = cheri_perms_and(gd_cap, kCidCapPerms);

  write_auxval(output_iter++, AT_CHERI_EXEC_RW_CAP, cheri_exec_rw_cap);
  write_auxval(output_iter++, AT_CHERI_EXEC_RX_CAP, cheri_exec_rx_cap);
  write_auxval(output_iter++, AT_CHERI_INTERP_RW_CAP, cheri_interp_rw_cap);
  write_auxval(output_iter++, AT_CHERI_INTERP_RX_CAP, cheri_interp_rx_cap);
  write_auxval(output_iter++, AT_CHERI_STACK_CAP, cheri_stack_cap);
  write_auxval(output_iter++, AT_CHERI_SEAL_CAP, cheri_seal_cap);
  write_auxval(output_iter++, AT_CHERI_CID_CAP, cheri_cid_cap);

  if (cheri_interp_rx_cap != 0) {
    out_entry_point_rx_cap = cheri_interp_rx_cap;
  } else {
    out_entry_point_rx_cap = cheri_exec_rx_cap;
  }

  out_new_stack_pointer = cheri_perms_and(cheri_stack_cap, kStackCapPerms);

  // entry_addr is earlier obtained from the input array
  if (!entry_addr) {
    early_helper_abort();
  }
  uintcap_t cheri_entry_cap = cheri_address_set(cheri_exec_rx_cap, entry_addr);
  cheri_entry_cap = cheri_perms_and(cheri_entry_cap,
                                    __CHERI_CAP_PERMISSION_GLOBAL__ | kReadCapPerms | kExeCapPerms);
#if !__SANITIZE_CHERISEED__
  cheri_entry_cap = cheri_sentry_create(cheri_entry_cap);
#endif /* !__SANITIZE_CHERISEED__ */
  write_auxval(output_iter++, AT_ENTRY, cheri_entry_cap);

  write_auxval(output_iter++, AT_ARGC, at_argc);
  write_auxval(output_iter++, AT_ARGV, reinterpret_cast<uintcap_t>(at_argv));
  write_auxval(output_iter++, AT_ENVC, at_envc);
  write_auxval(output_iter++, AT_ENVP, reinterpret_cast<uintcap_t>(at_envp));

  // The last auxv is AT_NULL.
  write_auxval(output_iter++, AT_NULL, static_cast<uintcap_t>(0));

  return output_start;
}

// Move the arguments (argc, argv, envp, auxv), updating
// the layout to support 64-bit kernels but 128-bit capabilities.
// Arguments:
//  'old_sp_cap': The old stack pointer - pointing to 'argc'.
//  'out_new_stack_pointer': output - capability for the new stack.
//  'out_entry_point_rx_cap': output - capability for RX region containing
//                            the entry point of the process.
static void prepare_arguments(ptraddr_t *old_sp_cap, uintcap_t &out_new_stack_pointer,
                              uintcap_t &out_entry_point_rx_cap) {
  int argc;
  PtrRange<ptraddr_t> input_argv, input_envp;
  PtrRange<Elf64_auxv_kernel_t> input_auxv;

  find_arguments(old_sp_cap, argc, input_argv, input_envp, input_auxv);

  const size_t page_size = find_page_size(input_auxv);

  // Pull argv entries.
  const uintcap_t *output_argv_start = prepare_argv_or_envp(input_argv, page_size);

  // Pull envp entries.
  const uintcap_t *output_envp_start = prepare_argv_or_envp(input_envp, page_size);

  // Pull auxv entries.
  const Elf64_auxv_t *output_auxv_start = prepare_auxv(
      input_auxv, out_new_stack_pointer, out_entry_point_rx_cap, input_argv.end - input_argv.start,
      input_envp.end - input_envp.start, output_argv_start, output_envp_start, page_size);

  // Allocate space on the stack for initial data expected from libshim.
  out_new_stack_pointer -= kOutputsRegistrySize;
  uintcap_t *output_array =
      reinterpret_cast<uintcap_t *>(cheri_bounds_set(out_new_stack_pointer, kOutputsRegistrySize));
  // Populate the allocated area.
  output_array[0] = argc;
  output_array[1] = reinterpret_cast<uintcap_t>(output_argv_start);
  output_array[2] = reinterpret_cast<uintcap_t>(output_envp_start);
  output_array[3] = reinterpret_cast<uintcap_t>(output_auxv_start);
#if __SANITIZE_CHERISEED__
  out_new_stack_pointer -= sizeof(uintcap_t);
  *reinterpret_cast<uintcap_t **>(out_new_stack_pointer) = output_array;
#endif
}

#if __SANITIZE_CHERISEED__
// Prepare the environment (argc, argv, envp, auxv) for CHERIseed,
// updating the layout to support 64-bit kernels but 128-bit capabilities.
// Arguments:
//  'old_sp_addr': The old 64-bit stack address - pointing to 'argc'.
//  'new_sp_addr_loc': The location of the new 64-bit stack address.
extern "C" void __shim_prepare_environment(ptraddr_t old_sp_addr, ptraddr_t new_sp_addr_loc) {
  // Derive new capabilities using global data capability.
  const uintcap_t gd_cap = global_data_cap();
  ptraddr_t *old_sp_cap = cap_address_set<ptraddr_t *>(gd_cap, old_sp_addr);
  ptraddr_t *new_sp_addr_loc_cap = cap_address_set<ptraddr_t *>(gd_cap, new_sp_addr_loc);
  uintcap_t entry_point_rx_cap = 0;
  uintcap_t new_stack_pointer;

  // Also populate new_stack_pointer and output_array.
  prepare_arguments(old_sp_cap, new_stack_pointer, entry_point_rx_cap);

  *new_sp_addr_loc_cap = cheri_address_get(new_stack_pointer);
}
#else   // __SANITIZE_CHERISEED__
// Prepare the environment (argc, argv, envp, auxv), updating
// the layout to support 64-bit kernels but 128-bit capabilities.
// Arguments:
//  'old_sp_cap': The old stack pointer - pointing to 'argc'.
//  'ret_addr': The return address from the __shim_marshal_program_arguments.
//  'new_stack_pointer': A capability pointing to the new 64-bit stack pointer.
// Returns:
//  New return capability (used to setup PCC bounds/permissions)
extern "C" uintcap_t __shim_prepare_environment(ptraddr_t *old_sp_cap, ptraddr_t ret_addr,
                                                uintcap_t *new_stack_pointer) {
  uintcap_t entry_point_rx_cap = 0;

  // Also populate new_stack_pointer and output_array.
  prepare_arguments(old_sp_cap, *new_stack_pointer, entry_point_rx_cap);

  uintcap_t pcc = cheri_address_set(entry_point_rx_cap, ret_addr);

  pcc = cheri_perms_and(pcc, __CHERI_CAP_PERMISSION_GLOBAL__ | kReadCapPerms | kExeCapPerms);

  pcc = cheri_sentry_create(pcc);

  return pcc;
}
#endif  // __SANITIZE_CHERISEED__

// Marshals the arguments by moving them to separate areas with
// updated layout. The data in the original stack area remains
// unchanged.
// Note that this function returns the new 'sp' in the register in which
// the old 'sp' is passed.
// AT_CHERI members are added to auxv to provide root RX, RW and sealing
// capabilities.
LIBSHIM_NAKED
extern "C" void __shim_marshal_program_arguments(ptraddr_t sp) {
#if defined(__aarch64__) && __SANITIZE_CHERISEED__
  __asm__(
      // Set global data capability while preserving the original SP and
      // link-register
      "stp   x0, lr, [sp, -0x10]!\n\t"
      "adrp  x0, __cheriseed_shadowed_global___shim_data_cap\n\t"
      "add   x0, x0, :lo12:__cheriseed_shadowed_global___shim_data_cap\n\t"
      "bl    __cheriseed_ddc_get\n\t"
      "ldp   x0, lr, [sp], 0x10\n\t"  // x0 was clobbered in the call above
      // Allocate space for new stack pointer.
      "sub   sp, sp, 0x10\n\t"
      "mov   x1, sp\n\t"
      // Prepare environment, including allocation of the new stack, while
      // preserving the link-register.
      "str   lr, [sp, -0x10]!\n\t"
      "bl    __shim_prepare_environment\n\t"
      "ldr   lr, [sp], 0x10\n\t"
      // Set new SP.
      "ldr   x0, [sp]\n\t"
      "mov   sp, x0\n\t"
      "ret\n\t");
#elif defined(__aarch64__) && defined(__ARM_FEATURE_C64)
  __asm__(
      // Set __shim_data_cap
      "adrp  c1, __shim_data_cap\n\t"
      "add   c1, c1, :lo12:__shim_data_cap\n\t"
      "cvtdz c1, x1\n\t"
      "mrs   c2, ddc\n\t"
      "str   c2, [c1]\n\t"
      // CSP is not valid, temporarily derive from DDC.
      "cvtd  c0, x0\n\t"
      "mov   csp, c0\n\t"
      // Prepare environment.
      "mov   x1, lr\n\t"
      // Allocate space for new stack pointer.
      "sub   csp, csp, 0x10\n\t"
      "mov   c2, csp\n\t"
      "bl    __shim_prepare_environment\n\t"
      // Set new CLR (consequently, setup PCC bounds/permissions)
      "mov   clr, c0\n\t"
      // Set new CSP.
      "ldr   c0, [csp]\n\t"
      "mov   csp, c0\n\t"
#if LIBSHIM_ZERO_DDC
      "msr   ddc, czr\n\t"
#endif
      "ret   clr\n\t");
#elif defined(__x86_64__) && __SANITIZE_CHERISEED__
  __asm__(
      // Set global data capability preserving the original SP
      "push  %rdi\n\t"
      "lea   __cheriseed_shadowed_global___shim_data_cap(%rip), %rdi\n\t"
      "call  __cheriseed_ddc_get\n\t"
      "pop   %rdi\n\t"
      // Prepare environment including allocation of the new stack
      "sub   $0x10, %rsp\n\t"
      "mov   %rsp, %rsi\n\t"
      "push  %rdi\n\t"
      "call  __shim_prepare_environment\n\t"
      "pop   %rdi\n\t"
      // Load new SP.
      "mov   (%rsp), %rdi\n\t"
      "add   $0x10, %rsp\n\t"
      // Save the return address from the original stack.
      "pop   %rdx\n\t"
      // Set new SP.
      "mov   %rdi, %rsp\n\t"
      // Set the return address on the new stack.
      "push  %rdx\n\t"
      "ret\n\t");
#else
#error "Support for this architecture is missing"
#endif
}

// Temporary support during transition period.
extern "C" void do_raw_args_marshalling(ptraddr_t)
    __attribute__((alias("__shim_marshal_program_arguments")));

}  // namespace shim
}  // namespace morello

#endif  // defined(__CHERI_PURE_CAPABILITY__)
