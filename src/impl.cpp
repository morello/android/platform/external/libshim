/*
 * Copyright (c) 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"
#include "shim.h"
#include "stl/panic.h"

namespace morello {
namespace shim {

using morello::shim::check::Check;
using morello::shim::check::CheckType;
using morello::shim::stl::Arg;
using morello::shim::stl::BufferedFormattedString;

static void PrintExpectation(Check& check, BufferedFormattedString<>& message) noexcept {
  // Return if there is no argument information.
  if (check.arg1().idx == Arg::kInvalidIdx) {
    return;
  }

  // Print expectation based on the check type. Argument 1 is always involved.
  message << "Expected that " << check.arg1();
  switch (check.type()) {
    case CheckType::STRING:
      message << " is a pointer to well-formed string.\n";
      break;
    case CheckType::CONST_BUFFER:
      message << " is a pointer to a readable memory range of size " << check.arg2().value
              << " bytes.\n";
      // Argument specifying size is printed below.
      break;
    case CheckType::BUFFER:
      message << " is a pointer to a readable and writable memory range of size "
              << check.arg2().value << " bytes.\n";
      // Argument specifying size is printed below.
      break;
    case CheckType::OWNING_CAPABILITY:
      message << " is an owning capability of a range with size " << check.arg2().value
              << " bytes.\n";
      // Argument specifying size is printed below.
      break;
    case CheckType::PROT_IS_IN_MAX:
      message << " supplies valid protection flags.\n";
      break;
    case CheckType::MAY_SET_PROT:
      message << " is a capability with permissions requested by " << check.arg2() << ".\n";
      break;
    case CheckType::PERMISSION:
      message << " has all the required permissions.\n";
      break;
    case check::CheckType::NULL_DERIVED:
      message << " is a null-derived capability.\n";
      break;
    case CheckType::HAS_NO_VALID_CAPABILITIES:
      message << " is a pointer to a memory range which holds only invalid (tag cleared) "
                 "capabilities.\n";
      break;
    case CheckType::NULLPTR:
      message << " is the null pointer (capability).\n";
      break;
  }

  // Print argument specifying size.
  switch (check.type()) {
    case CheckType::CONST_BUFFER:
    case CheckType::BUFFER:
    case CheckType::OWNING_CAPABILITY:
      if (check.arg2().idx != Arg::kInvalidIdx) {
        message << "The size of the range was specified in " << check.arg2()
                << " either directly or indirectly.\n";
      }
      break;
    case CheckType::HAS_NO_VALID_CAPABILITIES:
      message << "The size of the checked range was " << static_cast<size_t>(check.arg2().value)
              << " bytes.\n";
      break;
    default:
      break;
  }
}

void PrintFailedSystemCall(Check& check, unsigned num_args, LIBSHIM_COMMON_ARGS) noexcept {
  BufferedFormattedString<> message;
  message << check.name() << ": " << check.message() << ".\n";
  PrintExpectation(check, message);
  message << "Arguments:\n";
  if (0 < num_args) {
    message << "  1: " << arg1 << "\n";
  }
  if (1 < num_args) {
    message << "  2: " << arg2 << "\n";
  }
  if (2 < num_args) {
    message << "  3: " << arg3 << "\n";
  }
  if (3 < num_args) {
    message << "  4: " << arg4 << "\n";
  }
  if (4 < num_args) {
    message << "  5: " << arg5 << "\n";
  }
  if (5 < num_args) {
    message << "  6: " << arg6 << "\n";
  }
  message.write_fd(STDERR_FILENO);
}

}  // namespace shim
}  // namespace morello
