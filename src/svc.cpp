/*
 * Copyright (c) 2020-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "shim.h"

namespace morello {
namespace shim {

// The generic svc() implementation.

#if __SANITIZE_CHERISEED__
using ImplArgTy = ptraddr_t;
#else
using ImplArgTy = uintptr_t;
#endif

#if __SANITIZE_CHERISEED__ && defined(__CHERI_PURE_CAPABILITY__)
// It is a trait of CHERIseed Sanitizer that an 'int to ptr' operation creates
// a new capability on stack with the integer as value. The 64-bit kernel ABI
// is such that we need to pass the addresses from capabilities. Therefore use
// 'cheri_address_get' for all of the arguments.
static inline ImplArgTy ToImplArg(uintptr_shim_t arg) {
  return static_cast<ImplArgTy>(cheri_address_get(static_cast<void*>(arg)));
}
#else
static inline ImplArgTy ToImplArg(uintptr_shim_t arg) {
  return reinterpret_cast<ImplArgTy>(static_cast<void*>(arg));
}
#endif  // __SANITIZE_CHERISEED__ && defined(__CHERI_PURE_CAPABILITY__)

LIBSHIM_NAKED
static ImplArgTy svc_impl(ImplArgTy arg1, ImplArgTy arg2, ImplArgTy arg3, ImplArgTy arg4,
                          ImplArgTy arg5, ImplArgTy arg6, long nr) {
#if defined(__aarch64__)
  // clang-format off
  asm volatile(
      // Reorder arguments
#if defined(MORELLO_16REGS_ONLY)
      // The 7th parameter, NR, is on the stack.
      "ldrh   w8, [csp, 0x0]\n\t"
#else
      "mov    w8, w6\n\t"
#endif
      // Do the system call
      "svc    0\n\t"
      "ret\n\t"
  );
  // clang-format on
#elif defined(__x86_64__)
  // clang-format off
  asm volatile(
      // [x86 - 1] Reorder arguments
      //
      // Arguments on entry:
      //    %rdi:    arg1
      //    %rsi:    arg2
      //    %rdx:    arg3
      //    %rcx:    arg4
      //    %r8:     arg5
      //    %r9:     arg6
      //    8(%rsp): NR
      //
      // Linux kernel system call ABI:
      //    %rdi:    arg1 - ok
      //    %rsi:    arg2 - ok
      //    %rdx:    arg3 - ok
      //    %r10:    arg4
      "mov    %rcx, %r10\n\t"
      //    %r8:     arg5 - ok
      //    %r9:     arg6 - ok
      //    %rax:    NR
      "mov    8(%rsp), %rax\n\t"
      // Do the system call
      "syscall\n\t"
      "ret\n\t"
  );
  // clang-format on
#else
#error "Architecture is not yet supported."
#endif
}

#if LIBSHIM_CANCELLATION_POINTS

// If implemented and returns non-zero the current system call is halted
// within the cancellable region. This is for testing only. Without this
// feature it would be very hard to write reproducible tests for cancellations.
LIBSHIM_WEAK extern "C" int __shim_pause_in_cp(void);

// Called when a system call is to be cancelled.
extern "C" ImplArgTy __shim_cancel_syscall(void);

// Constants passed from svc() to svc_impl().
static constexpr long kPauseInCpNr = -1;
static constexpr long kNotImplementedNr = -2;

LIBSHIM_NAKED
static ImplArgTy svc_impl_cp(ImplArgTy arg1, ImplArgTy arg2, ImplArgTy arg3, ImplArgTy arg4,
                             ImplArgTy arg5, ImplArgTy arg6, long nr, ImplArgTy cg) {
#if defined(__aarch64__)
  // clang-format off
  asm volatile(
      // Support for cancellation points: start of range.
      ".global __shim_cp_begin\n\t"
      ".size __shim_cp_begin, 1\n\t"
      "__shim_cp_begin:\n\t"
      "cbz    x7, .Lnot_cancelled\n\t"
#if !defined(__CHERI_PURE_CAPABILITY__) || __SANITIZE_CHERISEED__
      "ldr    w7, [x7]\n\t"
#else
      "ldr    w7, [c7]\n\t"
#endif
      "cbz    w7, .Lnot_cancelled\n\t"
      "b      __shim_cancel_syscall\n\t"
      ".Lnot_cancelled:\n\t"
      // Reorder arguments
      "mov    w8, w6\n\t"
      // Support for pausing between __shim_cp_begin and __shim_cp_end.
      // This should be used for testing only.
      "cmp    w8, %0\n\t"
      "b.eq   .\n\t"
      // Do the system call
      "svc    0\n\t"
      // Support for cancellation points: end of range.
      ".global __shim_cp_end\n\t"
      ".size __shim_cp_end, 1\n\t"
      "__shim_cp_end:\n\t"
      "ret\n\t"
      :
      : "i"(kPauseInCpNr)
      :
  );
  // clang-format on
#elif defined(__x86_64__)
  // clang-format off
  asm volatile(
      // Support for cancellation points: start of range.
      ".global __shim_cp_begin\n\t"
      ".size __shim_cp_begin, 1\n\t"
      "__shim_cp_begin:\n\t"
      // cg is at 16(%%rsp)
      "movq   16(%%rsp), %%rax\n\t"
      "testq  %%rax, %%rax\n\t"
      "je     .Lnot_cancelled\n\t"
      "cmpl   $0, 16(%%rax)\n\t"
      "je     .Lnot_cancelled\n\t"
      "jmp    __shim_cancel_syscall\n\t"
      ".Lnot_cancelled:\n\t"
      // Reorder arguments
      // See note [x86 - 1] above.
      "mov    %%rcx, %%r10\n\t"
      "mov    8(%%rsp), %%rax\n\t"
      // Support for pausing between __shim_cp_begin and __shim_cp_end.
      // This should be used for testing only.
      "cmpq   %0, %%rax\n\t"
      "je     .\n\t"
      // Do the system call
      "syscall\n\t"
      // Support for cancellation points: end of range.
      ".global __shim_cp_end\n\t"
      ".size __shim_cp_end, 1\n\t"
      "__shim_cp_end:\n\t"
      "ret\n\t"
      :
      : "i"(kPauseInCpNr)
      :
  );
  // clang-format on
#else
#error "Architecture is not yet supported."
#endif
}

#endif  // LIBSHIM_CANCELLATION_POINTS

LIBSHIM_FN(svc) {
#if LIBSHIM_CANCELLATION_POINTS
  if (cg == nullptr) {
#endif  // LIBSHIM_CANCELLATION_POINTS
    return svc_impl(ToImplArg(arg1), ToImplArg(arg2), ToImplArg(arg3), ToImplArg(arg4),
                    ToImplArg(arg5), ToImplArg(arg6), nr);
#if LIBSHIM_CANCELLATION_POINTS
  }

  long effective_nr = nr;
  if (__builtin_expect(effective_nr == kPauseInCpNr, 0)) {
    // Prevent infinite looping initiated from syscall() level.
    // This will return a "function not implemented".
    effective_nr = kNotImplementedNr;
  } else if (__builtin_expect(&__shim_pause_in_cp != 0, 0) && (__shim_pause_in_cp() != 0)) {
    effective_nr = kPauseInCpNr;
  }
  return svc_impl_cp(ToImplArg(arg1), ToImplArg(arg2), ToImplArg(arg3), ToImplArg(arg4),
                     ToImplArg(arg5), ToImplArg(arg6), effective_nr, ToImplArg(cg));
#endif  // LIBSHIM_CANCELLATION_POINTS
}

}  // namespace shim
}  // namespace morello
