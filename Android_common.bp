// Generator for libshim
// =========================================================

filegroup {
    name: "libshim-generator-sources",
    srcs: [
        "tools/*.py",
        "tools/templates/*",
        "*.json",
    ]
}

filegroup {
    name: "libshim-generator-entry",
    srcs: ["tools/main.py"]
}

genrule_defaults {
    name: "libshim-generator-files",
    tool_files: [
        ":libshim-generator-entry",
        ":libshim-generator-sources",
    ],
}

genrule_defaults {
    name: "libshim-headers-generator",
    defaults: ["libshim-generator-files"],
    out: ["shim_gen.h"],
}

genrule_defaults {
    name: "libshim-shims-generator",
    defaults: ["libshim-generator-files"],
    out: [
        "shims.cpp",
        "syscall.cpp",
    ],
}

genrule_defaults {
    name: "libshim-wrappers-generator",
    defaults: ["libshim-generator-files"],
    out: ["wrappers.cpp"],
}

genrule_defaults {
    name: "libshim-stubs-generator",
    defaults: ["libshim-generator-files"],
    out: ["shim_stubs.cpp"],
}

// Common flags
// =========================================================

libshim_common_flags = [
    "-Wall",
    "-Wextra",
    "-Wunused",
    "-Wno-char-subscripts",
    "-Wno-deprecated-declarations",
    "-Wimplicit-fallthrough",
    "-Wno-unused-parameter",
    "-Wno-sign-compare",
    "-Werror=pointer-to-int-cast",
    "-Werror=int-to-pointer-cast",
    "-Werror=type-limits",
    "-Werror",
    "-Dlint",
    "-DLIBSHIM_LIBRARY_BUILD",
    "-DLIBSHIM_NEW_PURECAP_VARARGS_PCS",
    "-DLIBSHIM_TRANSITION_SWITCH_PROT_MAX_IMPLEMENTED=1",
    "-DLIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED=1",
    "-DLIBSHIM_ZERO_DDC=1",
]

libshim_common_cpp_flags = [
    "-fno-exceptions",
    "-std=c++11",
]

// Defaults for libshim
// ========================================================

cc_defaults {
    name: "libshim_common_defaults",
    cflags: libshim_common_flags,
    asflags: libshim_common_flags,
    cppflags: libshim_common_cpp_flags,
    conlyflags: ["-std=gnu99"],
    local_include_dirs: ["include"],
    host_supported: false,
    enabled: false,
    arch: {
        arm64: {
            enabled: true,
        },
        morello: {
            enabled: true,
        },
    },
}

cc_defaults {
    name: "libshim_lib_defaults",
    cppflags: ["-Wold-style-cast"],
    system_shared_libs: [],
    vendor_available: true,
    recovery_available: true,
    stl: "none",
    nocrt: true,
    no_libcrt: true,
}

cc_defaults {
    name: "libshim_defaults",
    defaults: ["libshim_common_defaults"],
    generated_headers: ["libshim-headers-arm64"],
    generated_sources: [
        "libshim-shims-arm64",
    ],
}

cc_defaults {
    name: "libshim_test_defaults",
    defaults: ["libshim_defaults"],
    compile_multilib: "both",
    static_libs: ["libshim"],
}

// libshim-unit-tests
// ========================================================

cc_test {
    name: "libshim-unit-tests-static",
    defaults: ["libshim_test_defaults"],
    static_executable: true,
    srcs: [
        "tests/**/*.cpp",
    ],
    cflags: [
        "-DLIBSHIM_SYSCALL_STRICT=1",
        "-DLIBSHIM_ERROR_PANICS=1",
    ],
    local_include_dirs: ["tests"],
}

// Examples for libshim API usage
// ========================================================

cc_test {
    name: "libshim-example-transform",
    defaults: ["libshim_test_defaults"],
    srcs: ["examples/transform.cpp"],
}
