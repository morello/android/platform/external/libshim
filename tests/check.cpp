/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "check.h"

#include <gtest/gtest.h>

#include "stl/panic.h"

using namespace morello;
using namespace morello::shim::check;

using morello::shim::stl::Arg;

TEST(CheckTest, ConstString) {
  ASSERT_TRUE(Check("test").const_string(Arg{ nullptr }).passed());
  ASSERT_TRUE(Check("test").const_string(Arg{ "\0" }).passed());
  ASSERT_TRUE(Check("test").const_string(Arg{ "Hello, World!" }).passed());
}

TEST(CheckTest, ConstStringArray) {
  ASSERT_TRUE(Check("test").const_string_array(Arg{ nullptr }).passed());

  const char* array[3] = {
    "\0",
    "a",
    nullptr,
  };
  ASSERT_TRUE(Check("test").const_string_array(Arg{ array }).passed());
}

TEST(CheckTest, ConstBuffer) {
  const int foo[23] = { 0 };

  ASSERT_TRUE(Check("test").const_buffer(Arg{ nullptr }, Arg{ 0 }).passed());
  ASSERT_TRUE(Check("test").const_buffer(Arg{ &foo[0] }, Arg{ 0 }).passed());
  ASSERT_TRUE(Check("test").const_buffer(Arg{ &foo[0] }, Arg{ sizeof(foo) }).passed());
  ASSERT_TRUE(Check("test").const_buffer(Arg{ nullptr }, Arg{ 1 }).passed());
}

TEST(CheckTest, Buffer) {
  int foo[23];

  ASSERT_TRUE(Check("test").buffer(Arg{ nullptr }, Arg{ 0 }).passed());
  ASSERT_TRUE(Check("test").buffer(Arg{ &foo[0] }, Arg{ 0 }).passed());
  ASSERT_TRUE(Check("test").buffer(Arg{ &foo[0] }, Arg{ sizeof(foo) }).passed());
  ASSERT_TRUE(Check("test").buffer(Arg{ nullptr }, Arg{ 1 }).passed());
}

TEST(CheckTest, PointerToConst) {
  const int foo = 42;
  ASSERT_TRUE(Check("test").pointer_to_const<const int>(Arg{ &foo }).passed());
  ASSERT_TRUE(Check("test").pointer_to_const<int>(Arg{ &foo }).passed());
}

TEST(CheckTest, Pointer) {
  int foo = 42;
  ASSERT_TRUE(Check("test").pointer<int>(Arg{ &foo }).passed());
}

TEST(CheckTest, PointerToConstArray) {
  const int foo[42] = { 0 };
  ASSERT_TRUE(Check("test").pointer_to_const_array<const int>(Arg{ &foo[0] }, 42).passed());
  ASSERT_TRUE(Check("test").pointer_to_const_array<int>(Arg{ &foo[0] }, 42).passed());
}

TEST(CheckTest, PointerToArray) {
  int foo[42] = { 0 };
  ASSERT_TRUE(Check("test").pointer_to_array<int>(Arg{ &foo[0] }, Arg{ 42 }).passed());
}

#ifdef __CHERI_PURE_CAPABILITY__
static bool TestHasNoValidCapabilities(const char* ptr, size_t size, size_t offset) {
  return Check("test").has_no_valid_capabilities(Arg{ ptr + offset }, size - offset).passed();
}
#endif

TEST(CheckTest, HasNoValidCapabilities_AllUntagged) {
#ifndef __CHERI_PURE_CAPABILITY__
  GTEST_SKIP();
#else
  const char *range[] = { nullptr, nullptr };
  const char *rangePtr = reinterpret_cast<const char *>(range);
  ASSERT_TRUE(TestHasNoValidCapabilities(nullptr, 0, 0));
  ASSERT_TRUE(TestHasNoValidCapabilities(rangePtr, sizeof(range), 0));
  ASSERT_TRUE(TestHasNoValidCapabilities(rangePtr, sizeof(range), 1));
  ASSERT_TRUE(TestHasNoValidCapabilities(rangePtr, sizeof(range), sizeof(void *) - 1));
#endif
}

TEST(CheckTest, HasNoValidCapabilities_HasTagged) {
#ifndef __CHERI_PURE_CAPABILITY__
  GTEST_SKIP();
#else
  char x;
  const char *range[] = { &x, &x, nullptr };
  const char *rangePtr = reinterpret_cast<const char *>(range);
  // Clear all permissions.
  rangePtr = cheri_perms_and(rangePtr, 0);
  ASSERT_FALSE(TestHasNoValidCapabilities(rangePtr, sizeof(range), 0));
  ASSERT_FALSE(TestHasNoValidCapabilities(rangePtr, sizeof(range), 1));
  ASSERT_FALSE(TestHasNoValidCapabilities(rangePtr, sizeof(range), sizeof(void *) - 1));
  ASSERT_FALSE(TestHasNoValidCapabilities(rangePtr, sizeof(range), sizeof(void *)));
  ASSERT_TRUE(TestHasNoValidCapabilities(rangePtr, sizeof(range), sizeof(void *) + 1));
#endif
}
