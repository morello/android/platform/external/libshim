/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <inttypes.h>

#include <iostream>
#include <sstream>

#include "check.h"
#include "stl/panic.h"

using namespace morello::shim;

struct Narrow;

struct Wide {
  static inline void validate(check::Check& check, stl::Arg input, const Narrow& local,
                              bool) noexcept {
  }

  Wide& operator<<(const Narrow& narrow) noexcept;
  Narrow& operator>>(Narrow& narrow) noexcept;

  inline bool operator==(const Wide& other) const noexcept {
    return data == other.data;
  }

  uint64_t data;
};

struct Narrow {
  static inline void validate(check::Check& check, stl::Arg input, const Wide& local,
                              bool) noexcept {
  }

  Narrow& operator<<(const Wide& wide) noexcept;
  Wide& operator>>(Wide& wide) noexcept;

  inline bool operator==(const Narrow& other) const noexcept {
    return data == other.data;
  }

  uint32_t data;
};

struct RegistryItem {
  RegistryItem() : data{ 42 } {
  }

  void display(int fd) {
    dprintf(fd, "%d\n", data);
  }

  int data;
};
