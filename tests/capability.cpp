/*
 * Copyright (c) 2021 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <gtest/gtest.h>

#include "shim.h"

using namespace morello::shim;

#ifdef __CHERI_PURE_CAPABILITY__
uintcap_t UnTagCapability(uintcap_t cap) {
  EXPECT_TRUE(cheri_tag_get(cap));

  uintptr_t cap_value = reinterpret_cast<uintptr_t>(cap);
  uintptr_t value = 0;
  value |= cap_value;
  EXPECT_EQ(cap_value, value);

  uintcap_t untagged_cap = reinterpret_cast<uintcap_t>(value);
  EXPECT_EQ(cap, untagged_cap);
  EXPECT_FALSE(cheri_tag_get(untagged_cap));
  return untagged_cap;
}

#endif

TEST(CapabilityTest, Build) {
#ifndef __CHERI_PURE_CAPABILITY__
  GTEST_SKIP();
#else
  volatile int value = 42;
  uintcap_t cap = UnTagCapability(reinterpret_cast<uintcap_t>(&value));
  uintcap_t new_cap = capability::build<uintcap_t>(cap, 0);
  ASSERT_TRUE(cheri_tag_get(new_cap));
  // Note: not testing all permissions
  ASSERT_TRUE((cheri_perms_get(new_cap) & CHERI_PERM_EXECUTE) != 0);
  ASSERT_TRUE((cheri_perms_get(new_cap) & CHERI_PERM_LOAD) != 0);
  ASSERT_TRUE((cheri_perms_get(new_cap) & CHERI_PERM_STORE) != 0);
#endif
}

TEST(CapabilityTest, BuildNoExec) {
#ifndef __CHERI_PURE_CAPABILITY__
  GTEST_SKIP();
#else
  volatile int value = 42;
  uintcap_t cap = UnTagCapability(reinterpret_cast<uintcap_t>(&value));
  uintcap_t new_cap = capability::build_no_exec<uintcap_t>(cap);
  ASSERT_TRUE(cheri_tag_get(new_cap));
  ASSERT_FALSE((cheri_perms_get(new_cap) & CHERI_PERM_EXECUTE) != 0);
#endif
}

TEST(CapabilityTest, BuildNoWrite) {
#ifndef __CHERI_PURE_CAPABILITY__
  GTEST_SKIP();
#else
  volatile int value = 42;
  uintcap_t cap = UnTagCapability(reinterpret_cast<uintcap_t>(&value));
  uintcap_t new_cap = capability::build_no_write<uintcap_t>(cap);
  ASSERT_TRUE(cheri_tag_get(new_cap));
  ASSERT_FALSE((cheri_perms_get(new_cap) & CHERI_PERM_STORE) != 0);
  ASSERT_FALSE((cheri_perms_get(new_cap) & CHERI_PERM_STORE_CAP) != 0);
  ASSERT_FALSE((cheri_perms_get(new_cap) & CHERI_PERM_STORE_LOCAL_CAP) != 0);
#endif
}
