/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>

#include "shim.h"
#include "stl/config.h"

using namespace morello;

void SyscallDeathTest_Unknown() {
  shim::syscall(1, 2, 3, 4, 5, 6, /* nr */ 1234);
}

TEST(SyscallDeathTest, Unknown) {
  ASSERT_EXIT(SyscallDeathTest_Unknown(), ::testing::ExitedWithCode(shim::stl::kPanicExitCode),
              "\\[LIBSHIM\\] Unknown system call number: 1234.\n");
}
