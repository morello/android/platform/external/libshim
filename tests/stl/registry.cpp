/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/registry.h"

#include <gtest/gtest.h>

#include "helpers.h"

using namespace morello::shim::stl;

TEST(RegistryTest, DefaultSize) {
  Registry<uint32_t> reg{};

  ASSERT_EQ(reg.size(), Registry<uint32_t>::default_size());
  ASSERT_EQ(16, Registry<uint32_t>::default_size());
}

TEST(RegistryTest, Grows) {
  Registry<uint32_t> reg{};

  const size_t size = reg.default_size();
  ASSERT_EQ(reg.size(), size);

  reg.update(size - 1, 1);
  ASSERT_EQ(reg.size(), size);

  // Grow the registry
  reg.update(size, 1);
  ASSERT_EQ(reg.size(), 2 * size);

  reg.update(2 * size - 1, 1);
  ASSERT_EQ(reg.size(), 2 * size);

  reg.update(10 * size - 1, 1);
  ASSERT_EQ(reg.size(), 10 * size);

  reg.update(10 * size, 1);
  ASSERT_EQ(reg.size(), 11 * size);
}

TEST(RegistryTest, Iter) {
  Registry<uint32_t> reg{};

  {
    size_t index;
    for (index = 0; index < reg.size(); ++index) {
      ASSERT_EQ(reg[index], 0);
      ASSERT_EQ(reg.update(index, index + 1), 0);
    }
    ASSERT_EQ(index, Registry<uint32_t>::default_size());

    for (index = 0; index < reg.size(); ++index) {
      ASSERT_EQ(reg[index], index + 1);
    }
  }

  // Grow the registry
  ASSERT_EQ(reg.update(reg.size(), 0xFFFF), 0);

  // Check values after resize.
  {
    size_t index;
    for (index = 0; index < reg.default_size(); ++index) {
      ASSERT_EQ(reg[index], index + 1);
    }

    ASSERT_EQ(reg[index], 0xFFFF);
    ++index;

    for (; index < reg.size(); ++index) {
      ASSERT_EQ(reg[index], 0);
    }

    ASSERT_EQ(index, 2 * Registry<uint32_t>::default_size());
  }
}

TEST(RegistryTest, DefaultValue) {
  Registry<RegistryItem> reg{};

  for (size_t index = 0; index < reg.size(); ++index) {
    ASSERT_EQ(reg[index].data, 42) << " at index " << index << "\n";
  }
}

TEST(RegistryTest, Display) {
  Registry<RegistryItem> reg{};

  reg.display();
}
