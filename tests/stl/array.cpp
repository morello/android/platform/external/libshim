/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/array.h"

#include <gtest/gtest.h>

using namespace morello::shim::stl;

TEST(ArrayTest, ZeroLength) {
  Array<uint32_t> array{ 0 };

  for (auto item : array.iter()) {
    FAIL() << "Iterator should not iterate in this case.";
    (void)item;
  }

  ASSERT_EQ(array.data(), nullptr);
  ASSERT_EQ(array.size(), 0);
}

static void TestArray(const size_t size, const bool on_heap) {
  uint32_t* array_data;
  size_t array_size;
  Array<uint32_t> moved_array{ 0 };

  {
    Array<uint32_t> array{ size };

    ASSERT_EQ(array.is_on_heap(), on_heap);
    ASSERT_NE(array.data(), nullptr);
    ASSERT_EQ(array.size(), size);

    const bool is_on_stack =
        ((char*)array.data() >= (char*)&array && (char*)array.data() < (char*)(&array + 1));
    ASSERT_NE(on_heap, is_on_stack);

    // Test iterator
    size_t count = 0;
    for (auto& item : array.iter()) {
      item = count + 1;
      ++count;
    }
    ASSERT_EQ(count, size);

    count = 0;
    for (auto& item : array.iter()) {
      ASSERT_EQ(item, count + 1);
      ++count;
    }

    for (auto i = 0; i < count; ++i) {
      ASSERT_EQ(array.data()[i], i + 1);
    }

    // Test move assignment operator
    array_data = array.data();
    array_size = array.size();
    moved_array = std::move(array);
  }

  ASSERT_EQ(moved_array.size(), array_size);
  if (on_heap) {
    ASSERT_EQ(moved_array.data(), array_data);
  } else {
    ASSERT_NE(moved_array.data(), array_data);
  }
}

TEST(ArrayTest, OnStack) {
  const size_t kSize = 8;
  ASSERT_TRUE(Array<uint32_t>::default_threshold() > kSize);
  TestArray(kSize, false);
}

TEST(ArrayTest, OnHeap) {
  const size_t kSize = Array<uint32_t>::default_threshold() + 1;
  TestArray(kSize, true);
}
