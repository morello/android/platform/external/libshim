/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/iterator.h"

#include <gtest/gtest.h>

#include "stl/config.h"

using namespace morello::shim::stl;

TEST(IteratorTest, RangeBasedForLoop_L0) {
  Iterator<uint32_t> iterator{ nullptr, 0 };

  for (auto item : iterator) {
    FAIL() << "Iterator should not iterate in this case.";
    (void)item;
  }
}

TEST(IteratorTest, RangeBasedForLoop_L4) {
  uint32_t array[] = { 1, 2, 3, 4 };

  Iterator<uint32_t> iterator{ &array[0], sizeof(array) / sizeof(array[0]) };

  uint32_t expected = 1;
  for (auto item : iterator) {
    ASSERT_EQ(item, expected);
    ++expected;
  }

  ASSERT_EQ(expected, 5);
}

void IteratorDeathTest_SizedNullptr() {
  Iterator<uint32_t> iterator{ nullptr, 1 };
}

TEST(IteratorDeathTest, SizedNullptr) {
  ASSERT_EXIT(IteratorDeathTest_SizedNullptr(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Argument 'items' is nullptr but length is not zero.");
}

void IteratorDeathTest_RangeBasedForLoop_Over() {
  uint32_t array[] = { 1 };

  Iterator<uint32_t> iterator{ &array[0], sizeof(array) / sizeof(array[0]) };

  for (auto item : iterator) {
    ASSERT_EQ(item, 1);
  }

  // Try to overiterate.
  *(++iterator);
}

TEST(IteratorDeathTest, RangeBasedForLoop_Over) {
  ASSERT_EXIT(IteratorDeathTest_RangeBasedForLoop_Over(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Tried to iterate over the range.");
}

void IteratorDeathTest_RangeBasedForLoop_Over2() {
  uint32_t array[] = { 42 };

  Iterator<uint32_t> iterator{ &array[0], sizeof(array) / sizeof(array[0]) };

  for (auto item : iterator) {
    ASSERT_EQ(item, 42);
  }

  // Try to overiterate.
  iterator += 1;
  *iterator;
}

TEST(IteratorDeathTest, RangeBasedForLoop_Over2) {
  ASSERT_EXIT(IteratorDeathTest_RangeBasedForLoop_Over2(),
              ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Tried to iterate over the range.");
}
