/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/signal.h"

#include <gtest/gtest.h>
#include <signal.h>
#include <unistd.h>

#include <thread>
#include <vector>

using namespace morello::shim::stl;

static bool signal_triggered = false;
static int triggered_signo = -1;

TEST(ScopedSignalBlockTest, Blocks) {
  struct sigaction handler {};
  handler.sa_handler = [](int signo) {
    signal_triggered = true;
    triggered_signo = signo;
  };
  ASSERT_EQ(sigaction(SIGUSR1, &handler, nullptr), 0);
  ASSERT_EQ(sigaction(SIGUSR2, &handler, nullptr), 0);

  // Test that signaling works.
  ASSERT_EQ(pthread_kill(pthread_self(), SIGUSR1), 0);
  ASSERT_EQ(signal_triggered, true);
  ASSERT_EQ(triggered_signo, SIGUSR1);
  signal_triggered = false;

  // Test that ScopedSignalBlock actually blocks a signal on the current thread.
  {
    ScopedSignalBlock block;
    ASSERT_EQ(pthread_kill(pthread_self(), SIGUSR2), 0);
    ASSERT_EQ(signal_triggered, false);
  }

  // Signal should have been delivered by now.
  ASSERT_EQ(signal_triggered, true);
  ASSERT_EQ(triggered_signo, SIGUSR2);

  // Restore default handlers.
  handler.sa_handler = SIG_DFL;
  ASSERT_EQ(sigaction(SIGUSR1, &handler, nullptr), 0);
  ASSERT_EQ(sigaction(SIGUSR2, &handler, nullptr), 0);
}
