/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>

#include "stl/config.h"
#include "stl/iterator.h"

using namespace morello::shim::stl;

TEST(ZipIteratorTest, RangeBasedForLoop_L0) {
  ZipIterator<uint32_t, uint32_t> iterator{ nullptr, nullptr, 0 };

  for (auto item : iterator) {
    FAIL() << "ZipIterator should not iterate in this case.";
    (void)item;
  }
}

TEST(ZipIteratorTest, RangeBasedForLoop_L4) {
  uint32_t array1[] = { 1, 2, 3, 4 };
  uint16_t array2[] = { 11, 12, 13, 14, 15 };

  ZipIterator<uint32_t, uint16_t> iterator{ &array1[0], &array2[0],
                                            sizeof(array1) / sizeof(array1[0]) };

  uint32_t expected = 1;
  for (auto item : iterator) {
    ASSERT_EQ(item.input, expected);
    ASSERT_EQ(item.output, expected + 10);
    ++expected;
  }

  ASSERT_EQ(expected, 5);
}

void ZipIteratorDeathTest_SizedInputNullptr() {
  uint8_t array[] = { 1 };

  ZipIterator<uint8_t, uint8_t> iterator{ nullptr, &array[0], 1 };
}

TEST(ZipIteratorDeathTest, SizedInputNullptr) {
  ASSERT_EXIT(ZipIteratorDeathTest_SizedInputNullptr(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Argument 'inputs' is nullptr but length is not zero.");
}

void ZipIteratorDeathTest_SizedOutputNullptr() {
  uint8_t array[] = { 1 };

  ZipIterator<uint8_t, uint8_t> iterator{ &array[0], nullptr, 1 };
}

TEST(ZipIteratorDeathTest, SizedOutputNullptr) {
  ASSERT_EXIT(ZipIteratorDeathTest_SizedOutputNullptr(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Argument 'outputs' is nullptr but length is not zero.");
}
void ZipIteratorDeathTest_RangeBasedForLoop_Over() {
  uint32_t array1[] = { 1 };
  uint8_t array2[] = { 2 };

  ZipIterator<uint32_t, uint8_t> iterator{ &array1[0], &array2[0],
                                           sizeof(array1) / sizeof(array1[0]) };

  for (auto item : iterator) {
    ASSERT_EQ(item.input, 1);
    ASSERT_EQ(item.output, 2);
  }

  // Try to overiterate.
  *(++iterator);
}

TEST(ZipIteratorDeathTest, RangeBasedForLoop_Over) {
  ASSERT_EXIT(ZipIteratorDeathTest_RangeBasedForLoop_Over(),
              ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] Tried to iterate over the range.");
}
