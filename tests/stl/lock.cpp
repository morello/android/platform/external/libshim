/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/lock.h"

#include <gtest/gtest.h>

#include <thread>
#include <vector>

using namespace morello::shim::stl;

TEST(LockTest, Simple) {
  LockGuard guard;

  // Lock, unlock.
  { Lock lock{ guard }; }

  // Test that we can re-lock a dropped lock.
  { Lock lock{ guard }; }
}

void ContendingForLock(LockGuard& guard, int& value) {
  for (size_t i = 0; i < 1000; ++i) {
    {
      // Acquire the lock.
      Lock lock{ guard };
      ++value;
      // The lock is dropped here.
    }
  }
}

TEST(LockTest, Contention) {
  LockGuard guard;

  int value = 0;
  std::vector<std::thread> threads;

  // Spawn threads and hold them back with a lock so that none of them runs away.
  {
    // Acquire the lock.
    Lock lock{ guard };

    for (size_t i = 0; i < 4; ++i) {
      threads.push_back(std::thread(ContendingForLock, std::ref(guard), std::ref(value)));
    }

    // The lock is dropped here.
  }

  // Wait for the threads to finish.
  for (auto& thread : threads) {
    thread.join();
  }

  // The value should add up.
  ASSERT_EQ(value, 1000 * 4);
}
