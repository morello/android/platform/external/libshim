/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "stl/panic.h"

#include <gtest/gtest.h>

using namespace morello::shim::stl;

TEST(PanicTest, ErrorMessage) {
  error_message("This is a message on stderr: %d.\n", 2);
}

TEST(PanicTest, ErrorMessageLong) {
  char long_message[1024];
  std::fill_n(long_message, sizeof(long_message), 'A');
  long_message[sizeof(long_message) - 1] = '\0';

  error_message("%s\n", &long_message[0]);
}

void PanicTest_Panic() {
  panic("This was a very bad idea: %d.\n", 42);
}

TEST(PanicTest, Panic) {
  ASSERT_EXIT(PanicTest_Panic(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] This was a very bad idea: 42.\n");
}

void PanicTest_PanicLong() {
  char long_message[1024];
  std::fill_n(long_message, sizeof(long_message), 'A');
  long_message[sizeof(long_message) - 1] = '\0';

  panic("%s\n", &long_message[0]);
}

TEST(PanicTest, PanicLong) {
  ASSERT_EXIT(PanicTest_PanicLong(), ::testing::ExitedWithCode(kPanicExitCode),
              "\\[LIBSHIM\\] [A]+<truncated>\n");
}
