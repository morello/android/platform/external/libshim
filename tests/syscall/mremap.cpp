/*
 * Copyright (c) 2021 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <sys/user.h>
#include <unistd.h>

#include "shim.h"

// Test basic mremap functionality

TEST(MremapTest, Shrink) {
  morello::shim::uintptr_shim_t map =
      mmap(nullptr, PAGE_SIZE * 5, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(map, MAP_FAILED);

  morello::shim::uintptr_shim_t new_map = mremap(map, PAGE_SIZE * 5, PAGE_SIZE, 0);
  ASSERT_EQ(new_map, map);

  ASSERT_EXIT((memset(new_map, 1, PAGE_SIZE), exit(0)), ::testing::ExitedWithCode(0), ".*");
  ASSERT_EXIT((memset(new_map, 2, PAGE_SIZE * 2), exit(0)), ::testing::KilledBySignal(SIGSEGV),
              ".*");

  ASSERT_EQ(munmap(new_map, PAGE_SIZE), 0);
}

TEST(MremapTest, ShrinkGrow) {
  morello::shim::uintptr_shim_t map =
      mmap(nullptr, PAGE_SIZE * 5, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(map, MAP_FAILED);

  morello::shim::uintptr_shim_t new_map = mremap(map, PAGE_SIZE * 5, PAGE_SIZE, 0);
  ASSERT_EQ(new_map, map);
  map = new_map;

  new_map = mremap(map, PAGE_SIZE, PAGE_SIZE * 2, 0);
  ASSERT_EQ(new_map, map);

  ASSERT_EXIT((memset(new_map, 1, PAGE_SIZE * 2), exit(0)), ::testing::ExitedWithCode(0), ".*");
  ASSERT_EXIT((memset(new_map, 2, PAGE_SIZE * 5), exit(0)), ::testing::KilledBySignal(SIGSEGV),
              ".*");

  ASSERT_EQ(munmap(new_map, PAGE_SIZE * 2), 0);
}

// Test the case where old_size == 0, for duplicating a shared mapping.

TEST(MremapTest, DuplicateSharedMapping) {
  morello::shim::uintptr_shim_t map =
      mmap(nullptr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(map, MAP_FAILED);

  *((int*)map) = 42;

  morello::shim::uintptr_shim_t new_map = mremap(map, 0, PAGE_SIZE, MREMAP_MAYMOVE);
  ASSERT_NE(new_map, MAP_FAILED);

  ASSERT_EQ(*((int*)new_map), 42);

  ASSERT_EQ(munmap(map, PAGE_SIZE), 0);
  ASSERT_EQ(munmap(new_map, PAGE_SIZE), 0);
}
