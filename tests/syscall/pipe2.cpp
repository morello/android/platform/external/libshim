/*
 * Copyright (c) 2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>
#include <stdio.h>
#include <unistd.h>

#include "stl/panic.h"

TEST(Pipe2Test, Nullptr) {
  int retval = pipe2(nullptr, 0);
  ASSERT_EQ(retval, -1);
  ASSERT_EQ(errno, EFAULT);
}

TEST(Pipe2Test, NoError) {
  int pipefd[2];
  int retval = pipe2(&pipefd[0], 0);
  ASSERT_EQ(retval, 0);
}

TEST(Pipe2Test, InvalidFd) {
#ifdef __CHERI_PURE_CAPABILITY__
  int pipefd[1];
  int retval = pipe2(&pipefd[0], 0);
  ASSERT_EQ(retval, -1);
  ASSERT_EQ(errno, EFAULT);
#else
  GTEST_SKIP();
#endif
}
