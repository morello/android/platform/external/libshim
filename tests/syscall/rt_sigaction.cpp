/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>
#include <signal.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>

// Restores the default handler for signo
static void restore_signal_handler(int signo) {
  struct sigaction handler2 {};
  handler2.sa_handler = SIG_DFL;
  ASSERT_EQ(sigaction(signo, &handler2, nullptr), 0);
}

TEST(SigactionTest, None) {
  ASSERT_EQ(sigaction(SIGUSR2, nullptr, nullptr), 0);
}

TEST(SigactionTest, OldAct) {
  struct sigaction old_handler {};

  ASSERT_EQ(sigaction(SIGUSR2, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);
}

TEST(SigactionTest, SaHandler) {
  struct sigaction handler {};
  struct sigaction old_handler {};
  handler.sa_handler = [](int) {};

  ASSERT_EQ(sigaction(SIGUSR2, &handler, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);

  ASSERT_EQ(sigaction(SIGUSR2, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, handler.sa_handler);

  struct sigaction handler2 {};
  handler2.sa_handler = SIG_DFL;
  ASSERT_EQ(sigaction(SIGUSR2, &handler2, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, handler.sa_handler);

  ASSERT_EQ(sigaction(SIGUSR2, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);
}

TEST(SigactionTest, SaSigaction) {
  struct sigaction handler {};
  struct sigaction old_handler {};
  handler.sa_sigaction = [](int, siginfo_t*, void*) {};
  handler.sa_flags = SA_SIGINFO;

  ASSERT_EQ(sigaction(SIGUSR2, &handler, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_flags & SA_SIGINFO, 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);

  ASSERT_EQ(sigaction(SIGUSR2, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_sigaction, handler.sa_sigaction);

  // SIG_DFL should be without SA_SIGINFO.
  struct sigaction handler2 {};
  handler2.sa_handler = SIG_DFL;
  ASSERT_EQ(sigaction(SIGUSR2, &handler2, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_sigaction, handler.sa_sigaction);

  ASSERT_EQ(sigaction(SIGUSR2, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);
}

TEST(SigactionTest, Unmodified) {
  struct sigaction handler {};
  struct sigaction old_handler {};
  handler.sa_sigaction = [](int, siginfo_t*, void*) {};
  handler.sa_flags = SA_SIGINFO;

  ASSERT_EQ(sigaction(SIGKILL, &handler, &old_handler), -1);

  ASSERT_EQ(sigaction(SIGKILL, nullptr, &old_handler), 0);
  ASSERT_EQ(old_handler.sa_flags & SA_SIGINFO, 0);
  ASSERT_EQ(old_handler.sa_handler, SIG_DFL);
}

TEST(SigactionTest, InvokeSaHandler) {
  static int triggered_signo = -1;

  struct sigaction handler {};
  handler.sa_handler = [](int signo) { triggered_signo = signo; };

  ASSERT_EQ(sigaction(SIGUSR2, &handler, nullptr), 0);

  ASSERT_EQ(kill(getpid(), SIGUSR2), 0);

  ASSERT_EQ(triggered_signo, SIGUSR2);

  restore_signal_handler(SIGUSR2);
}

TEST(SigactionTest, InvokeSaSigaction) {
  static int triggered_signo = -1;
  static siginfo_t received_siginfo{};

  struct sigaction handler {};
  handler.sa_handler = [](int signo) { triggered_signo = signo; };
  handler.sa_sigaction = [](int signo, siginfo_t* siginfo, void*) {
    triggered_signo = signo;
    received_siginfo = *siginfo;
  };
  handler.sa_flags = SA_SIGINFO;

  ASSERT_EQ(sigaction(SIGUSR2, &handler, nullptr), 0);

  siginfo info{};
  info.si_code = SI_TKILL;
  ASSERT_EQ(syscall(SYS_rt_tgsigqueueinfo, getpid(), gettid(), SIGUSR2, &info), 0);

  ASSERT_EQ(triggered_signo, SIGUSR2);
  ASSERT_EQ(received_siginfo.si_code, info.si_code);

  restore_signal_handler(SIGUSR2);
}
