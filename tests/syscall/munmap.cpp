/*
 * Copyright (c) 2022-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <gtest/gtest.h>
#include <sys/mman.h>

#include "shim.h"

using namespace morello::shim;

#ifdef __CHERI_PURE_CAPABILITY__
// Successful unmap
TEST(Munmap, UnmapOwningCap) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem = ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);
}

#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
// Failed unmap - no ownership permission
TEST(Munmap, UnmapNotOwningCap) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem = ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  void *new_mem = cheri_perms_clear(mem, CHERI_PERM_SW_VMEM);
  ASSERT_EQ(munmap(new_mem, PAGE_SIZE), -1);
  ASSERT_EQ(errno, EINVAL);

  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);
}
#endif  // LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED

// Null derived capability mmap and failed unmap due to out of bounds.
TEST(Munmap, UnmapCapOutOfBounds) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem = ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), -1);
  ASSERT_EQ(errno, EINVAL);

  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);
}
#endif  // __CHERI_PURE_CAPABILITY__
