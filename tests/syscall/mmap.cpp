/*
 * Copyright (c) 2021-2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cheriintrin.h>
#include <gtest/gtest.h>
#include <sys/mman.h>

#include "shim.h"

using namespace morello::shim;

// Not MAP_FIXED and hint address is untagged.
TEST(Mmap, NotFixedUntaggedCap) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000000));
  uintptr_shim_t mem =
      ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);
}

// MAP_FIXED but with untagged capability.
TEST(Mmap, FixedUntaggedCap) {
  void *mem =
      ::mmap(nullptr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);
  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);

  ptraddr_t hint_value = static_cast<ptraddr_t>(reinterpret_cast<uintptr_t>(mem));
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(hint_value));
#ifdef __CHERI_PURE_CAPABILITY__
  ASSERT_EQ(cheri_tag_get(hint), false);
#endif
  void *new_mem =
      ::mmap(hint, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  ASSERT_NE(new_mem, MAP_FAILED);
}

#ifdef __CHERI_PURE_CAPABILITY__
// MAP_FIXED but with a wrong-bounded tagged capability.
TEST(Mmap, FixedWrongCap) {
  void *mem =
      ::mmap(nullptr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  void *wrong_mem = cheri_bounds_set(mem, PAGE_SIZE - 1);
  ASSERT_EQ(cheri_tag_get(wrong_mem), true);
  void *new_mem =
      ::mmap(wrong_mem, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  ASSERT_EQ(new_mem, MAP_FAILED);
}

// Null derived capability
TEST(Mmap, NullDerivedCap) {
  void *addr = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem = ::mmap(addr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);
}

// Null derived capability with MAP_FIXED
TEST(Mmap, NullDerivedCapMapFixed) {
  uintptr_shim_t hint = 0x10000;
  void *mem =
      ::mmap(hint, PAGE_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  // Call with an addr to a preexisting mapped range should fail with EEXIST.
  void *mem2 = ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE,
                      MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  ASSERT_EQ(mem2, MAP_FAILED);
  // TODO: Once reservations are supported, this testcase should fail
  // with ERESERVATION and hence need to be updated.
  ASSERT_EQ(errno, EEXIST);

  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), 0);
}

// Owning Capability without MAP_FIXED
TEST(Mmap, OwningCap) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem =
      ::mmap(hint, PAGE_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  // As per spec, if addr is an owning capability, if flags does not include MAP_FIXED or
  // MAP_FIXED_NOREPLACE, then the call fails with -EINVAL.
  void *new_mem =
      ::mmap(mem, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_EQ(new_mem, MAP_FAILED);
  ASSERT_EQ(errno, EINVAL);

  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), 0);
}

// Owning Capability with MAP_FIXED
TEST(Mmap, OwningCapMapFixed) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem =
      ::mmap(hint, PAGE_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);
  void *new_mem = ::mmap(mem, PAGE_SIZE, PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  ASSERT_NE(new_mem, MAP_FAILED);

  ASSERT_EQ(cheri_is_equal_exact(mem, new_mem), true);
  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), 0);
}

// Owning Capability with MAP_FIXED_NOREPLACE
TEST(Mmap, OwningCapMapFixedNoReplace) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem =
      ::mmap(hint, PAGE_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  // Do partial unmap.
  ASSERT_EQ(munmap(mem, PAGE_SIZE), 0);

  void *new_mem = ::mmap(mem, PAGE_SIZE, PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
  ASSERT_NE(new_mem, MAP_FAILED);

  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), 0);
}

// Owning Capability with MAP_FIXED_NOREPLACE. Fails with EEXIST.
// Memory allocated with first mmap is not unmapped before calling the second mmap.
TEST(Mmap, OwningCapMapFixedNoReplaceWithoutUnmap) {
  void *hint = reinterpret_cast<void *>(static_cast<uintptr_t>(0x10000));
  void *mem =
      ::mmap(hint, PAGE_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(mem, MAP_FAILED);

  void *new_mem = ::mmap(mem, PAGE_SIZE, PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
  ASSERT_EQ(new_mem, MAP_FAILED);
  ASSERT_EQ(errno, EEXIST);

  ASSERT_EQ(munmap(mem, PAGE_SIZE * 2), 0);
}

#if LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED
// Neither Null capability nor Owning Capability
TEST(Mmap, NeitherNullNorOwn) {
  uintptr_shim_t hint = 0x10000;
  hint.to_valid_data_pointer(PAGE_SIZE);

  void *mem = ::mmap(hint, PAGE_SIZE, PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

  ASSERT_EQ(mem, MAP_FAILED);
  ASSERT_EQ(errno, EINVAL);
}
#endif  // LIBSHIM_TRANSITION_SWITCH_VMEM_IMPLEMENTED

// If flags contain MAP_GROWSDOWN, mmap should fail(EOPNOTSUPP)
TEST(Mmap, MapGrowsDown) {
  void *mem =
      ::mmap(nullptr, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_GROWSDOWN, -1, 0);

  ASSERT_EQ(mem, MAP_FAILED);
  ASSERT_EQ(errno, EOPNOTSUPP);
}
#endif  // __CHERI_PURE_CAPABILITY__
