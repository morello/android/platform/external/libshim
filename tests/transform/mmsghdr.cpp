/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>
#include <sys/socket.h>

#include <array>

#include "helpers_msghdr.h"

TEST(MmsgHdr, Smoke) {
  // Create socket
  int socket_fds[2];
  int send_fd, recv_fd;
  OK(socketpair(AF_UNIX, SOCK_DGRAM, 0, &socket_fds[0]));
  send_fd = socket_fds[0];
  recv_fd = socket_fds[1];

  // Transferred message
  const char* part1 = "Smoke test for ";
  const char* part2 = "morello::shim::transform::*MmsgHdrs. ";
  const char* part3 = "This is more complex than sendmsg or recvmsg. ";
  const char* part4 = "Hope that it works fine.";
  const size_t msg_len_part_1 = strlen(part1) + strlen(part2);
  const size_t msg_len_part_2 = strlen(part3) + strlen(part4);

  // Send message
  int sent;
  MsgHdrHelper<2> send_msg_1{ { part1, part2 } };
  MsgHdrHelper<2> send_msg_2{ { part3, part4 } };
  MmsgHdrHelper<2, 2> send_mmsg{ { send_msg_1, send_msg_2 } };

  OK(sent = sendmmsg(send_fd, send_mmsg.mmsghdr_list.data(), send_mmsg.mmsghdr_list.size(), 0));
  ASSERT_EQ(sent, 2);
  ASSERT_EQ(send_mmsg.mmsghdr_list[0].msg_len, msg_len_part_1);
  ASSERT_EQ(send_mmsg.mmsghdr_list[1].msg_len, msg_len_part_2);

  // Receive message
  int received;
  std::array<char[RECEIVE_BUFF_LEN], 2> recv_buffers_1;
  MsgHdrHelper<2> recv_msg_1{ recv_buffers_1 };
  std::array<char[RECEIVE_BUFF_LEN], 2> recv_buffers_2;
  MsgHdrHelper<2> recv_msg_2{ recv_buffers_2 };
  MmsgHdrHelper<2, 2> recv_mmsg{ { recv_msg_1, recv_msg_2 } };

  OK(received = recvmmsg(recv_fd, recv_mmsg.mmsghdr_list.data(), recv_mmsg.mmsghdr_list.size(), 0,
                         nullptr));
  ASSERT_EQ(received, 2);
  ASSERT_EQ(recv_mmsg.mmsghdr_list[0].msg_len, msg_len_part_1);
  ASSERT_EQ(recv_mmsg.mmsghdr_list[1].msg_len, msg_len_part_2);
}
