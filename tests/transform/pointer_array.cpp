/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "transform/pointer_array.h"

#include <gtest/gtest.h>

using namespace morello::shim::transform;

TEST(PointerArrayTest, Length) {
  ASSERT_EQ(nullterminated_array_length(nullptr), 0);

  void* array1[] = { nullptr };
  ASSERT_EQ(nullterminated_array_length(array1), 1);

  int foo = 42;
  int bar = 24;
  void* array3[] = {
    reinterpret_cast<void*>(&foo),
    reinterpret_cast<void*>(&bar),
    nullptr,
  };
  ASSERT_EQ(nullterminated_array_length(array3), 3);
}
