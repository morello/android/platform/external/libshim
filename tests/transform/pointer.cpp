/*
 * Copyright (c) 2020-2021, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "transform/pointer.h"

#include <gtest/gtest.h>

#include "helpers.h"
#include "shimconfig.h"

using namespace morello::shim;
using namespace morello::shim::transform;

template <typename T>
uintptr_shim_t make_pointer(size_t value) {
  return capability::build_no_exec<T*>(value);
}

size_t round_integer(size_t value) {
  uintptr_shim_t ptr{ value };
  ptr = ptr.round_to_page_size();
  return static_cast<size_t>(ptr);
}

template <typename T>
size_t round_pointer(size_t value) {
  uintptr_shim_t ptr{ capability::build_no_exec<T*>(value) };
  ptr = ptr.round_to_page_size();
  return static_cast<size_t>(ptr);
}

// For testing purposes.
static volatile int foo = 42;

TEST(PointerTest, RoundToPageSize) {
  static const size_t kPageSize = sysconf(_SC_PAGESIZE);

  // Round pointers
  ASSERT_EQ(round_pointer<const void>(0), 0);
  ASSERT_EQ(round_pointer<const void>(kPageSize), kPageSize);
  ASSERT_EQ(round_pointer<const void>(kPageSize - 1), kPageSize);
  ASSERT_EQ(round_pointer<const void>(kPageSize), kPageSize);
  ASSERT_EQ(round_pointer<const void>(kPageSize + 1), 2 * kPageSize);
  ASSERT_EQ(round_pointer<const void>(2 * kPageSize - 1), 2 * kPageSize);

  // Round integers
  ASSERT_EQ(round_integer(0), 0);
  ASSERT_EQ(round_integer(kPageSize), kPageSize);
  ASSERT_EQ(round_integer(kPageSize - 1), kPageSize);
  ASSERT_EQ(round_integer(kPageSize), kPageSize);
  ASSERT_EQ(round_integer(kPageSize + 1), 2 * kPageSize);
  ASSERT_EQ(round_integer(2 * kPageSize - 1), 2 * kPageSize);
}

TEST(PointerTest, PointerValid) {
  static const size_t kPageSize = sysconf(_SC_PAGESIZE);

  // Check pointers
  ASSERT_EQ(make_pointer<const void>(0).is_valid_pointer(), false);
  ASSERT_EQ(make_pointer<const void>(kPageSize - 1).is_valid_pointer(), false);
  ASSERT_EQ(make_pointer<const void>(kPageSize).is_valid_pointer(), true);
  ASSERT_EQ(uintptr_shim_t{ &kPageSize }.is_valid_pointer(), true);
  ASSERT_EQ(uintptr_shim_t{ &foo }.is_valid_pointer(), true);
  ASSERT_EQ(make_pointer<const void>(static_cast<size_t>(1) << 55).is_valid_pointer(), false);

  // Check integers
  ASSERT_EQ(uintptr_shim_t{ 0 }.is_valid_pointer(), false);
  ASSERT_EQ(uintptr_shim_t{ kPageSize - 1 }.is_valid_pointer(), false);
#ifdef __CHERI_PURE_CAPABILITY__
  ASSERT_EQ(uintptr_shim_t{ kPageSize }.is_valid_pointer(), false);
#else
  ASSERT_EQ(uintptr_shim_t{ kPageSize }.is_valid_pointer(), true);
#endif
  ASSERT_EQ(uintptr_shim_t{ static_cast<size_t>(1) << 55 }.is_valid_pointer(), false);
}
