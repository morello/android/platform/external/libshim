/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>

#include "helpers.h"
#include "transform/base.h"

using namespace morello::shim;
using namespace morello::shim::check;

TEST(OutputTransformationTest, NullPtr) {
  Check check{ "" };
  transform::OutputTransformation<const Wide, Narrow> trans{ check, Arg{ nullptr } };

  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  ASSERT_EQ(trans.data(), nullptr);
}

TEST(OutputTransformationTest, WideToNarrow) {
  Wide input{ .data = 0x12345678deadbeef };
  Check check{ "" };

  // Conversion from Wide to Narrow.
  transform::OutputTransformation<const Wide, Narrow> trans{ check, Arg{ &input } };

  // Use 'operator uintptr_shim_t()' which returns the pointer of the temporary,
  // 'Narrow' structure.
  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  ASSERT_EQ(trans.data()->data, 0xdeadbeef);
}

TEST(OutputTransformationTest, NarrowToWide) {
  Narrow input{ .data = 0xdeadbeef };
  Check check{ "" };

  // Conversion from Narrow to Wide.
  transform::OutputTransformation<const Narrow, Wide> trans{ check, Arg{ &input } };

  // Use 'operator uintptr_shim_t()' which returns the pointer of the temporary,
  // 'Wide' structure.
  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  ASSERT_EQ(trans.data()->data, 0x00000000deadbeef);
}
