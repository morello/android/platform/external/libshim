/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>
#include <sys/socket.h>

#include <array>

#include "helpers_msghdr.h"

TEST(MsgHdr, Smoke) {
  // Create socket
  int socket_fds[2];
  int send_fd, recv_fd;
  OK(socketpair(AF_UNIX, SOCK_DGRAM, 0, &socket_fds[0]));
  send_fd = socket_fds[0];
  recv_fd = socket_fds[1];

  // Transferred message
  const char* part1 = "Smoke test for ";
  const char* part2 = "morello::shim::transform::*MsgHdrs.";
  const size_t msg_len = strlen(part1) + strlen(part2);

  // Send message
  int sent;
  MsgHdrHelper<2> send_msg{ { part1, part2 } };

  OK(sent = sendmsg(send_fd, &send_msg.msghdr, 0));
  ASSERT_EQ(sent, msg_len);

  // Receive message
  int received;
  std::array<char[RECEIVE_BUFF_LEN], 2> recv_buffers;
  MsgHdrHelper<2> recv_msg{ recv_buffers };

  OK(received = recvmsg(recv_fd, &recv_msg.msghdr, 0));
  ASSERT_EQ(received, msg_len);
  ASSERT_EQ(strlen(recv_buffers[0]), msg_len);
  ASSERT_EQ(strlen(recv_buffers[1]), 0);
}
