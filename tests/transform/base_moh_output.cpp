/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>

#include <iostream>

#include "helpers.h"
#include "transform/base.h"

using namespace morello::shim;
using namespace morello::shim::check;

TEST(MaybeOnHeapOutputTransformationTest, NullPtr) {
  Check check{ "" };
  transform::MaybeOnHeapOutputTransformation<const Wide, Narrow> trans{ check, Arg{ nullptr },
                                                                        Arg{ 0 } };

  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  ASSERT_EQ(trans.data(), nullptr);

  // Test iter()
  {
    size_t count = 0;
    for (auto& item : trans.iter()) {
      (void)item;
      ++count;
    }
    ASSERT_EQ(count, 0);
  }

  // Test zip()
  {
    size_t count = 0;
    for (auto items : trans.zip()) {
      (void)items;
      ++count;
    }
    ASSERT_EQ(count, 0);
  }
}

TEST(MaybeOnHeapOutputTransformation, NullPtrWithSize) {
  auto check = Check("test");
  transform::MaybeOnHeapOutputTransformation<const Wide, Narrow> trans{ check, Arg{ nullptr },
                                                                        Arg{ 1 } };
  ASSERT_TRUE(check.passed());
}

TEST(MaybeOnHeapOutputTransformationTest, WideToNarrowOnStack) {
  Check check{ "" };
  constexpr size_t kSize = 3;
  constexpr size_t kThreshold =
      transform::MaybeOnHeapOutputTransformation<const Wide, Narrow>::default_threshold();

  ASSERT_TRUE(kThreshold > kSize);

  Wide inputs[kSize] = {
    { .data = 0x1000000110000001 },
    { .data = 0x2000000220000002 },
    { .data = 0x3000000330000003 },
  };

  // Conversion from Wide to Narrow.
  transform::MaybeOnHeapOutputTransformation<const Wide, Narrow> trans{ check, Arg{ &inputs[0] },
                                                                        Arg{ kSize } };

  // Test size()
  ASSERT_EQ(trans.size(), kSize);

  // Use 'operator uintptr_shim_t()' which returns the pointer of the temporary,
  // 'Narrow' structure.
  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  // Test iter()
  {
    size_t count = 0;
    uint32_t expected = 0x10000001;
    for (auto& item : trans.iter()) {
      ASSERT_EQ(item.data, expected);
      expected += 0x10000001;
      ++count;
    }
    ASSERT_EQ(count, kSize);
  }

  // Test zip()
  {
    size_t count = 0;
    uint64_t input_expected = 0x1000000110000001;
    uint32_t output_expected = 0x10000001;
    for (auto items : trans.zip()) {
      ASSERT_EQ(items.input.data, input_expected);
      ASSERT_EQ(items.output.data, output_expected);
      input_expected += 0x1000000110000001;
      output_expected += 0x10000001;
      ++count;
    }
    ASSERT_EQ(count, kSize);
  }
}

TEST(MaybeOnHeapOutputTransformationTest, NarrowToWideOnStack) {
  Check check{ "" };
  constexpr size_t kSize = 3;
  constexpr size_t kThreshold =
      transform::MaybeOnHeapOutputTransformation<const Wide, Narrow>::default_threshold();
  ASSERT_TRUE(kThreshold > kSize);

  Narrow inputs[kSize] = {
    { .data = 0x10000001 },
    { .data = 0x20000002 },
    { .data = 0x30000003 },
  };

  // Conversion from Narrow to Wide.
  transform::MaybeOnHeapOutputTransformation<const Narrow, Wide> trans{ check, Arg{ &inputs[0] },
                                                                        Arg{ kSize } };

  // Test size()
  ASSERT_EQ(trans.size(), kSize);

  // Use 'operator uintptr_shim_t()' which returns the pointer of the temporary,
  // 'Narrow' structure.
  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  // Test iter()
  {
    size_t count = 0;
    uint64_t expected = 0x0000000010000001;
    for (auto& item : trans.iter()) {
      ASSERT_EQ(item.data, expected);
      expected += 0x0000000010000001;
      ++count;
    }
    ASSERT_EQ(count, kSize);
  }

  // Test zip()
  {
    size_t count = 0;
    uint32_t input_expected = 0x10000001;
    uint64_t output_expected = 0x0000000010000001;
    for (auto items : trans.zip()) {
      ASSERT_EQ(items.input.data, input_expected);
      ASSERT_EQ(items.output.data, output_expected);
      input_expected += 0x10000001;
      output_expected += 0x0000000010000001;
      ++count;
    }
    ASSERT_EQ(count, kSize);
  }
}
