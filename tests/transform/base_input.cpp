/*
 * Copyright (c) 2020, 2023 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtest/gtest.h>

#include "helpers.h"
#include "transform/base.h"

using namespace morello::shim;
using namespace morello::shim::check;

TEST(InputTransformationTest, NullPtr) {
  Check check{ "" };
  transform::InputTransformation<Wide, Narrow> trans{ check, Arg{ nullptr } };

  uintptr_shim_t shim_value = trans;
  ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

  ASSERT_EQ(trans.data(), nullptr);
}

TEST(InputTransformationTest, WideToNarrow) {
  Wide input{ .data = 0x12345678deadbeef };
  Check check{ "" };

  {
    // Conversion from Wide to Narrow and back.
    transform::InputTransformation<Wide, Narrow> trans{ check, Arg{ &input } };

    // Use 'operator uintptr_shim_t()' which returns the pointer of the
    // temporary, 'Narrow' structure.
    uintptr_shim_t shim_value = trans;
    ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

    ASSERT_EQ(trans.data()->data, 0xdeadbeef);

    // Change data which will be written back.
    trans.data()->data = 42;

    // The original input is still the same.
    ASSERT_EQ(input.data, 0x12345678deadbeef);

    // "trans" is dropped here: writeback occurs here.
  }

  ASSERT_EQ(input.data, 42);
}

TEST(InputTransformationTest, NarrowToWide) {
  Narrow input{ .data = 0xdeadbeef };
  Check check{ "" };

  {
    // Conversion from Narrow to Wide and back.
    transform::InputTransformation<Narrow, Wide> trans{ check, Arg{ &input } };

    // Use 'operator uintptr_shim_t()' which returns the pointer of the
    // temporary, 'Wide' structure.
    uintptr_shim_t shim_value = trans;
    ASSERT_EQ(shim_value, (uintptr_shim_t)trans.data());

    ASSERT_EQ(trans.data()->data, 0x00000000deadbeef);

    // Change data which will be written back.
    trans.data()->data = 42;

    // The original input is still the same.
    ASSERT_EQ(input.data, 0xdeadbeef);

    // "trans" is dropped here: writeback occurs here.
  }

  ASSERT_EQ(input.data, 42);
}
