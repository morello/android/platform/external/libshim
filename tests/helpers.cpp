/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "helpers.h"

Wide& Wide::operator<<(const Narrow& narrow) noexcept {
  data = narrow.data;
  return *this;
}

Narrow& Wide::operator>>(Narrow& narrow) noexcept {
  narrow.data = data;
  return narrow;
}

Narrow& Narrow::operator<<(const Wide& wide) noexcept {
  data = wide.data;
  return *this;
}

Wide& Narrow::operator>>(Wide& wide) noexcept {
  wide.data = data;
  return wide;
}
