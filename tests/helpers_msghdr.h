/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <array>
#include <cerrno>
#include <string>

#define OK(__stmt)                                                                 \
  if ((__stmt) < 0) {                                                              \
    FAIL() << "Failed statement: '" #__stmt "': " << std::strerror(errno) << "\n"; \
  }

static constexpr size_t RECEIVE_BUFF_LEN = 80;

template <const size_t SIZE, const size_t BUFF_LEN = RECEIVE_BUFF_LEN>
struct MsgHdrHelper {
  MsgHdrHelper(std::array<const char*, SIZE> buffer_list) {
    init_msghdr(msghdr, iov_list);

    for (size_t idx = 0; idx < SIZE; ++idx) {
      auto buffer = buffer_list[idx];
      auto& iov = iov_list[idx];

      iov.iov_base = const_cast<char*>(buffer);
      if (buffer != nullptr) {
        iov.iov_len = strlen(buffer);
      } else {
        iov.iov_len = 0;
      }
    }
  }

  MsgHdrHelper(std::array<char[BUFF_LEN], SIZE>& buffer_list) {
    init_msghdr(msghdr, iov_list);

    for (size_t idx = 0; idx < SIZE; ++idx) {
      auto buffer = buffer_list[idx];
      auto& iov = iov_list[idx];
      iov.iov_base = const_cast<char*>(buffer);
      iov.iov_len = BUFF_LEN;
      memset(iov.iov_base, 0, iov.iov_len);
    }
  }

  static void init_msghdr(struct msghdr& msghdr, std::array<struct iovec, SIZE>& iov_list) {
    memset(&msghdr, 0, sizeof(msghdr));
    msghdr.msg_iov = iov_list.data();
    msghdr.msg_iovlen = iov_list.size();
    msghdr.msg_name = nullptr;
    msghdr.msg_namelen = 0;
  }

  std::array<char*, SIZE> buffer_list;
  std::array<struct iovec, SIZE> iov_list;
  struct msghdr msghdr;
};

template <const size_t SIZE, const size_t IOV_LEN>
struct MmsgHdrHelper {
  MmsgHdrHelper(std::array<MsgHdrHelper<IOV_LEN>, SIZE> msghdr_list) {
    for (size_t idx = 0; idx < SIZE; ++idx) {
      mmsghdr_list[idx].msg_hdr = msghdr_list[idx].msghdr;
      mmsghdr_list[idx].msg_len = 0;
    }
  }

  std::array<struct mmsghdr, SIZE> mmsghdr_list;
};
